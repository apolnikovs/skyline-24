DROP PROCEDURE IF EXISTS trigger_delete;

DELIMITER $$

CREATE PROCEDURE trigger_delete(IN tableName VARCHAR(100), IN primaryID INT(11), IN userID INT(11), IN value TEXT)
BEGIN

	IF userID IS NULL
	THEN SET userID = 0;
	END IF;
	
	INSERT INTO audit_new 
				(
					UserID, 
					TableName,
					`Action`, 
					PrimaryID, 
					OldValue 
				) 
	VALUES 		
				(
					userID,
					tableName,
					'delete',
					primaryID,
					value
				);

END
$$

DELIMITER ;