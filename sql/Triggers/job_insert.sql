DROP TRIGGER IF EXISTS job_insert;

DELIMITER ;;

CREATE TRIGGER job_insert BEFORE INSERT ON job FOR EACH ROW 
BEGIN

	/*Inserts closed date if job status is cancelled*/
	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;

	/*Tries to look up and insert ConditionCodeID if ConditionCode is inserted*/
	IF NEW.ConditionCode IS NOT NULL AND NEW.ConditionCodeID IS NULL
	THEN 
		BEGIN
			IF (@id := (SELECT ConditionCodeID FROM condition_codes WHERE ConditionCode = NEW.ConditionCode)) != NULL
			THEN SET NEW.ConditionCodeID = @id;
			END IF;
		END;
	END IF;

	/*Tries to look up and insert SymptomCodeID if SymptomCode is inserted*/
	IF NEW.SymptomCode IS NOT NULL AND NEW.SymptomCodeID IS NULL
	THEN 
		BEGIN
			IF (@id := (SELECT SymptomCodeID FROM symptom_codes WHERE SymptomCode = NEW.SymptomCode)) != NULL
			THEN SET NEW.SymptomCodeID = @id;
			END IF;
		END;
	END IF;

END
;;

DELIMITER ;