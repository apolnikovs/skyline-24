# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.219');


# ---------------------------------------------------------------------- #
# Add Table datatables_custom_columns                                    #
# ---------------------------------------------------------------------- #
CREATE TABLE datatables_custom_columns (
										DatatablesCustomColumnsID INT(10) NULL AUTO_INCREMENT, 
										UserID INT(10) NULL, PageID INT(10) NULL, 
										ColumnString VARCHAR(500) NULL, 
										PRIMARY KEY (DatatablesCustomColumnsID) 
										) COLLATE='latin1_swedish_ci' ENGINE=InnoDB; 
										

# ---------------------------------------------------------------------- #
# Modify Table service_provider                                          #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider ADD COLUMN LockAppWindow ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER GeneralManagerEmail,
							 ADD COLUMN LockAppWindowTime INT NOT NULL DEFAULT '60' AFTER LockAppWindow;
							 
							 
# ---------------------------------------------------------------------- #
# Add Table datatables_custom_default_columns                            #
# ---------------------------------------------------------------------- #
CREATE TABLE datatables_custom_default_columns (
												DatatablesCustomDefaultColumnsID INT(10) NOT NULL AUTO_INCREMENT, 
												UserID INT(10) NULL DEFAULT NULL, 
												PageID INT(10) NULL DEFAULT NULL, 
												ColumnDisplayString VARCHAR(500) NULL DEFAULT NULL, 
												ColumnOrderString VARCHAR(500) NULL DEFAULT NULL, 
												ColumnNameString VARCHAR(5000) NULL DEFAULT NULL, 
												PRIMARY KEY (DatatablesCustomDefaultColumnsID)
												) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=5;

												
# ---------------------------------------------------------------------- #
# Add Table deferred_time_slot                                           #
# ---------------------------------------------------------------------- #
CREATE TABLE deferred_time_slot ( 
								DeferredTimeSlotID INT(10) NOT NULL AUTO_INCREMENT, 
								ServiceProviderID INT(10) NULL DEFAULT '0', 
								EarliestWorkday TIME NULL DEFAULT NULL, 
								LatestWorkday TIME NULL DEFAULT NULL, 
								EarliestWeekend TIME NULL DEFAULT NULL, 
								LatestWeekend TIME NULL DEFAULT NULL, 
								PRIMARY KEY (DeferredTimeSlotID)
								) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

								
# ---------------------------------------------------------------------- #
# Add Table deferred_postcode                                            #
# ---------------------------------------------------------------------- #
CREATE TABLE deferred_postcode ( 
								DeferredPostcodeID INT(10) NOT NULL AUTO_INCREMENT, 
								Postcode VARCHAR(8) NULL DEFAULT NULL, 
								ServiceProviderID INT(10) NOT NULL DEFAULT '0', 
								DpType ENUM('Working','WeekEnd') NOT NULL DEFAULT 'Working', 
								PRIMARY KEY (DeferredPostcodeID) 
								) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.220');
