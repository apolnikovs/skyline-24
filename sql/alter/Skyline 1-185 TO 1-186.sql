# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.185');

# ---------------------------------------------------------------------- #
# Add table "shipping"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `shipping` CHANGE COLUMN `CourierID` `CourierID` INT(11) NULL DEFAULT NULL AFTER `JobID`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.186');
