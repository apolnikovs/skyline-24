# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.295');

# ---------------------------------------------------------------------- #
# Vyk Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE user_reports ADD COLUMN Schedule TEXT NULL DEFAULT NULL AFTER Structure;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.296');
