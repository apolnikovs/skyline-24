# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.203');

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `job` ADD COLUMN `ImeiNo` VARCHAR(20) NULL DEFAULT NULL AFTER `ServiceAction`;



# ---------------------------------------------------------------------- #
# Update Triggers                                                        #
# ---------------------------------------------------------------------- #

DROP TRIGGER IF EXISTS job_insert;

DELIMITER ;;

CREATE TRIGGER job_insert BEFORE INSERT ON job FOR EACH ROW 
BEGIN

        IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
        THEN SET NEW.ClosedDate = CURDATE();
        END IF;

END
;;

DELIMITER ;

DROP TRIGGER IF EXISTS job_update;

DELIMITER ;;

CREATE TRIGGER job_update BEFORE UPDATE ON job FOR EACH ROW 
BEGIN

        /*Insert closed date if Job Status is being changed to cancelled*/
        
        IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
        THEN SET NEW.ClosedDate = CURDATE();
        END IF;
        
        /*Audit logging begins here*/

        /*Parameter definitions*/
        /*CALL trigger_update([old data], [new data], [table name], [field name], [primary ID], [modified user ID]);*/

        /*StatusID*/
        CALL trigger_update(OLD.StatusID, NEW.StatusID, 'job', 'StatusID', OLD.JobID, NEW.ModifiedUserID);
        
        /*ProductID*/
        CALL trigger_update(OLD.ProductID, NEW.ProductID, 'job', 'ProductID', OLD.JobID, NEW.ModifiedUserID);
                
        /*ServiceBaseModel*/
        CALL trigger_update(OLD.ServiceBaseModel, NEW.ServiceBaseModel, 'job', 'ModelID', OLD.JobID, NEW.ModifiedUserID);

        /*ManufacturerID*/
        CALL trigger_update(OLD.ManufacturerID, NEW.ManufacturerID, 'job', 'ManufacturerID', OLD.JobID, NEW.ModifiedUserID);
                                                        
        /*ServiceBaseUnitType*/
        CALL trigger_update(OLD.ServiceBaseUnitType, NEW.ServiceBaseUnitType, 'job', 'ServiceBaseUnitType', OLD.JobID, NEW.ModifiedUserID);
        
        /*RepairType*/
        CALL trigger_update(OLD.RepairType, NEW.RepairType, 'job', 'RepairType', OLD.JobID, NEW.ModifiedUserID);

        /*ServiceTypeID*/
        CALL trigger_update(OLD.ServiceTypeID, NEW.ServiceTypeID, 'job', 'ServiceTypeID', OLD.JobID, NEW.ModifiedUserID);

        /*SerialNo*/
        CALL trigger_update(OLD.SerialNo, NEW.SerialNo, 'job', 'SerialNo', OLD.JobID, NEW.ModifiedUserID);
        
        /*ProductLocation*/
        CALL trigger_update(OLD.ProductLocation, NEW.ProductLocation, 'job', 'ProductLocation', OLD.JobID, NEW.ModifiedUserID);

        /*DateOfPurchase*/
        CALL trigger_update(OLD.DateOfPurchase, NEW.DateOfPurchase, 'job', 'DateOfPurchase', OLD.JobID, NEW.ModifiedUserID);
        
        /*OriginalRetailer*/
        CALL trigger_update(OLD.OriginalRetailer, NEW.OriginalRetailer, 'job', 'OriginalRetailer', OLD.JobID, NEW.ModifiedUserID);
        
        /*ReceiptNo*/
        CALL trigger_update(OLD.ReceiptNo, NEW.ReceiptNo, 'job', 'ReceiptNo', OLD.JobID, NEW.ModifiedUserID);

        /*Insurer*/
        CALL trigger_update(OLD.Insurer, NEW.Insurer, 'job', 'Insurer', OLD.JobID, NEW.ModifiedUserID);

        /*PolicyNo*/
        CALL trigger_update(OLD.PolicyNo, NEW.PolicyNo, 'job', 'PolicyNo', OLD.JobID, NEW.ModifiedUserID);

        /*AuthorisationNo*/
        CALL trigger_update(OLD.AuthorisationNo, NEW.AuthorisationNo, 'job', 'AuthorisationNo', OLD.JobID, NEW.ModifiedUserID);

        /*AgentRefNo*/
        CALL trigger_update(OLD.AgentRefNo, NEW.AgentRefNo, 'job', 'AgentRefNo', OLD.JobID, NEW.ModifiedUserID);
                                        
        /*Notes*/
        CALL trigger_update(OLD.Notes, NEW.Notes, 'job', 'Notes', OLD.JobID, NEW.ModifiedUserID);
        
        /*ReportedFault*/
        CALL trigger_update(OLD.ReportedFault, NEW.ReportedFault, 'job', 'ReportedFault', OLD.JobID, NEW.ModifiedUserID);
        
        /*ChargeableLabourCost*/
        CALL trigger_update(OLD.ChargeableLabourCost, NEW.ChargeableLabourCost, 'job', 'ChargeableLabourCost', OLD.JobID, NEW.ModifiedUserID);

        /*ChargeableLabourVATCost*/
        CALL trigger_update(OLD.ChargeableLabourVATCost, NEW.ChargeableLabourVATCost, 'job', 'ChargeableLabourVATCost', OLD.JobID, NEW.ModifiedUserID);
        
        /*ChargeablePartsCost*/
        CALL trigger_update(OLD.ChargeablePartsCost, NEW.ChargeablePartsCost, 'job', 'ChargeablePartsCost', OLD.JobID, NEW.ModifiedUserID);

        /*ChargeableDeliveryCost*/
        CALL trigger_update(OLD.ChargeableDeliveryCost, NEW.ChargeableDeliveryCost, 'job', 'ChargeableDeliveryCost', OLD.JobID, NEW.ModifiedUserID);

        /*ChargeableDeliveryVATCost*/
        CALL trigger_update(OLD.ChargeableDeliveryVATCost, NEW.ChargeableDeliveryVATCost, 'job', 'ChargeableDeliveryVATCost', OLD.JobID, NEW.ModifiedUserID);
        
        /*ChargeableVATCost*/
        CALL trigger_update(OLD.ChargeableVATCost, NEW.ChargeableVATCost, 'job', 'ChargeableVATCost', OLD.JobID, NEW.ModifiedUserID);
        
        /*ChargeableSubTotal*/
        CALL trigger_update(OLD.ChargeableSubTotal, NEW.ChargeableSubTotal, 'job', 'ChargeableSubTotal', OLD.JobID, NEW.ModifiedUserID);
        
        /*ChargeableTotalCost*/
        CALL trigger_update(OLD.ChargeableTotalCost, NEW.ChargeableTotalCost, 'job', 'ChargeableTotalCost', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddCompanyName*/
        CALL trigger_update(OLD.ColAddCompanyName, NEW.ColAddCompanyName, 'job', 'ColAddCompanyName', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddBuildingNameNumber*/
        CALL trigger_update(OLD.ColAddBuildingNameNumber, NEW.ColAddBuildingNameNumber, 'job', 'ColAddBuildingNameNumber', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddStreet*/
        CALL trigger_update(OLD.ColAddStreet, NEW.ColAddStreet, 'job', 'ColAddStreet', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddLocalArea*/
        CALL trigger_update(OLD.ColAddLocalArea, NEW.ColAddLocalArea, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddTownCity*/
        CALL trigger_update(OLD.ColAddTownCity, NEW.ColAddTownCity, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddCountyID*/
        CALL trigger_update(OLD.ColAddCountyID, NEW.ColAddCountyID, 'job', 'ColAddCountyID', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddCountryID*/
        CALL trigger_update(OLD.ColAddCountryID, NEW.ColAddCountryID, 'job', 'ColAddCountryID', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddPostcode*/
        CALL trigger_update(OLD.ColAddPostcode, NEW.ColAddPostcode, 'job', 'ColAddPostcode', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddEmail*/
        CALL trigger_update(OLD.ColAddEmail, NEW.ColAddEmail, 'job', 'ColAddEmail', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddPhone*/
        CALL trigger_update(OLD.ColAddPhone, NEW.ColAddPhone, 'job', 'ColAddPhone', OLD.JobID, NEW.ModifiedUserID);
        
        /*ColAddPhoneExt*/
        CALL trigger_update(OLD.ColAddPhoneExt, NEW.ColAddPhoneExt, 'job', 'ColAddPhoneExt', OLD.JobID, NEW.ModifiedUserID);

        /*Accessories*/
        CALL trigger_update(OLD.Accessories, NEW.Accessories, 'job', 'Accessories', OLD.JobID, NEW.ModifiedUserID);
        
        /*UnitCondition*/
        CALL trigger_update(OLD.UnitCondition, NEW.UnitCondition, 'job', 'UnitCondition', OLD.JobID, NEW.ModifiedUserID);
        
        /*ServiceProviderID*/
        CALL trigger_update(OLD.ServiceProviderID, NEW.ServiceProviderID, 'job', 'ServiceProviderID', OLD.JobID, NEW.ModifiedUserID);
        
END
;;

DELIMITER ;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.204');
