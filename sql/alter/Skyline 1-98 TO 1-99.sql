# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-18 14:40                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.98');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer` ADD COLUMN `StartLocation` ENUM('Home','Office','Other');

ALTER TABLE `service_provider_engineer` ADD COLUMN `EndLocation` ENUM('Home','Office','Other');

ALTER TABLE `service_provider_engineer` ADD COLUMN `StartHomePostcode` VARCHAR(10);

ALTER TABLE `service_provider_engineer` ADD COLUMN `StartOfficePostcode` VARCHAR(10);

ALTER TABLE `service_provider_engineer` ADD COLUMN `StartOtherPostcode` VARCHAR(10);

ALTER TABLE `service_provider_engineer` ADD COLUMN `EndHomePostcode` VARCHAR(10);

ALTER TABLE `service_provider_engineer` ADD COLUMN `EndOfficePostcode` VARCHAR(10);

ALTER TABLE `service_provider_engineer` ADD COLUMN `EndOtherPostcode` VARCHAR(10);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.99');
