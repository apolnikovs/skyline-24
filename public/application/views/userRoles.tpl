{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $UserRoles}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}

    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
        
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
    var $jobTypes = new Array();
       
        {foreach from=$allBrandsJobTypes item=bJobTypes key=brandId}
           
           $jobTypes["{$brandId}"] = [        
            
            {foreach from=$bJobTypes item=jTypes} 
                   ["{$jTypes.JobTypeID}", "{$jTypes.Type}"],
            {/foreach}
                
           ]
           
        {/foreach}
        
    function gotoEditPage($sRow) {

        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');

    }

    function inactiveRow(nRow, aData) {
    
        
        if(aData[5])
        {
                
             $.post("{$_subdomain}/LookupTables/roles/tagged/"+aData[5]+"/",         
                                                 '',      
                                                 function(data){
                                                         
                                                         var $taggedCount  = eval("(" + data + ")");
                                                            
                                                         $('td:eq(4)', nRow).html( $taggedCount );   
                                                        
                                                 });   
                
        }        
    
          
        if (aData[3]==$statuses[1][1]) {  
            $(nRow).addClass("inactive");

            $('td:eq(2)', nRow).html( $statuses[1][0] );
        } else {
            $(nRow).addClass("");
                  $('td:eq(2)', nRow).html( $statuses[0][0] );
        }

    }

    function gotoInsertPage($sRow) { 

        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
    }

    $(document).ready(function() {

                  //change handler for brand dropdown box.
                  $(document).on('change', '#BrandID', function() {

                            $jobTypesDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                            var $brandId = $("#BrandID").val();

                            if($brandId && $brandId!='') {
                                $brandJobTypes = $jobTypes[$brandId];
                                if($brandJobTypes) {
                                        for(var $i=0;$i<$brandJobTypes.length;$i++) {
                                            $jobTypesDropDownList += '<option value="'+$brandJobTypes[$i][0]+'" >'+$brandJobTypes[$i][1]+'</option>';
                                        }
                                    }

                                $("#JobTypeID").html($jobTypesDropDownList);     
                            }


                    });  
                    



                //Click handler for finish button.
                $(document).on('click', '#finish_btn', function() { 
                    $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');
                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UPA";
                     
                     {else}

                         var displayButtons =  "UP";

                     {/if} 
                     


                    
                    $('#STResults').PCCSDataTable( {
                            "aoColumns": [ 
                                /* RoleID */  {  "bVisible":    false },    
                                /* Role Name */   null,
                                /* UserType */  null,
                                /* Status */  null,
                                /* Comment */ { 'bSortable' : false, 'sWidth' : '350px'},
                                /* Tagged */  {  "bSortable":    false }
                            ],     
                            
                            "aaSorting": [[ 1, "asc" ]],
                            
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/LookupTables/roles/insert/',
                            createDataUrl:   '{$_subdomain}/LookupTables/ProcessData/UserRoles/',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                                Name: { required: true },
                                                UserType: { required: true },
                                                Status: { required: true }     
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    Name:
                                                        {
                                                            required: "{$page['Text']['name_error']|escape:'html'}"
                                                        },
                                                    UserType:
                                                        {
                                                            required: "{$page['Text']['usertype_error']|escape:'html'}"
                                                        }, 
                                                    Status:
                                                        {
                                                            required: "{$page['Text']['status_error']|escape:'html'}"
                                                        }    
                                              },                     
                            
                            popUpFormWidth:  1000,
                            popUpFormHeight: '800px',
                            
                            pickButtonId:"copy_btn",
                            pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                            pickCallbackMethod: "gotoInsertPage",
                            colorboxForceClose: false,
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/roles/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/UserRoles/',
                            formUpdateButton:'update_save_btn',
                            
                            colorboxFormId:  "STForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'STResultsPanel',
                            htmlTableId:     'STResults',
                            htmlChildTableId: 'search_results_test',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/UserRoles/fetch/{$RoleStatus}',
                            formCancelButton:'cancel_btn',
			    //pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            //fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli', 
                            bottomButtonsDivId:'dataTables_command'


                        });
                        
            

                $("input[name='RoleStatus']").change(function(){
                    
                    document.location.href = "{$_subdomain}/LookupTables/Roles/RoleStatus="+$(this).val();
                    
                });
                        

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>

    <div class="main" id="home" >

        <div class="LTTopPanel" >
            <form id="countyForm" name="countyForm" method="post"  action="#" class="inline">

                <fieldset>
                <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                <p>
                    <label>{$page['Text']['description']|escape:'html'}</label>
                </p> 

                </fieldset> 


            </form>
        </div>  


        <div class="LTResultsPanel" id="STResultsPanel" >
            
            <input name="RoleStatus" type="radio" value="Active" {if $RoleStatus eq 'Active'} checked="checked" {/if} > {$page['Text']['active']|escape:'html'}
            <input name="RoleStatus" type="radio" value="In-active" {if $RoleStatus eq 'In-active'} checked="checked" {/if} > {$page['Text']['in_active']|escape:'html'}
            <input name="RoleStatus" type="radio" value="Both" {if $RoleStatus eq 'Both'} checked="checked" {/if} > {$page['Text']['both']|escape:'html'}
            
            <table id="STResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                <thead>
                        <tr>
                                <th></th> 
                                <th width="25%" title="{$page['Labels']['role_name']|escape:'html'}" >{$page['Labels']['role_name']|escape:'html'}</th>
                                <th width="20%" title="{$page['Labels']['user_type']|escape:'html'}"  >{$page['Labels']['user_type']|escape:'html'}</th>
                                <th width="10%" title="{$page['Labels']['status']|escape:'html'}" >{$page['Labels']['status']|escape:'html'}</th>
                                <th width="37%" title="{$page['Labels']['comment']|escape:'html'}" >{$page['Labels']['comment']|escape:'html'}</th>
                                <th width="8%" title="{$page['Labels']['tagged']|escape:'html'}" >{$page['Labels']['tagged']|escape:'html'}</th>
                        </tr>
                </thead>
                <tbody>

                </tbody>
            </table>  
        </div>        

                <div class="bottomButtonsPanel" >
                    <hr />
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



