
    



    <script type="text/javascript" >
        $(document).ready(function() {
        $("#CountryID").combobox();
        $("#PrimarySupplierID").combobox();
        $("#BouncerPeriodBasedOn").combobox();
        $("#ForceStatusID").combobox();
        $("#JobFaultCodeID").combobox();
        $("#PartFaultCodeID").combobox();
        $("#EDIExportFormatID").combobox();
        $("#AssociatedManufacturers").combobox();
    });
    $(function() {
        $( "#tabs" ).tabs();
    });
    
   
    $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });
      
     
      

         $(function() {
    $( "#tabs" ).tabs();
  });
    var oAuthTable;
    var savedHeight=null;
    {if $datarow.ManufacturerID eq '' || $datarow.ManufacturerID eq '0'}
    var AuthServiceProvidersSavePending = false;
    {/if}
       
    function PostAuthMan( manufacturer_key ) {
        {if $datarow.ManufacturerID eq '' || $datarow.ManufacturerID eq '0'}
        {* Manufacturer Insert mode, so only do update if a Save is Pending *}
        if (!AuthServiceProvidersSavePending) return;
        {/if}  
        var post_data = $('input',oAuthTable.fnGetNodes()).serialize();       
        if (post_data == '') 
            post_data = 'mfg='+manufacturer_key;
        else 
            post_data += '&mfg='+manufacturer_key;
        $.ajax({
                type: "POST",
                url: '{$_subdomain}'+"/ProductSetup/UpdateServiceProviderAuth",
                data: post_data,
                success: function(msg){
                                    // save updated auth and checked values                                   
                                    $('.auth',oAuthTable.fnGetNodes()).each(
                                              function() {
                                                  var id = $(this).val();
                                                  var data = oAuthTable.fnGetData();
                                                  for (var i=0; i<data.length; i++) {
                                                      if (data[i][3] == id) {
                                                          if ($(this).is(':checked')) {
                                                              oAuthTable.fnUpdate('1', i, 4, false);
                                                          } else {
                                                              oAuthTable.fnUpdate('0', i, 4, false);
                                                          }
                                                          break;
                                                       }
                                                  }
                                              }
                                         );
                                    $('.check',oAuthTable.fnGetNodes()).each(
                                              function() {
                                                  var id = $(this).val();
                                                  var data = oAuthTable.fnGetData();
                                                  for (var i=0; i<data.length; i++) {
                                                      if (data[i][4] == id) {
                                                          if ($(this).is(':checked')) {
                                                              oAuthTable.fnUpdate('1', i, 5, false);
                                                          } else {
                                                              oAuthTable.fnUpdate('0', i, 5, false);
                                                          }
                                                          break;
                                                       }
                                                  }
                                              }
                                         );  
                         }
           });
    }
        
	$(document).ready(function() {
        
        if($('#ValidateDateCode').attr('checked')=="checked"){ $('#addFieldsDop').show()}else{ $('#addFieldsDop').hide() }
        if($('#AutoFinalRejectClaimsNotResubmitted').attr('checked')=="checked"){ $('#DaysToResubmitP').show()}else{ $('#DaysToResubmitP').hide() }
        if($('#LimitResubmissions').attr('checked')=="checked"){ $('#LimitResubmissionsToP').show()}else{ $('#LimitResubmissionsToP').hide() }
        
        
        
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function() {
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                }); 
                
		
	    if($("input[name=AuthorisationRequired]:checked").val() == "Yes") {
		$("#authManager").show();
	    } else {
		$("#authManager").hide();
	    }
	
	    $(document).on("change", "input[name=AuthorisationRequired]", function() {
		if($("input[name=AuthorisationRequired]:checked").val() == "Yes") {
		    $("#authManager").show();
		} else {
		    $("#authManager").hide();
		}
		$.colorbox.resize();
	    });
            
            oAuthTable = $('#AuthorisedServiceProvidersTable').dataTable( {
                       "aoColumns": [
                            null,
			    { sWidth: '72px', bSortable: true, 
                              mRender: function ( data, type, row ) {
                                       if (type === 'filter' || type === 'sort' || type === 'type') {
                                           return data;
                                       }
                                       if (data == 1)
                                           return '<input type="checkbox" checked="checked" name="auth[]" value="'+row[3]+'" class="auth-tag" checked />';
                                       else
                                           return '<input type="checkbox" checked="checked" name="auth[]" value="'+row[3]+'" class="auth-tag" />';
                                   } 
                            },
                            { sWidth: '72px', bSortable: true,
                              mRender: function ( data, type, row ) {
                                       if (type === 'filter' || type === 'sort' || type === 'type') {
                                           return data;
                                       }
                                       if (data == 1)
                                           return '<input type="checkbox" checked="checked" name="check[]" value="'+row[3]+'" class="check-tag" checked />';
                                       else 
                                           return '<input type="checkbox" checked="checked" name="check[]" value="'+row[3]+'" class="check-tag"  />';
                                   }                        
                            },
                            { bVisible: false },
                            { bVisible: false },
                            { bVisible: false }
		        ],
                        "bAutoWidth": false,
                        "bInfo": false,
                        "bLengthChange": true,
                        "iDisplayLength": 10,
                        "bPaginate": true,
                        "sPaginationType": "full_numbers",
                        "sDom": "ftlp"
                    } );
                    
             $(document).on('change', '#AuthorisedServiceProvidersTable_length select', 
                                function() {
                                    var h = $('#AuthoriseServiceProvidersFormPanel').height()+135;
                                    $('#colorbox').colorbox.resize({ height: h });               
                            });
               
             $(document).on('click', '#AuthManButton', 
                                function() {
                                    if (savedHeight==null) savedHeight = $('#cboxWrapper').height();
                                    $('#ManufacturersFormPanel').hide();
                                    var h = $('#AuthoriseServiceProvidersFormPanel').show().height() + 135;                                    
                                    $('#colorbox').colorbox.resize({ height: h });               
                            }); 
                
              $(document).on('click', '#authman_save_btn', 
                                function() { 
                                    {if $datarow.ManufacturerID neq '' && $datarow.ManufacturerID neq '0'}
                                    {* Manufacturer Update mode.... *}
                                    PostAuthMan( {$datarow.ManufacturerID} ); 
                                    {else}
                                    {* Mark as save pending if Manufacturer Insert mode.... *}
                                    AuthServiceProvidersSavePending = true;
                                    {/if}
                                    $('#AuthoriseServiceProvidersFormPanel').hide();
                                    $('#ManufacturersFormPanel').show();
                                    $('#colorbox').colorbox.resize({ height: savedHeight }); 
                                    return false;
                            });    
                            
              $(document).on('click', '#authman_cancel_btn', 
                                function() { 
                                   {if $datarow.ManufacturerID eq '' || $datarow.ManufacturerID eq '0'}
                                   {* Cancel Save Pending if Manufacturer Insert mode... *}
                                   AuthServiceProvidersSavePending = false;
                                   {/if}
                                    // reset auth and checked values
                                    //console.log('reset auth and checked values');
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        var orig_auth =  data[i][4];
                                        var orig_check = data[i][5];
                                        oAuthTable.fnUpdate(orig_auth, i, 1, false);
                                        oAuthTable.fnUpdate(orig_check, i, 2, false);
                                    }
                                    $('#AuthoriseServiceProvidersFormPanel').hide();
                                    $('#ManufacturersFormPanel').show();
                                    $('#colorbox').colorbox.resize({ height: savedHeight });
                                    return false;
                            });
                            
              $(document).on('click', '#authman_show_tagged', 
                                function() {                                   
                                    if ($(this).is(':checked')) { 
                                        oAuthTable.fnFilter('1',1);
                                    } else {
                                       oAuthTable.fnFilter('',1);
                                    }
                            });
                            
              $(document).on('click', '#checkman_show_tagged', 
                                function() {                                   
                                    if ($(this).is(':checked')) { 
                                        oAuthTable.fnFilter('1',2);
                                    } else {
                                       oAuthTable.fnFilter('',2);
                                    }
                            });
                            
              $(document).on('click', '.auth-tag',                                
                                function() {  
                                    var id = $(this).val();
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        if (data[i][3] == id) {
                                            if ($(this).is(':checked')) {
                                                oAuthTable.fnUpdate('1', i, 1, false);
                                            } else {
                                                oAuthTable.fnUpdate('0', i, 1, false);
                                            }
                                            break;
                                        }
                                    }
                            });
                            
             $(document).on('click', '.check-tag',                                
                                function() {  
                                    var id = $(this).val();
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        if (data[i][3] == id) {
                                            if ($(this).is(':checked')) {
                                                oAuthTable.fnUpdate('1', i, 2, false);
                                            } else {
                                                oAuthTable.fnUpdate('0', i, 2, false);
                                            }
                                            break;
                                        }
                                    }
                            });
				
	     
          
				
	   });     
        
       
        
    </script>
   
    
    <div id="ManufacturersFormPanel" class="SystemAdminFormPanel" >
    
                <form id="ManufacturersForm" name="ManufacturersForm" method="post" enctype="multipart/form-data" action="{$_subdomain}/ProductSetup/saveManufacturer" class="inline" >
       
                    <input type="hidden" name="mode" value="{$mode}">
                    <input type="hidden" name="ServiceProviderID" value="{$ServiceProviderID}">
                    <input type="hidden" name="ManufacturerID"  value="{if isset($id)}{$id}{/if}" >
                    <input type="hidden" name="CreatedDate"  value="{$datarow.CreatedDate|default:'0000-00-00 00:00:00'}" >
                    <input type="hidden" name="hiddenManufacturerLogo" id="hiddenManufacturerLogo"  value="{$datarow.ManufacturerLogo|escape:'html'}" >
                    
                <fieldset>
		    {* Updated by Praveen Kumar N , If editing an existing record, the form title is to change to "Edit Manufacturer" [START] *}
                    <legend title="" >{if isset($id)} {$page['Text']['update_page_legend']|escape:'html'} {else}  {$page['Text']['insert_page_legend']|escape:'html'} {/if}</legend>
                    {* [END] *}  
                    <p><label id="suggestText" ></label></p>
                            <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Company Details</a></li>
    <li><a href="#tabs-2">General Defaults</a></li>
    <li><a href="#tabs-3">Warranty Claim Defaults</a></li>
    <li><a {if $datarow.ManufacturerID!=''}onclick="loadAssocList()" {/if} href="#tabs-4">Assoc. Manufact</a></li>
    
  </ul>
  <div id="tabs-1" class="SystemAdminFormPanel inline">
                          <p>
                                <label ></label>
                                <span  class="topText cardTopLabel saFormSpan">Fields marked with a <sup>*</sup> are compulsory</span>
                            </p>
                       <br/>
                         
                         
                       <p style="position:absolute;">
                           <img src="{$_subdomain}/{$manufacturerLogosPath}{$datarow.ManufacturerLogo|default:'no_logo.png'}" id="mlogo" style="width:100px;">
                       </p>
                           
                                
                                <p>
                                <label class="cardLabel" for="ManufacturerName" >{$page['Labels']['name']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                                <input  type="text" class="text"  name="ManufacturerName" value="{$datarow.ManufacturerName|escape:'html'}" id="ManufacturerName" >
                                </p>
                                
                                <p>
                                    <label class="cardLabel" for="Acronym" >{$page['Labels']['acronym']|escape:'html'}:{*<sup>*</sup>*}</label>
                                    &nbsp;&nbsp; 
                                    <input  type="text" maxlength="30" class="text"  name="Acronym" value="{$datarow.Acronym|escape:'html'}" id="Acronym" >
                                     <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerAnconymHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                                </p>
                                
                           
                        
                         
                         
                         
                         <p>
                            <label class="cardLabel" for="ManufacturerLogo" >{$page['Labels']['logo_location']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                             <input  type="file" class="text" style="width:225px"  name="ManufacturerLogo"  id="ManufacturerLogo" ><img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerLogoHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                              
                             {if $datarow.ManufacturerLogo neq ''} 
                             
                              &nbsp;&nbsp;  <a href="{$_subdomain}/{$manufacturerLogosPath}{$datarow.ManufacturerLogo|escape:'html'}" target="_blank" >{$page['Text']['view']|escape:'html'}</a>  
                                 
                             {/if}
                            
                             <input  type="hidden" class="text"  name="UploadedManufacturerLogo"  id="UploadedManufacturerLogo" value="{$UploadedManufacturerLogo}" >
                        
                         </p>
                         
			 
			
			   
                            
                           
                             
                            <p>
                                <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>

                                &nbsp;&nbsp; 

                                    

					<span class="formRadioCheckText"  class="saFormSpan">
					    <input  type="checkbox" name="Status"  value="In-active" {if $datarow.Status eq 'In-active'}checked="checked"{/if}  /><label >Inactive</label>&nbsp;
					</span>

                                    
                            </p>
                            
                     
                          {if isset($datarow.ApproveStatus)&&$activeUser=="SuperAdmin"&&$datarow.ApproveStatus!="Approved"}
                         <p>
                                <label class="cardLabel" for="ApproveStatus" >Account Type:</label>

                                &nbsp;&nbsp; 

                                   

					<span class="formRadioCheckText"  class="saFormSpan">
					    <input  type="radio" name="ApproveStatus"  value="Approved"   /><label >Global</label>&nbsp;
					    <input  type="radio" name="ApproveStatus"  value="NotApproved"   /><label >{$ancronym} Specific</label>&nbsp;
					</span>

                                     
                            </p>
                            {/if}
                         
                         
                             <p>
                            <label class="cardLabel" for="PostalCode">Postcode:</label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostCode" id="PostalCode" value="{if isset($data)}{$data['PostCode']}{/if}" >&nbsp;

                            <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                        </p>
                        <p id="selectOutput" ></p>


                        <p>
                            <label class="cardLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber}" id="BuildingNameNumber" >          
                        </p>

                        <p>
                            <label class="cardLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{$datarow.Street}" id="Street" >          
                        </p>

                        <p>
                            <label class="cardLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{$datarow.LocalArea}" id="LocalArea" >          
                        </p>


                        <p>
                            <label class="cardLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{$datarow.TownCity}" id="TownCity" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:</label>
                            &nbsp;&nbsp; 
                                <select name="CountryID" id="CountryID" class="text" >
                                <option value="" {if isset($datarow)&& $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if isset($datarow)&& $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                        <hr>
                        
                        <p>
                            <label class="cardLabel" for="TelNoSwitchboard" >Switchboard No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TelNoSwitchboard" value="{$datarow.TelNoSwitchboard}" id="TelNoSwitchboard" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="TelNoSpares" >Spares No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TelNoSpares" value="{$datarow.TelNoSpares}" id="TelNoSpares" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="TelNoWarranty" >Warranty No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TelNoWarranty" value="{$datarow.TelNoWarranty}" id="TelNoWarranty" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="TelNoTechnical" >Technical No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TelNoTechnical" value="{$datarow.TelNoTechnical}" id="TelNoTechnical" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="FaxNo" >Fax No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="FaxNo" value="{$datarow.FaxNo}" id="FaxNo" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="EmailAddress" >General Email Address:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="EmailAddress" value="{$datarow.EmailAddress}" id="EmailAddress" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="WarrantyEmailAddress" >Warranty Email Address:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="WarrantyEmailAddress" value="{$datarow.WarrantyEmailAddress}" id="WarrantyEmailAddress" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="SparesEmailAddress" >Spares Email Address:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="SparesEmailAddress" value="{$datarow.SparesEmailAddress}" id="SparesEmailAddress" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="ContactName" >Contact Name:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="ContactName" value="{$datarow.ContactName}" id="ContactName" >          
                        </p>
                        
                     <p class="topText cardTopLabel">
                            <button type="button" id="AuthManButton" style="margin-left:150px;width:310px;" class="gplus-blue centerBtn" >{$page['Labels']['authorised_service_providers']|escape:'html'}</button>  <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AuthorisedSPHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                        </p>
                                 </div>     
                <div id="tabs-2" class="SystemAdminFormPanel inline">
                    
			    <p>
                                    <label class="cardLabel" for="AccountNo" >Primary Account No:</label>
                                    &nbsp;&nbsp; 
                                    <input  type="text"  class="text"  name="AccountNo" value="{$datarow.AccountNo|escape:'html'}" id="AccountNo" >
                            </p>
                             <p>
                            <label class="cardLabel" for="SageAccountNo" >Sage Account No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="SageAccountNo" value="{$datarow.SageAccountNo}" id="SageAccountNos" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="VATNo" >VAT Number:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="VATNo" value="{$datarow.VATNo}" id="VATNo" >          
                        </p>
                    <p>
                            <label class="cardLabel" for="PrimarySupplierID" >Primary Supplier:</label>
                            &nbsp;&nbsp; 
                                <select name="PrimarySupplierID" id="PrimarySupplierID" class="text" >
                                <option value="" {if isset($datarow.PrimarySupplierID)&& $datarow.PrimarySupplierID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $suppliers as $s}

                                    <option value="{$s.SupplierID}" {if isset($datarow.PrimarySupplierID)&& $datarow.PrimarySupplierID eq $s.SupplierID}selected="selected"{/if}>{$s.CompanyName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                        <p>
                            <label class="cardLabel" for="UseProductCode" >Use Product Code:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.UseProductCode=="Yes"}checked="checked"{/if} name="UseProductCode" value="{$datarow.UseProductCode}" id="UseProductCode" >
			    <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerUseProductCodeHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                        </p>
                         <p>
                            <label class="cardLabel" for="UseMSNNo" >Use MSN Number:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.UseMSNNo=="Yes"}checked="checked"{/if} name="UseMSNNo" value="{$datarow.UseMSNNo}" id="UseMSNNo" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerUseMsnNoHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                         </p>
                        
                        <p style="display:none">
                            <label class="cardLabel" for="UseMSNFormat" >Use MSN Format:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.UseMSNFormat=="Yes"}checked="checked"{/if} name="UseMSNFormat" value="{$datarow.UseMSNFormat}" id="UseMSNFormat" >
                            
                        </p>
                        
                         <p>
                            <label class="cardLabel" for="MSNFormat" >MSN Format:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="MSNFormat" value="{$datarow.MSNFormat}" id="MSNFormat" >   
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerMsnFormatHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                        </p>
                        <p>
                            <label class="cardLabel" for="UseQA" >Use QA:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.UseQA=="Yes"}checked="checked"{/if} name="UseQA" value="{$datarow.UseQA}" id="UseQA" >       
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerUseQAHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="UsePreQA" >Use Pre-QA:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.UsePreQA=="Yes"}checked="checked"{/if} name="UsePreQA" value="{$datarow.UsePreQA}" id="UsePreQA" >
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerUsePreQAHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="QAExchangeUnits" >QA Exchange Units:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.QAExchangeUnits=="Yes"}checked="checked"{/if} name="QAExchangeUnits" value="{$datarow.QAExchangeUnits}" id="QAExchangeUnits" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerQAExUnitsHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="CompleteOnQAPass" >Completion At QA Pass:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.CompleteOnQAPass=="Yes"}checked="checked"{/if} name="CompleteOnQAPass" value="{$datarow.CompleteOnQAPass}" id="CompleteOnQAPass" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerComplOnQaPassHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="ValidateNetworkQA" >Validate Network At QA:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ValidateNetworkQA=="Yes"}checked="checked"{/if} name="ValidateNetworkQA" value="{$datarow.ValidateNetworkQA}" id="ValidateNetworkQA" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerValidateNetwrokQAHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                      <p>
                            <label class="cardLabel" for="" >Create Technical Report:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.CreateTechnicalReport=="Yes"}checked="checked"{/if} name="CreateTechnicalReport" value="{$datarow.CreateTechnicalReport}" id="CreateTechnicalReport" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerCreateTehReportHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    
                      <p>
                            <label class="cardLabel" for="CreateServiceHistoryReport" >Create Service History Report:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.CreateServiceHistoryReport=="Yes"}checked="checked"{/if} name="CreateServiceHistoryReport" value="{$datarow.CreateServiceHistoryReport}" id="CreateServiceHistoryReport" >          
                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerCreateServiceHisatoyReportHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="ExcludeFromBouncerTable" >Exclude From Bouncer Table:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ExcludeFromBouncerTable=="Yes"}checked="checked"{/if} name="ExcludeFromBouncerTable" value="{$datarow.ExcludeFromBouncerTable}" id="ExcludeFromBouncerTable" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerExcludeFromBouncerHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="CopyDOPFromBouncerJob" >Copy DOP from Bouncer Job:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.CopyDOPFromBouncerJob=="Yes"}checked="checked"{/if} name="CopyDOPFromBouncerJob" value="{$datarow.CopyDOPFromBouncerJob}" id="CopyDOPFromBouncerJob" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerCopyDopFromBouncerHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="BouncerPeriod" >Bouncer Period:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="BouncerPeriod" value="{$datarow.BouncerPeriod}" id="BouncerPeriod" > 
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerBouncerPeriodHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="BouncerPeriodBasedOn" >Bouncer Period Based On:</label>
                            &nbsp;&nbsp; 
                                <select name="BouncerPeriodBasedOn" id="BouncerPeriodBasedOn" class="text" >
                                <option value="" {if isset($datarow)&& $datarow.BouncerPeriodBasedOn eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $BouncerPeriodBasedOn as $s}

                                    <option value="{$s}" {if isset($datarow)&& $datarow.BouncerPeriodBasedOn eq $s}selected="selected"{/if}>{$s|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                     </p>
                     <hr>
                     <p>
			 <span  class="topText cardTopLabel saFormSpan" style="font-weight:bold;">Compulsory Fields  <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerComplHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" ></span>
                     </p>
                        
                      <p>
                            <label class="cardLabel" for="CircuitRefReq" >Circuit Ref:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.CircuitRefReq=="Yes"}checked="checked"{/if} name="CircuitRefReq" value="{$datarow.CircuitRefReq}" id="CircuitRefReq" >          
                      </p>
                      <p>
                            <label class="cardLabel" for="ExtendedGuaranteeNoReq" >Extended Guarantee number:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ExtendedGuaranteeNoReq=="Yes"}checked="checked"{/if} name="ExtendedGuaranteeNoReq" value="{$datarow.ExtendedGuaranteeNoReq}" id="ExtendedGuaranteeNoReq" >          
                      </p>
                       <p>
                            <label class="cardLabel" for="FaultCodesReq" >Fault Codes:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.FaultCodesReq=="Yes"}checked="checked"{/if} name="FaultCodesReq" value="Yes" id="FaultCodesReq" >          
                        </p>
                      <p>
                            <label class="cardLabel" for="FirmwareReq" >Firmware:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.FirmwareReq=="Yes"}checked="checked"{/if} name="FirmwareReq" value="{$datarow.FirmwareReq}" id="FirmwareReq" >          
                        </p>
                      <p>
                            <label class="cardLabel" for="InvoiceNoReq" >Invoice No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.InvoiceNoReq=="Yes"}checked="checked"{/if} name="InvoiceNoReq" value="{$datarow.InvoiceNoReq}" id="InvoiceNoReq" >          
                        </p>
                      <p>
                            <label class="cardLabel" for="OriginalRetailerReq" >Original Retailer:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.OriginalRetailerReq=="Yes"}checked="checked"{/if} name="OriginalRetailerReq" value="{$datarow.OriginalRetailerReq}" id="OriginalRetailerReq" >          
                        </p>
                       <p>
                            <label class="cardLabel" for="PartNoReq" >Part No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.PartNoReq=="Yes"}checked="checked"{/if} name="PartNoReq" value="{$datarow.PartNoReq}" id="PartNoReq" >          
                        </p>
                      <p>
                            <label class="cardLabel" for="PolicyNoReq" >Policy No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.PolicyNoReq=="Yes"}checked="checked"{/if} name="PolicyNoReq" value="{$datarow.PolicyNoReq}" id="PolicyNoReq" >          
                        </p>
                      <p>
                            <label class="cardLabel" for="ReferralNoReq" >Referral No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ReferralNoReq=="Yes"}checked="checked"{/if} name="ReferralNoReq" value="{$datarow.ReferralNoReq}" id="ReferralNoReq" >          
                        </p>
                        <p style="margin-top:10px; margin-bottom:10px;" class="text" >
				<label class="cardLabel" for="AuthorisationRequired" >Repair Authorisation Required:</label>
				&nbsp;&nbsp; 
				
                                <input  type="checkbox" name="AuthorisationRequired" value="Yes" {if $datarow.AuthorisationRequired == "Yes"}checked="checked"{/if} />
				
				
				    
				
				{if $datarow.AuthReqChangedDate}
				  &nbsp;&nbsp;<span  class="saFormSpan">Last change {$datarow.AuthReqChangedDate}</span>
				{/if}
                                 <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerAuthRequiredHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
			    </p>
                            <div id="authManager" {if $datarow.AuthorisationRequired == "No"}style="display:none"{/if}>
				<p>
				    <label class="cardLabel" for="AuthorisationRequired">Authorisation Manager:</label>
				    &nbsp;&nbsp; 
				    <input type="text" class="text" name="AuthorisationManager" value="{$datarow.AuthorisationManager|escape:'html'}" id="AuthorisationManager">
				</p>
				<p>
				    <label class="cardLabel" for="AuthorisationRequired">Email Address:</label>
				    &nbsp;&nbsp; 
				    <input type="text" class="text" name="AuthorisationManagerEmail" value="{$datarow.AuthorisationManagerEmail|escape:'html'}" id="AuthorisationManagerEmail">
				</p>
			    </div>
                         <p>
                            <label class="cardLabel" for="SerialNoReq" >Serial No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.SerialNoReq=="Yes"}checked="checked"{/if} name="SerialNoReq" value="{$datarow.SerialNoReq}" id="SerialNoReq" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="WarrantyLabourRateReqOn" >Force warranty labour rate on:</label>
                            &nbsp;&nbsp; <input {if $datarow.WarrantyLabourRateReqOn=="None"|| $datarow.WarrantyLabourRateReqOn==""}checked=checked{/if}  type="radio"  name="WarrantyLabourRateReqOn" value="None" id="WarrantyLabourRateReqOn1" >    <label class="saFormSpan">Never</label> 
                            &nbsp;&nbsp; <input {if $datarow.WarrantyLabourRateReqOn=="Booking"}checked=checked{/if}  type="radio"  name="WarrantyLabourRateReqOn" value="Booking" id="WarrantyLabourRateReqOn1" > <label class="saFormSpan">On Booking</label>         
                            &nbsp;&nbsp; <input {if $datarow.WarrantyLabourRateReqOn=="Completion"}checked=checked{/if}  type="radio"  name="WarrantyLabourRateReqOn" value="Completion" id="WarrantyLabourRateReqOn1" > <label class="saFormSpan">On Completion</label>         
                                 
                        </p>
                        
                    </div>
                <div id="tabs-3" class="SystemAdminFormPanel inline">
                    <p>
                            <label class="cardLabel" for="WarrantyAccountNo" >Warranty Account No:</label>
                            &nbsp;&nbsp; <input   type="text" class="text"   name="WarrantyAccountNo" value="{$datarow.WarrantyAccountNo}" id="WarrantyAccountNo" >          
                    </p>
                     <p>
                            <label class="cardLabel" for="StartClaimNo" >Start Claim No:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="StartClaimNo" value="{$datarow.StartClaimNo}" id="StartClaimNo" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerStartClaimNoHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                     </p> 
                    <p>
                            <label class="cardLabel" for="NoWarrantyInvoices" >Produce Warranty Invoices:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.NoWarrantyInvoices=="Yes"}checked="checked"{/if} name="NoWarrantyInvoices" value="{$datarow.NoWarrantyInvoices}" id="NoWarrantyInvoices" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerProduceWarrantyInvoiceHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="IRISManufacturer" >Use IRIS Codes:</label>
                            &nbsp;&nbsp; <input  type="checkbox" name="IRISManufacturer" {if $datarow.IRISManufacturer=="Yes"}checked="checked"{/if} value="Yes" id="IRISManufacturer" >          
                    </p>
                     <p>
                            <label class="cardLabel" for="TechnicalReportIncludeAdj" >Include Adjustments in Technical Report:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.TechnicalReportIncludeAdj=="Yes"}checked="checked"{/if} name="TechnicalReportIncludeAdj" value="{$datarow.TechnicalReportIncludeAdj}" id="TechnicalReportIncludeAdj" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerIncludeAdjustmentinTechReportHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="ValidateDateCode" >Validate Date Code:</label>
                            &nbsp;&nbsp; <input onclick="if(this.checked==true){ $('#addFieldsDop').show()}else{ $('#addFieldsDop').hide() }"  type="checkbox" {if $datarow.ValidateDateCode=="Yes"}checked="checked"{/if} name="ValidateDateCode" value="{$datarow.ValidateDateCode}" id="ValidateDateCode" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerValidateDateCodeHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                        </p>
                        
                        <div id="addFieldsDop" style="display:none">
                      <p>
                            <label class="cardLabel" for="ForceStatusID" >Apply New System Status:</label>
                            &nbsp;&nbsp; 
                                <select name="ForceStatusID" id="ForceStatusID" class="text" >
                                <option value="" {if isset($datarow)&& $datarow.ForceStatusID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $systemstatus as $s}

                                    <option value="{$s.StatusID}" {if isset($datarow)&& $datarow.ForceStatusID eq $s.StatusID}selected="selected"{/if}>{$s.StatusName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                    <p>
                            <label class="cardLabel" for="POPPeriod" >POP Period:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="POPPeriod" value="{$datarow.POPPeriod}" id="POPPeriod" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="ClaimPeriod" >Claim Period:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="ClaimPeriod" value="{$datarow.ClaimPeriod}" id="ClaimPeriod" >          
                        </p>
                        </div>
                        
                       <p>
                            <label class="cardLabel" for="WarrantyPeriodFromDOP" >Warranty Period From DOP:</label>
                            &nbsp;&nbsp; <input  type="text" class="text"  name="WarrantyPeriodFromDOP" value="{$datarow.WarrantyPeriodFromDOP}" id="WarrantyPeriodFromDOP" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="WarrantyExchangeFree" >Warranty Exchange Fee:</label>
                            &nbsp;&nbsp; <input  type="decimal"  class="text"  name="WarrantyExchangeFree" value="{$datarow.WarrantyExchangeFree}" id="WarrantyExchangeFree" > 
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerWarrantyExchangeFeeHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                     <p >
                            <label class="cardLabel" for="AutoFinalRejectClaimsNotResubmitted" >Auto  Reject Claims If Not Resubmitted:</label>
                            &nbsp;&nbsp; <input onclick="if(this.checked==true){ $('#DaysToResubmitP').show()}else{ $('#DaysToResubmitP').hide() }"   type="checkbox" {if $datarow.AutoFinalRejectClaimsNotResubmitted=="Yes"}checked="checked"{/if} name="AutoFinalRejectClaimsNotResubmitted" value="{$datarow.AutoFinalRejectClaimsNotResubmitted}" id="AutoFinalRejectClaimsNotResubmitted" >          
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerAutoRejectClaimIfNotResubHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    
                    <p id="DaysToResubmitP" style="display:none">
                            <label class="cardLabel" for="DaysToResubmit" >Days To Resubmit:</label>
                            &nbsp;&nbsp; <input  type="text" class="text"  name="DaysToResubmit" value="{$datarow.DaysToResubmit}" id="DaysToResubmit" >          
                    </p>
                     <p>
                            <label class="cardLabel" for="LimitResubmissions" >Limit Resubmissions:</label>
                            &nbsp;&nbsp; <input onclick="if(this.checked==true){ $('#LimitResubmissionsToP').show()}else{ $('#LimitResubmissionsToP').hide() }"   type="checkbox" {if $datarow.LimitResubmissions=="Yes"}checked="checked"{/if} name="LimitResubmissions" value="{$datarow.LimitResubmissions}" id="LimitResubmissions" >  
			    <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerLimitResubmissionsHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p style="display:none" id="LimitResubmissionsToP">
                            <label class="cardLabel" for="LimitResubmissionsTo" >Times Allowed To Resubmit:</label>
                            &nbsp;&nbsp; <input  type="text" class="text"  name="LimitResubmissionsTo" value="{$datarow.LimitResubmissionsTo}" id="LimitResubmissionsTo" >          
                    </p>
                    <hr>
                    <p>
                        <span  class="topText cardTopLabel saFormSpan" style="font-weight:bold;">Compulsory Fields
                         <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerComplHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                         </span>
                     </p>
                     <p>
                            <label class="cardLabel" for="PartNoReqForWarrantyAdj" >Adjustment Part No:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.PartNoReqForWarrantyAdj=="Yes"}checked="checked"{/if} name="PartNoReqForWarrantyAdj" value="{$datarow.PartNoReqForWarrantyAdj}" id="PartNoReqForWarrantyAdj" >          
                            
                    </p>
                    <p>
                            <label class="cardLabel" for="ForceAdjOnNoParts" >Adjustment If No Parts Used:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ForceAdjOnNoParts=="Yes"}checked="checked"{/if} name="ForceAdjOnNoParts" value="{$datarow.ForceAdjOnNoParts}" id="ForceAdjOnNoParts" >          
                    </p>
                     <p>
                            <label class="cardLabel" for="UseFaultCodesForOBFJobs" >Fault Codes for OBF Jobs:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.UseFaultCodesForOBFJobs=="Yes"}checked="checked"{/if} name="UseFaultCodesForOBFJobs" value="{$datarow.UseFaultCodesForOBFJobs}" id="UseFaultCodesForOBFJobs" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="ForceOutOfWarrantyFaultCodes" >Out Of Warranty Fault Codes:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ForceOutOfWarrantyFaultCodes=="Yes"}checked="checked"{/if} name="ForceOutOfWarrantyFaultCodes" value="{$datarow.ForceOutOfWarrantyFaultCodes}" id="ForceOutOfWarrantyFaultCodes" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="ForceKeyRepairPart" >Primary Repair Part:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.ForceKeyRepairPart=="Yes"}checked="checked"{/if} name="ForceKeyRepairPart" value="{$datarow.ForceKeyRepairPart}" id="ForceKeyRepairPart" >          
                    </p>
                    
                    <hr>
                    <p>
                        <span  class="topText cardTopLabel saFormSpan" style="font-weight:bold;">Display Fields
                         <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerDisplaySectionHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                         </span>
                     </p>
                    
                    
                    
                    
                    
                    
                    
                    
                     
                        
                     <p style="display:none">
                            <label class="cardLabel" for="IncludeOutOfWarrantyJobs" >Include Out Of Warranty Jobs:</label>
                            &nbsp;&nbsp; <input  type="checkbox" {if $datarow.IncludeOutOfWarrantyJobs=="Yes"}checked="checked"{/if} name="IncludeOutOfWarrantyJobs" value="{$datarow.IncludeOutOfWarrantyJobs}" id="IncludeOutOfWarrantyJobs" >          
                    </p>
                      
                    <p style="display:none">
                            <label class="cardLabel" for="ForceStatusID" >Job Fault Codes:</label>
                            &nbsp;&nbsp; 
                                <select name="JobFaultCodeID" id="JobFaultCodeID" class="text" >
                                <option value="" {if isset($datarow.JobFaultCodeID)&& $datarow.JobFaultCodeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $ForceStatus as $s}

                                    <option value="{$s.StatusID}" {if isset($datarow.JobFaultCodeID)&& $datarow.JobFaultCodeID eq $s.JobFaultCodeID}selected="selected"{/if}>{$s.StatusID|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                        <p style="display:none">
                            <label class="cardLabel" for="PartFaultCodeID" >Part Fault Codes:</label>
                            &nbsp;&nbsp; 
                                <select name="PartFaultCodeID" id="PartFaultCodeID" class="text" >
                                <option value="" {if isset($datarow.PartFaultCodeID)&& $datarow.PartFaultCodeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $ForceStatus as $s}

                                    <option value="{$s.StatusID}" {if isset($datarow.PartFaultCodeID)&& $datarow.PartFaultCodeID eq $s.PartFaultCodeID}selected="selected"{/if}>{$s.StatusID|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                   
                     
                      
                      
                      
                      
                     
                    
                    
                   
                     
                  
                    
                     
                    
                      
                    
                         <p>
                            <label class="cardLabel" for="DisplaySectionCodes" >Section Codes:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplaySectionCodes=="Yes"}checked="checked"{/if} name="DisplaySectionCodes" value="{$datarow.DisplaySectionCodes}" id="DisplaySectionCodes" >          
                             
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayTwoConditionCodes" >2 Condition/Symptom Codes:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayTwoConditionCodes=="Yes"}checked="checked"{/if} name="DisplayTwoConditionCodes" value="{$datarow.DisplayTwoConditionCodes}" id="DisplayTwoConditionCodes" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayJobPriceStructure" >Job Price Structure:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayJobPriceStructure=="Yes"}checked="checked"{/if} name="DisplayJobPriceStructure" value="{$datarow.DisplayJobPriceStructure}" id="DisplayJobPriceStructure" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayTelNos" >Telephone Nos:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayTelNos=="Yes"}checked="checked"{/if} name="DisplayTelNos" value="{$datarow.DisplayTelNos}" id="DisplayTelNos" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayPartCost" >Parts Cost:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayPartCost=="Yes"}checked="checked"{/if} name="DisplayPartCost" value="{$datarow.DisplayPartCost}" id="DisplayPartCost" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayRepairTime" >Repair Time:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayRepairTime=="Yes"}checked="checked"{/if} name="DisplayRepairTime" value="{$datarow.DisplayRepairTime}" id="DisplayRepairTime" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayAuthNo" >Authorisation No:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayAuthNo=="Yes"}checked="checked"{/if} name="DisplayAuthNo" value="{$datarow.DisplayAuthNo}" id="DisplayAuthNo" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayOtherCosts" >"Other Costs" button:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayOtherCosts=="Yes"}checked="checked"{/if} name="DisplayOtherCosts" value="{$datarow.DisplayOtherCosts}" id="DisplayOtherCosts" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="DisplayAdditionalFreight" >Additional Freight:</label>
                            &nbsp;&nbsp; <input   type="checkbox" {if $datarow.DisplayAdditionalFreight=="Yes"}checked="checked"{/if} name="DisplayAdditionalFreight" value="{$datarow.DisplayAdditionalFreight}" id="DisplayAdditionalFreight" >          
                    </p>
                        
                        
                    <hr>
                      <p>
                            <label class="cardLabel" for="NoOfClaimPrints" >Print Claim Form - No Of Copies:</label>
                             &nbsp;&nbsp; <input  type="text" class="text"  name="NoOfClaimPrints" value="{$datarow.NoOfClaimPrints}" id="NoOfClaimPrints" >          
                    </p>
                    <p>
                            <label class="cardLabel" for="EDIClaimNo" >EDI Claim No:</label>
                             &nbsp;&nbsp; <input  type="text" class="text"  name="EDIClaimNo" value="{$datarow.EDIClaimNo}" id="EDIClaimNo" > 
                               <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerEDIClaimNoHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="EDIClaimFileName" >EDI File Name:</label>
                             &nbsp;&nbsp; <input  type="file" class="text"  name="EDIClaimFileName" value="{$datarow.EDIClaimFileName}" id="EDIClaimFileName" > 
			     <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerEDIClaimFileNameHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                    </p>
                    <p>
                            <label class="cardLabel" for="EDIExportFormatID" >EDI Export Format:</label>
                            &nbsp;&nbsp; 
                                <select name="EDIExportFormatID" id="EDIExportFormatID" class="text" >
                                <option value="" {if isset($datarow)&& $datarow.EDIExportFormatID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $EDIExportFormat as $s}

                                    <option value="{$s}" {if isset($datarow)&& $datarow.EDIExportFormatID eq $s}selected="selected"{/if}>{$s|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
				<img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerEDIExportFormatIDHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                        </p>
                    
                    </div>
                
                <div id="tabs-4" class="SystemAdminFormPanel inline">
                    {if $datarow.ManufacturerID!=''&&$datarow.ManufacturerID!='0'}
                     <p>
                            <label class="cardLabel" for="AssociatedManufacturers"  >Associated Manufacturers:</label>
                            &nbsp;&nbsp; <select name="AssociatedManufacturers" id="AssociatedManufacturers" class="text" >
                                <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>
                                
                                {foreach $manufacturers as $s}

                                    <option value="{$s.ManufacturerID}" >{$s.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>  <img style="cursor:pointer; alignment-adjust: central" src="{$_subdomain}/images/add_icon.png" onclick="addAssocManufacturer()" width="15" height="15" align="bottom">
                                
                        </p>
                        <p id="addAssocManufacturerP">
                        <div style="text-align:center" id="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </p>
                        {else}
                            <p>
                            Associated Manufacturers tab will become active only after saving changes.
                            </p> 
                            
                         {/if}
                    </div>
                        
                        <hr>
                               
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>
                </fieldset>    
                        
                </form>  
                    {if $datarow.ManufacturerID!=''&&$datarow.ManufacturerID!='0'}
                        <script>

                        function addAssocManufacturer(){
                       $('#addAssocManufacturerP').html('');
                       $('#ajaxLoader').show();
                         $.post("{$_subdomain}/ProductSetup/addAssocManufacturer/",{ PID:{$datarow.ManufacturerID} ,CID:$('#AssociatedManufacturers').val()},
function(data) {

removeAssocManOption($('#AssociatedManufacturers').val());
loadAssocList();
});
                        }
function removeAssocManOption(val){
$("#AssociatedManufacturers option[value="+val+"]").remove();
}

function loadAssocList(){
$('#ajaxLoader').show();
$('#addAssocManufacturerP').html('');
 $.post("{$_subdomain}/ProductSetup/loadAssocManufacturer/",{ PID:{$datarow.ManufacturerID} ,CID:$('#AssociatedManufacturers').val()},
function(data) {

en=jQuery.parseJSON(data);
$.each(en, function(key, value) { 
removeAssocManOption(en[key]['ChildManufacturerID'])
$('#addAssocManufacturerP').append("<label class='cardLabel'></label><label title='Click to Remove' class='saFormSpan' style='cursor:pointer' onclick='removeAssoc("+en[key]['ChildManufacturerID']+")'>"+en[key]['ManufacturerName']+"</label><br>");
});
$('#ajaxLoader').hide();
});
}
function removeAssoc(id){

if (confirm('Are you sure you want to remove this association?')) {
    $('#ajaxLoader').show();
   $.post("{$_subdomain}/ProductSetup/delAssocManufacturer/",{ PID:{$datarow.ManufacturerID} ,CID:id},
function() {
loadAssocList();
});
} else {
    // Do nothing!
}
}
                    
                        </script>
                        {/if}
       
</div>
 
<div id="AuthoriseServiceProvidersFormPanel" class="SystemAdminFormPanel" style="display: none;" >
    <form id="AuthoriseServiceProvidersForm" name="AuthoriseServiceProvidersForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" >{$page['Text']['authman_legend']|escape:'html'}</legend>
            
            <div style="text-align: right" >
                <span class="text" >{$page['Labels']['show_authorised_tagged']|escape:'html'}</span><input type="checkbox" checked="checked" id="authman_show_tagged" name="authman_show_tagged"  value="1" />
            </div>
            
           <div style="text-align: right" >
                <span class="text" >{$page['Labels']['show_datacheck_tagged']|escape:'html'}</span><input type="checkbox" checked="checked" id="checkman_show_tagged" name="checkman_show_tagged"  value="1" />
            </div>

                <table id="AuthorisedServiceProvidersTable" border="0" cellpadding="0" cellspacing="0" class="browse">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Authorised ASC</th>
                            <th>Data Checked</th> 
                            <th>Key</th>
                            <th>orig_auth</th>
                            <th>orig_check</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $service_providers as $service_provider}
                        <tr>
                            <td>{$service_provider.ServiceProviderName|escape:'html'}</td>
                            <td>{$service_provider.Auth}</td>
                            <td>{$service_provider.DataChecked}</td>
                            <td>{$service_provider.ServiceProviderID}</td>
                            <td>{$service_provider.Auth}</td>
                            <td>{$service_provider.DataChecked}</td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table> 
            
            <br />
 
            <p>
                <span class= "bottomButtons" >
                    <input type="submit" name="authman_save_btn" class="textSubmitButton centerBtn" id="authman_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                    <br />
                    <input type="submit" name="authman_cancel_btn" class="textSubmitButton rightBtn" id="authman_cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >
                </span>
            </p>
        </fieldset>
    </form>
</div>
                                    

                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > test </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
               
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$page['Text']['authman_legend']|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
