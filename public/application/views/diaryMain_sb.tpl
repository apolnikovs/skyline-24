{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']|escape:'html'}
{$PageId = 88}
{if !isset($args.table)&&in_array($actionType,[1,2,3,4])}
    
    {$args['table']=1}
    
    {/if}
    


{/block}
{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
{/block}

{block name=scripts}

     
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
       
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.ui.timepicker.css?v=0.3.1" type="text/css" />
        <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
        <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/Base/diary_only.css" />
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jspostcode.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.Editable.js"></script>
<!--        <script type="text/javascript" src="{$_subdomain}/js/jquery.balloon.js"></script>-->
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.timepicker.js?v=0.3.1"></script>

<script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>


<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
<link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/date.js"></script>

   
<style type="text/css" >
      
    .ui-combobox-input {
         width:270px;
        
     }  
     
     #LunchBreakDurationElement input {
         width:235px;
        
     }
         
</style>


{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
        

      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
var oTable;
var giRedraw = false;
var ppp ;
var time2;
var time3;
var t2;

function selectAppointments(typ)
{
    var selApptRemArr = new Array();
    if(typ == "all")
    {
        $('input[class=apptTagChk]').prop('checked', true);
        $('input[class=apptTagChk]').each( function() {
            selApptRemArr.push($(this).val());
        })
    }
    if(typ == "none")
    {
        $('input[class=apptTagChk]').prop('checked', false);
    }
    if(typ == "sel")
    {
        $('input[class=apptTagChk]').each( function() {
            if($(this).is(":Checked"))
                selApptRemArr.push($(this).val());
        })
        if(selApptRemArr.length == 0)
        {
            alert("Please Tag atleast one appointment to send reminder emails");
            return false;
        }
        else
        {
            $.post("{$_subdomain}/Diary/sendAppointmentReminder",{ apptId:selApptRemArr.join(", ") },
            function(data) {
                alert("Emails Sent.");
            })
        }
    }
}



{if isset($daySelected)}
$(document).ready(function() { //only if day selected
$('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })



 //Click handler for send mail.
$(document).on('click', '#SendDiaryEmail', 
              function() {
              
              

              $.colorbox({ 
 
                        href:"{$_subdomain}/Diary/sendDiaryEmail/",
                        title: "EMail Manager",
                        opacity: 0.75,
                        height:570,
                        width:900,
                        overlayClose: false,
                        escKey: false

                });


                return false;

              });





//check table display ststus and add vars
if('{if $AutoDisplayTable=="Appointment"&&(!isset($args.table)||$args.table=='1')}YES{/if}'=='YES'){
$('#mainTableID').show();
}
if($('#mainTableID').is(':visible')){
locationsAddItem('table',1);
}else{
locationsAddItem('table',0);
}
        //Adding auto select if edit mode enabled
        {if isset($editRow)}$("#{$editRow}").addClass('row_selected');{/if}
	/* Add a click handler to the rows - this could be used as a callback */
	$("#matches_table tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                
                
	});
        
        /* Add a dblclick handler to the rows - this could be used as a callback */
	$("#matches_table tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
      $.post("{$_subdomain}/Diary/showPopUp",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Appointment Data",escKey: false,
    overlayClose: false
,onComplete:function(data){
		
		$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });
    }

}

);


})
    }
                
	});
	
	/* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  
      $.post("{$_subdomain}/Diary/showPopUp",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Appointment Data",escKey: false,
      overlayClose: false
,onComplete:function(data){
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });
    }

}
);


})
    }else{
    alert("Please select appointment first");
    }
		
	} );
        
        /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    if(aData[1]=="APNCOMPL"){
     insertWaitL(6);
      $.post("{$_subdomain}/Diary/deleteAppointment",{ del:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Delete",escKey: false,
    overlayClose: false}

);


})}else{
//alert(aData[1]);
alert('This Appointment is marked as completed and cannot be deleted from system!');
}
    }else{
    alert("Please select appointment first");
    }
		
	} );
        
        
        
        
	
	/* Init the table */
	oTable = $('#matches_table').dataTable( {
"sDom": '<"left"i><"top"f>t<"bottom"i><"clear">',
"bPaginate": false,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    {$t1Filter[0]} },
			  { "bVisible":   {$t1Filter[1]} },
			    { "bVisible":  {$t1Filter[2]} },
                            { "bVisible":   {$t1Filter[3]} },
                             { "bVisible":   {$t1Filter[4]} },
                            { "bVisible":   {$t1Filter[5]} },
                        
                           { "bVisible":     {$t1Filter[6]} },
                            { "bVisible":    {$t1Filter[7]} },
                             { "bVisible":   {$t1Filter[8]} },
                             { "bVisible":   {$t1Filter[9]} },
                             { "bVisible":   {$t1Filter[10]} },
                             { "bVisible":   {$t1Filter[11]} },
                             { "bVisible":   {$t1Filter[12]} },
                            { "bVisible":    {$t1Filter[13]} },
                             { "bVisible":   {$t1Filter[14]} },
                             { "bVisible":   {$t1Filter[15]} },
                             { "bVisible":   {$t1Filter[16]} },
                             { "bVisible":   {$t1Filter[17]} },
                             { "bVisible":   false },
                             { "bVisible":   true },
                             { "bVisible":   true }
                            
                            
                            
		] ,
                "aaSorting": [ [11,'asc'], [2,'asc'] ]
});
} );





    {/if} 

/* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
// Ajax
function insertAppointment(d,m,y){
if(fnGetSelected(oTable)!=""){
$.post("{$_subdomain}/Diary/showPopUp",{ date:d+m+y },
function(data) {

$.colorbox({ html:data, title:"Add Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });

    }});


})


}else{
$.post("{$_subdomain}/Diary/showPopUp",{ date:d+m+y },
function(data) {

$.colorbox({ html:data, title:"Add Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });

    }});


})
}
};

function editAppointment(){
if(fnGetSelected(oTable)!=""){
$.post("{$_subdomain}/Diary/showPopUp",{ update:'t' },
function(data) {

$.colorbox({ html:data, title:"Edit Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });
    }});


})


}else{
alert('Please select record to edit first!');
}
};

locations="";
function applyFilters(f){


if($('#chAM').attr('checked')=='checked'){
locations=locations+"/am=t";
}
if($('#chPM').attr('checked')=='checked'){
locations=locations+"/pm=t";
}
if($('#chAT').attr('checked')=='checked'){
locations=locations+"/any=t";
}
if($('#postCodeFilter').val()!=''){
locations=locations+"/pc="+$('#postCodeFilter').val();
}
//SPID[]
var values = new Array();
$.each($("input[name='st[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});
if(values!=""){
locations=locations+"/tSkills="+values;
}
var values = new Array();
$.each($("input[name='SPID[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});

if(values!=""){
locations=locations+"/tEng="+values;
}


if($("#chSkillType").val()!="---"&&$("#chAppType").val()!="---"&&$('#postCodeFilter').val()!=""){
locationsAddItem('chSkillType',$("#chSkillType").val());
locationsAddItem('chAppType',$("#chAppType").val());

}else{
if(f!=1){
alert("Please select Appointment/Repair Type and Postcode");
}
return false;
}


window.location="{$_subdomain}/Diary/default"+locations;


}

function locationsAddItem(item,name){
var t=locations.indexOf(item+"=0");
var n=locations.indexOf(item+"="+name);
if(n>0){
 locations=locations.replace(item+"="+/[^0-9]+/g,'');
}else{
if(t>0){
locations=locations.replace(item+"="+/[^0-9]+/g,'');
}
 locations=locations+"/"+item+"="+name
 
}

}

function setDate(dateSlot){

locations=locations+"/setDate="+dateSlot;
applyFilters();
}
function setDate2(dateSlot){

locations=locations+"/setDate="+dateSlot;
window.location="{$_subdomain}/Diary/default"+locations;
}

function setActiveTab(tab){

$('#ttTab1').removeClass('active_tab_st');
$('#ttTab2').removeClass('active_tab_st');
$('#ttTab3').removeClass('active_tab_st');

$('#ttTab5').removeClass('active_tab_st');
$('#ttTab7').removeClass('active_tab_st');
$('#ttTab'+tab).addClass('active_tab_st');
$('#box1').hide();
$('#box2').hide();
$('#box3').hide();
$('#box5').hide();
$('#box7').hide();
$('#tab1').hide();
$('#tab2').hide();
$('#tab3').hide();
$('#tab4').hide();
$('#tab5').hide();
$('#tab7').hide();
if(tab==1){
$('#tab1').show();
$('#tab4').show();

}
$('#box'+tab).show();

$('#tab'+tab).show();


}
{if isset($daySelected)}
$(document).ready(function() {
{if isset($args.timeSliders)}
$('#summary_tableDiv').hide();
$('#mainTableID').hide();
$('#timeSliders_tableDiv').show();
{/if}
    {if isset($args.st)}setActiveTab(2){/if}
       
        {if isset($args.rebookMode)}locationsAddItem('rebookMode',{$args.rebookMode}); rebookModeDaySelect('rebookConfirm');{/if}
          var actionType={$actionType}  
          var refresh={$refresh}
            if(actionType==2&&fnGetSelected( oTable )==""&&refresh==0){
            applyFilters();
            }



if($.browser.version!='7.0'){

{* This is added by Raju on 14th June 2013 for TB log 159 i.e. inline edit should be disable starts here*}
{$finalizedDay=array()}
{foreach $finalizedDays as $dd}
{$finalizedDay[]=$dd|date_format:"%d"}
{/foreach}
{$sDay = $daySelected|date_format:"%d"}
{* This is added by Raju on 14th June 2013 for TB log 159 i.e. inline edit should be disable ends here*}
/* Apply the jEditable handlers to the table */
{if $diaryType=="FullViamente"}
    {* Checking the condition for inline editing*}
    {if isset($sDay) && !in_array($sDay,$finalizedDay)}
	$('td:eq(5)', oTable.fnGetNodes()).editable( '{$_subdomain}/Diary/tableUpdate', {
       

               

		"callback": function( sValue, y ) {
               
			var aPos = oTable.fnGetPosition( this );
			//oTable.fnUpdate( sValue, aPos[1] );
                          
		},
		"submitdata": function ( value, settings ) {
                 
			return {
                      
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
                         
		},
		"height": "14px",
		"width": "30px",
                "onblur": "submit"
	} );
        
        
    
    
        
        
        
        $('td:eq(13)', oTable.fnGetNodes()).editable( '{$_subdomain}/Diary/tableUpdateSkillset', {
       loadurl : '{$_subdomain}/Diary/getJsonSkillsetList',
    submit : 'OK',
     type   : 'select',
     style  : "inherit",
     


               

		"callback": function( sValue, y ) {
               
			var aPos = oTable.fnGetPosition( this );
			//oTable.fnUpdate( 'ff', aPos[2] );
                          
		},
		"submitdata": function ( value, settings ) {
               
			return {
                      
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
                         
		},
		"height": "14px",
                "onblur": "submit",
                "onselect":"submit"
	} );
    {/if}
{/if}

{if $diaryType=="NoViamente"}
//update sort order
$('td:eq(0)', oTable.fnGetNodes()).editable( '{$_subdomain}/Diary/tableUpdateSortOrder', {
       

               

		"callback": function( sValue, y ) {
               
			var aPos = oTable.fnGetPosition( this );
			//oTable.fnUpdate( sValue, aPos[1] );
                          
		},
		"submitdata": function ( value, settings ) {
                 
			return {
                      
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
                         
		},
		"height": "14px",
                "onblur": "submit"
	} );
        
        //update skillset
        $('td:eq(9)', oTable.fnGetNodes()).editable( '{$_subdomain}/Diary/tableUpdateSkillset', {
       loadurl : '{$_subdomain}/Diary/getJsonSkillsetList',
    submit : 'OK',
     type   : 'select',
     style  : "inherit",
     


               

		"callback": function( sValue, y ) {
               
			var aPos = oTable.fnGetPosition( this );
			//oTable.fnUpdate( 'ff', aPos[2] );
                          
		},
		"submitdata": function ( value, settings ) {
               
			return {
                      
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
                         
		},
		"height": "14px",
                "onblur": "submit",
                "onselect":"submit"
	} );

{/if}

{if $diaryType=="FullViamente"&&$GlobalViamenteStatus}
//update sort order
$('td:eq(0)', oTable.fnGetNodes()).editable( '{$_subdomain}/Diary/tableUpdateSortOrder', {
       

               

		"callback": function( sValue, y ) {
               
			var aPos = oTable.fnGetPosition( this );
			//oTable.fnUpdate( sValue, aPos[1] );
                          
		},
		"submitdata": function ( value, settings ) {
                 
			return {
                      
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
                         
		},
		"height": "14px",
                "onblur": "submit"
	} );
        
       

{/if}
       } 
});//end document ready function     
    
  function showGMap(){
  {if $diaryType=="FullViamente"}
$.post("{$_subdomain}/Diary/ViamenteMap",{ date:"" },
function(data) {
//alert(data);
data=data.replace(' ', '');
data=data.replace(' ', '');
 window.open("{$viamenteServer}/export/map/"+data,"_blank");


});
{else}
 //It opens color box popup page.              
            $.colorbox( {   href: '{$_subdomain}/Diary/displayAppointmentMap/',
                            title: "Appointment Map",
                            opacity: 0.75,
                            width:$(window).width(),
                            height:$(window).height(),
                            overlayClose: false,
                            escKey: false
                            
            }); 


{/if}
}
{/if}
 function showInsertSlot(){
$.post("{$_subdomain}/Diary/insertSlot",{ date:"" },
function(data) {

$.colorbox({ html:data, title:"Insert Appointment Slot"});


})
}
function loadSkillType(sid){
$.post("{$_subdomain}/Diary/getRepSkill",{ sid:sid },
function(data) {

$.colorbox({ html:data, title:"Appointments Map",escKey: false,
    overlayClose: false});


})
}

function toggleTable(){
$('#mainTableID').toggle();
if($('#mainTableID').is(':visible')){
locationsAddItem('table',1);
}else{
locationsAddItem('table',0);
}
$('#summary_tableDiv').hide();
}
function toggleTable2(){
$('#summary_tableDiv').toggle();
$('#mainTableID').hide();
}


$(function(){
    $.contextMenu({
        selector: '.APcalendarNone', 
        callback: function(key, options) {
            if(key=='Insert New Slot')
                {
            var m =this.attr('id');
           $.post("{$_subdomain}/Diary/addDiarySlot",{ day:m },
function(data) {

$.colorbox({ html:data, title:"Add New Diary Allocation Slot",escKey: false,
    overlayClose: false});


})
}
        },
        items: {
            
            "sep1": "---------",
            "quit": { name: "Quit", icon: "quit"}
        }
    });
    
    $('.context-menu-one').on('click', function(e){
     //   console.log('clicked', this);
    })
});

$(function(){
    $.contextMenu({
        selector: '.cmenu44', 
        callback: function(key, options) {
            if(key=='sendemail')
                {
            insertWaitL(6);
           $.post("{$_subdomain}/Diary/emailViamnteMap",{ 1:1 },
function(data) {
$.colorbox.close();
alert("Emails Sent.");



})
}
if(key=='apptRemainders')
{
    var clickedCellDate = $(this).attr('date');
    insertWaitL(6);
    $.post("{$_subdomain}/Diary/getDayAppointments",{ curDate:clickedCellDate },
    function(data) {
        $.colorbox.close();
        alert("Emails Sent.");
    })
}
if(key=='FORCE ADD APPOINTMENT')
                {
                        t2='0';
                         var m =this.attr('id');
                      confirmPass(1,m)
                   
           
        

}

        },
        items: {
            {if in_array($actionType,[1,6])} 
                "FORCE ADD APPOINTMENT": { name: "FORCE ADD APPOINTMENT", icon: "paste"},
            {/if}
            "sendemail":{ name: "Email Routes", icon: "edit"},
            "sep1": "---------",
            "apptRemainders":{ name: "Email Appointment Reminders", icon: "paste"},
            "sep2": "---------",
            "quit": { name: "Quit", icon: "quit"}
        }
    });
    
    $('.context-menu-one').on('click', function(e){
     //   console.log('clicked', this);
    })
});

$(function(){
    $.contextMenu({
        selector: '#help_video',
        trigger:"hover",
        callback: function(key, options) {
            if(key=='Insert New Slot')
                {
            var m =this.attr('id');
           $.post("{$_subdomain}/Diary/addDiarySlot",{ day:m },
function(data) {

$.colorbox({ html:data, title:"Add New Diary Allocation Slot",escKey: false,
    overlayClose: false});


})
}
        },
        items: {
            "V1": { name: "VIDEO 1", icon: "edit"},
            "V2": { name: "VIDEO 2", icon: "edit"},
            "V3": { name: "VIDEO 3", icon: "edit"},
            "sep1": "---------",
            "quit": { name: "Quit", icon: "quit"}
        }
    });
    
    $('.context-menu-one').on('click', function(e){
     //   console.log('clicked', this);
    })
});

function rebookMode(id){
var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        
        if(aData[1]=="APNCOMPL"){
        
        $('#rebookAppInfo').html(aData[12]+', '+aData[9]+', SB Job No '+aData[15]);
            locationsAddItem('rebookMode',aData[0]);
      $.post("{$_subdomain}/Diary/setEditRow",{ editRow:aData[0],editRowData:$('#rebookAppInfo').html()},
function(data) {

if($.trim(data)=="Samsung One Touch"){
     $.post("{$_subdomain}/Diary/samsungMoveReason",{ 'show':1},
function(data) {

$.colorbox({ html:data, title:"Samsung One Touch",escKey: false,
  overlayClose: false});
}
)};
$("#"+id).show();
{if isset($editRow)}locationsAddItem('rebookMode',{$editRow});{/if}


})}else{
alert('This Appointment is marked as completed and cannot be rebooked!');
}
    }else{
    alert("Please select appointment first");
    }

}

function rebookModeDaySelect(id){
$("#"+id).show();
}

function confirmRebook(){
window.location="{$_subdomain}/Diary/confirmRebook";
}

function cancelRebooking(){
$("#rebookConfirm").hide();
window.location="{$_subdomain}/Diary/cancelRebooking";
}





function filterTable(name){

var oTable = $('#matches_table').dataTable();
   
  // Sometime later - filter...
  oTable.fnFilter( name);
//$('input[type="text"]','#matches_table_filter').val(name);
//e = jQuery.Event("keyup");
//e.which = 13;
//$('input[type="text"]','#matches_table_filter').trigger(e);


}

$(function(){

    $.contextMenu({
        selector: '.cmenu2', 
        callback: function(key, options) {
            if(key=='Insert Appointment')
                { var m =this.attr('id');
                       
                        if(insertTimeCheck(m)==true){
           
           $.post("{$_subdomain}/Diary/menuInsertAppointment",{ day:m },
function(data) {

$.colorbox({ html:data, title:"Add New Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    //create slider
       $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });
    }});


})

}}
if(key=='Show Appointment Table')//start
                {
            var m =this.attr('ph');
           locationsAddItem('table',1);
          setDate2(m);
}//end 
if(key=='FORCE ADD APPOINTMENT')
                {
                        t2='0';
                         var m =this.attr('id');
                      confirmPass(1,m)
                   
           
        

}
        },
        items: {
           {if in_array($actionType,[1,6])} "Insert Appointment": { name: "Insert Appointment", icon: "paste"},
           "FORCE ADD APPOINTMENT": { name: "FORCE ADD APPOINTMENT", icon: "paste"},{/if}
            "Show Appointment Table": { name: "Show Appointment Table", icon: "table"},
            "sep1": "---------",
            "quit": { name: "Quit", icon: "quit"}
        }
    });
    
    $('.context-menu-one').on('click', function(e){
       // console.log('clicked', this);
    })
});

function insertWaitL(action){



switch(action){

case 1:
       $.post("{$_subdomain}/Diary/checkEmptyDay",false,
function(data) {

if(data*1!=0){
  $.colorbox({ html:$('#ViaDiv1').html(), title:"",escKey: false,
  overlayClose: false});
}else{
alert('You have selected to run Viamente optimisation on a day that has no appointments. Please select another day to optimise.');
}
//$('#cboxClose').remove();
})
break;  
case 2:
    $.post("{$_subdomain}/Diary/checkDayErrors",false,
    function(data) {
        if(data*1==0){
            $.colorbox({ html:$('#ViaDiv2').html(), title:"",escKey: false,
            overlayClose: false});
            $('#cboxClose').remove();
        }else{
            $.colorbox({ html:$('#ViaDivError').html(), title:"",escKey: false,overlayClose: false});
        }
    });
break;
case 3:
    $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false, overlayClose: false});
    $('#cboxClose').remove();
    if($('#setappwind').val()!="" ){
        $.post("{$_subdomain}/Diary/LockAppWindow",false,function() {
            window.location="{$_subdomain}/Diary/viamenteUpdate2/defaultRet=1 ";
        });
    }else{
        window.location="{$_subdomain}/Diary/viamenteUpdate2/defaultRet=1 ";
    }
break;
case 4: 
    $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
if($('#setappwind').val()!="" ){
$.post("{$_subdomain}/Diary/LockAppWindow",false,function()
{
    window.location="{$_subdomain}/Diary/viamenteUpdate2/defaultRet=2 ";
 }
);

}else{
window.location="{$_subdomain}/Diary/viamenteUpdate2/defaultRet=2 ";
}
   break;
   
   case 5: 
     
    $.colorbox({ html:$('#ViaDiv3').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();

   break;
   case 6: 
    $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();

   break;
   case 7: //visulaq fx only browngoods
    if($("#vtype").val() == "")
        $('.errMsgP1').html('<span style=color:red>You must select a Skill Type before continuing</span>')
    else
    {
        $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
        overlayClose: false});
        $('#cboxClose').remove();
        window.location="{$_subdomain}/Diary/viamenteUpdate2VisualFX/mode="+$('#vtype').val();
    }
   break;
    case 8: //visulaq fx only white
    if($("#vtype").val() == "")
        $('.errMsgP1').html('<span style=color:red>You must select a Skill Type before continuing</span>')
    else
    {
        $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
        overlayClose: false});
        $('#cboxClose').remove();
        window.location="{$_subdomain}/Diary/viamenteUpdate2VisualFX/mode="+$('#vtype').val()+"/defaultRet=2";
    }
   break;
    case 9: //visulaq fx only both
    $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
window.location="{$_subdomain}/Diary/viamenteUpdate2VisualFX/mode=fxBoth";
   break;
   
}



}




$(function () {
    $('#tagAllSkillsets').click(function () {
        $(this).parents('fieldset:eq(0)').find(':checkbox').attr('checked', this.checked);
    });
});

$(function () {
    $('#tagAllEngineers').click(function () {
        $(this).parents('fieldset:eq(0)').find(':checkbox').attr('checked', this.checked);
    });
});

function posttest(){
 $.post("{$_subdomain}/API/AppointmentsAPI",false,
function(data) {

alert(data);
   


})
}


function filterSkillset(man){

}


function insertTimeCheck(time)
{ 
    
    if(time==null){
    t2='1';
    time='{if isset($daySelected)}{$daySelected}{/if}';
    }else{
    t2='0'
    }
    if(t2=='0'){
    time3=time;
    time= time.split(" ");
time2=time[1]
    }else{
    time2=time
    }
var myDate = new Date();
if(t2=='1'){

 displayDate = (('0'+(myDate.getMonth()+1)).slice(-2) + '/' + ('0'+(myDate.getDate()+1)).slice(-2) + '/' + myDate.getFullYear());
}else{

 displayDate = (myDate.getFullYear() + '-' + ('0'+(myDate.getMonth()+1)).slice(-2) + '-' + ('0'+(myDate.getDate()+1)).slice(-2));
}

hours=myDate.getHours();
minutes=myDate.getMinutes();
//console.log(hours);
//console.log(minutes);
//console.log({$limitTime|date_format:"%H"});
//console.log({$limitTime|date_format:"%M"});
//console.log('{$lockedBookingAfter}');
if((displayDate==time2&&hours>={$limitTime|date_format:"%H"})&&'{$lockedBookingAfter}'=="Yes"){
    if(minutes>={$limitTime|date_format:"%M"}){
$.colorbox({ html:$('#insertWarning').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
}
}else{
if(t2=='0'){
return true;
}else{
insertAppointment('{if isset($daySelected)}{$daySelected}{/if}');
}
}
}

function confirmPass(b,time){
if(time) { time3=time;}
$.colorbox({ html:$('#confirmPass').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
}

function checkpass(b){
 
$.post("{$_subdomain}/Diary/checkPass",{ pass:ppp },
function(data) {

//console.log(data);
if(data*1==12){

if(t2=='0'){

insertApp2(time3,b);
}else{

insertAppointment('{if isset($daySelected)}{$daySelected}{/if}');
}
}else{
alert("Wrong password entered");
}
    });
}

function insertApp2(m,v){ 
if(!v){
v=0;
}else{
v=1;
}
 $.post("{$_subdomain}/Diary/menuInsertAppointment",{ day:m,slot:v },
function(data) {
 
$.colorbox({ html:data, title:"Add New Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
    //create slider
       
      $("#slider").slider({
      min: 0, //minimum value
      max: 10, //maximum value
      value: $("#importance").val(), //default value
      slide: function(event, ui) {
           $("#importance").val(ui.value);
          }
      });
       

    }});});
}

function skillsetMan(m){
if(m==1){
$('#chSkillType').html((($('#sills1').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').html((($('#aptypes1').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').val("");
$('#chSkillType').val("");
}
if(m==2){
$('#chSkillType').html((($('#sills2').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').html((($('#aptypes2').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').val("");
$('#chSkillType').val("");
}
}


$(document).ready(function() {
 {if !isset($adrGeotag)||$adrGeotag==""}
    
     {if $actionType==1}
         showPointMap();
         {/if}
     {/if}
if($.browser.version=='7.0'){
$('#help_menu').css('top',-50);
}
///


//body keypres catcher
$(document).keypress(function(event) {
      //alert('Handler for .keypress() called. - ' + event.charCode);
      if(event.charCode==96){
      //v image slide
    //  console.log($('#vImg').is(":visible") )
      if(!$('#vImg').is(":visible")){
      
      $('#chSkillType').val("");
      $('#chAppType').val("");
      $('#1person').attr("checked","checked");
      $('#2person').attr("checked",false);
      $('#postCodeFilter').val("");
     $.post("{$_subdomain}/Diary/showFullDiary",{ a:1 } );
      
  $('#vImg').toggle("slide", { direction: "up" }, 1000,function() {
     window.location="{$_subdomain}/Diary/default"
  });
    return ;
    }
 
 $('#vImg').toggle("slide", { direction: "up" }, 1000);
$.post("{$_subdomain}/Diary/showFullDiary",{ a:1 }

    );


///
      }
});
//supervisor 
//confirmPass()
//man selector
//alert($('input[name="menReq"]:checked','#cPanleForm').val());
if($('input[name="menReq"]:checked','#cPanleForm').val()==1||$('input[name="menReq"]:checked','#cPanleForm').val()=='undefined'){

$('#chSkillType').html((($('#sills1').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').html((($('#aptypes1').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
locationsAddItem('menReq',1);
}
if($('input[name="menReq"]:checked','#cPanleForm').val()==2){

$('#chSkillType').html((($('#sills2').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
$('#chAppType').html((($('#aptypes2').html().replace('<select>','')).replace('</select>','').replace('<SELECT>','')).replace('</SELECT>',''));
locationsAddItem('menReq',2);
}
   
 $("#postCodeFilter").keypress(function(data) {
  
  if(data.keyCode==13){
  applyFilters();
  return false;
}
});
});


$(function(){
    $.contextMenu({
        selector: '.cmenu3', 
        callback: function(key, options) {
            if(key=='FORCE ADD APPOINTMENT')
                {
                        t2='0';
                         var m =this.attr('id');
                      confirmPass(1,m)
                   
           
        

}
        },
        items: { {if in_array($actionType,[1,6])}
            "FORCE ADD APPOINTMENT": { name: "FORCE ADD APPOINTMENT", icon: "edit"},
            
            "sep1": "---------",{/if}
            "quit": { name: "Quit", icon: "quit"}
            
        }
    });
    
    $('.context-menu-one').on('click', function(e){
       // console.log('clicked', this);
    })
});

function EditGeotag(pc,fa){

//$.post("{$_subdomain}/Diary/editGeoTag",{ 'pc':pc },
//function(data) {
//
//$.colorbox({ html:data, title:"Edit Geo Tag",escKey: false,
//    overlayClose: false,onComplete:function(data){
//	
//         var geocoder = new google.maps.Geocoder();
//          geocoder.geocode({ 'address' : pc+" "+fa}, function(results, status){
//    var defLat = results[0].geometry.location.lat();
//    var defLong =results[0].geometry.location.lng() ;
//    var mylatlong=defLat+','+defLong;
//
//          
//          
//$('#map_canvas').gmap({ 'center':  mylatlong, 'zoom':12}).bind('init', function(event, map) { 
//	$('#map_canvas').gmap('addMarker', {
//			'position': mylatlong, 
//			'draggable': true, 
//			'bounds': false
//		}, function(map, marker) {
//			//$('#dialog').append('');
//			findLocation(marker.getPosition(), marker);
//		}).dragend( function(event) {
//			findLocation(event.latLng, this);
//		}).click( function() {
//			openDialog(this);
//		})
//	
//        
//        $(map).click( function(event) {
//               $('#map_canvas').gmap('clear', 'markers');
//		$('#map_canvas').gmap('addMarker', {
//			'position': event.latLng, 
//			'draggable': true, 
//			'bounds': false
//		}, function(map, marker) {
//			//$('#dialog').append('');
//			findLocation(marker.getPosition(), marker);
//		}).dragend( function(event) {
//			findLocation(event.latLng, this);
//		}).click( function() {
//			openDialog(this);
//		})
//	});
//
//        
//});
//});	
//	
//      }});
//
//   });
alert('To edit postcode geotag please open appointment in edit mode and click on map view button.')

}

function printSelectedEngineers(){

var values = new Array();
$.each($("input[name='SPID[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});

if(values!=""){
engs="/tEng="+values;
}

window.open("{$_subdomain}/Diary/printReport1"+engs,"_blank");
}


function dataExport(){

$.post("{$_subdomain}/Diary/showDataExport",{ 'a':1 },
function(data) {

$.colorbox({ html:data, title:"Data Export",escKey: false,
    overlayClose: false,onComplete:function(data){

                  $("#dateFrom" ).datepicker(
              {
              dateFormat: "dd/mm/yy",
                       showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        

          }
          }
          );
            
            
             $('#dateTo').datepicker(  {
              dateFormat: "dd/mm/yy",
                        showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        

          }
          });
     
	
        
        var  oTable3 = $('#export_table').dataTable( {
"sDom": '<"top"><"bottom"><"clear">',
"bPaginate": false,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    false },
                                  false
			 
                           
                            
                            
                            
		] 
});//end datatable

/* Add a click handler to the rows - this could be used as a callback */
	$("#export_table tbody").click(function(event) {
		$(oTable3.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable3 );
                var aData = oTable3.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
        
        
        
        
          }
      });



})


}

function submenu(){

$('#subMenu1').toggle();
}

var aidArray="";

function addSelectedApp(aid){

n=aidArray.indexOf(aid+",");

if(n>-1){
 aidArray=aidArray.replace(aid+",",'');
}else{
aidArray=aidArray+aid+",";
}
//console.log(aidArray);
}

function checkSpecify(){
if(aidArray!=""){
$('#specifyEngButtonDiary').attr('disabled',false);
$('#specifyEngButtonDiary').css('background-color','rgb(21, 136, 195)');
}
}
function specifyEngineer(){
$.post("{$_subdomain}/Diary/checkCompleted",{ arr:aidArray },
function(data) {
if(data*1==1){
alert("Egnineer cannot be changed becouse one or more jobs are marked as completed!");

}else{



insertWaitL(6);
 $.post("{$_subdomain}/Diary/specifyEngineerBulk",{ 'apIds':aidArray,'engSel':$("input[name='SPID[]']:checked").val() },
function(data) {
$.colorbox.close();
$('#dummyHolder').html(data);

//window.location="{$_subdomain}/Diary/default"+locations;
});

}
});
}

function unspecifyBut(){
$('#unspecifyEngButtonDiary').show();
$('#specifyEngButtonDiary').hide();
}
function hideConfirmBut(){
$('#unspecifyEngButtonDiary').hide();
$('#specifyEngButtonDiary').show();
}
function unspecifyEngineer(){


$.post("{$_subdomain}/Diary/checkCompleted",{ arr:aidArray },
function(data) {
if(data*1==1){
alert("Egnineer cannot be changed becouse one or more jobs are marked as completed!");

}else{



insertWaitL(6);
 $.post("{$_subdomain}/Diary/unspecifyEngineerBulk",{ 'apIds':aidArray,'engSel':$("input[name='SPID[]']:checked").val() },
function(data) {
$.colorbox.close();
$('#dummyHolder').html(data);

//window.location="{$_subdomain}/Diary/default"+locations;
});

}
});

}

function markAsComplited(){

if(aidArray!=""){
//insertWaitL(6);
 $.post("{$_subdomain}/Diary/markAsComplited",{ 'apIds':aidArray,'type':$("input[name='mComplete']:checked").val() },
function(data) {
//alert(data);
$.colorbox.close();
$('#dummyHolder').html(data);

//window.location="{$_subdomain}/Diary/default"+locations;
});}
else{
alert("No appointments selected");
}
}

function showPrintMenu(){

$.post("{$_subdomain}/Diary/showPrintMenu",{ 'a':1 },
function(data) {

$.colorbox({ html:data, title:"Data Print",escKey: false,
    overlayClose: false,onComplete:function(data){

                  $("#dateFromPrint" ).datepicker(
              {
              dateFormat: "dd/mm/yy",
                       showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        changeDate();

          }
          }
          );
            
            
             
     
	
        
        var  oTable3 = $('#export_table').dataTable( {
"sDom": '<"top"><"bottom"><"clear">',
"bPaginate": false,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    false },
                                  false
			 
                           
                            
                            
                            
		] 
});//end datatable

/* Add a click handler to the rows - this could be used as a callback */
	$("#export_table tbody").click(function(event) {
		$(oTable3.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable3 );
                var aData = oTable3.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
        
        
        
        
          }
      });



})


}

function checkCompletedApp(){

 $.post("{$_subdomain}/Diary/checkCompleted",{ arr:aidArray },
function(data) {
if(data*1==1){
alert("Egnineer cannot be changed becouse one or more jobs are marked as completed!");
return false;
}else{
return 1;
}
});
}

function showTimeSliders(){
window.location="{$_subdomain}/Diary/default/timeSliders=1";
}





function showMultiMap(){
if($("#chSkillType").val()!="---"&&$("#chAppType").val()!="---"&&$('#postCodeFilter').val()!=""){



var winW = 1280; // sets a default width for browsers who do not understand screen.width below
var winH = 1024; // ditto for height

if (screen){ // weeds out older browsers who do not understand screen.width/screen.height
 var  winW = screen.width-40;
  var  winH = screen.height;
}

//window.open('{$_subdomain}/Diary/multimap','popUpWindow','height=2px,width='+winW+'px,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
 $.colorbox({ 
 
                        href:"{$_subdomain}/Diary/multimap",
                        title: "Skyline Multimap",
                        opacity: 0,
                        height:winH,
                        width:winW,
                        overlayClose: false,
                        escKey: false,
                        title:false,
                        onComplete:function(data){
                        
                        
                         $("#cboxContent").addClass("multimapstyle");
                       
                         $("#cboxLoadedContent").css('width', $("#cboxLoadedContent").width()+100);
                        }

                });
                }else{

alert("Please select Appointment/Repair Type and Postcode");
}
}

function showPointMap(){
//console.log("{$_subdomain}/Diary/showPointMap/ddd/"+$('#postCodeFilter').val().split(' ').join('+'));
$.colorbox({ 
 
                        href:"{$_subdomain}/Diary/showPointMap/ddd/"+$('#postCodeFilter').val().split(' ').join('+')+"/main",
                        title: "Geotag locator",
                        opacity: 0.75,
                        width:1024,
                        overlayClose: false,
                        escKey: false

                });


                return false;

              
}
function showHelpIcons(){
$('#help_video').toggle();
$('#help_pdf').toggle();
$('#help_pdf_new').toggle();
$('#help_faq').toggle();
$('#help_guide').toggle();
}

function settablemode(m){
/* Get the DataTables object again - this is not a recreation, just a get of the object */
	var oTable = $('#matches_table').dataTable();
	switch(m){
        case 1: 
            oTable.fnSetColumnVis(5,false);
            oTable.fnSetColumnVis(7,false);
            oTable.fnSetColumnVis(8,false);
            oTable.fnSetColumnVis(6,false);
            oTable.fnSetColumnVis(10,true);
            oTable.fnSetColumnVis(13,false);
            oTable.fnSetColumnVis(14,false);
            oTable.fnSetColumnVis(15,true);
            oTable.fnSetColumnVis(16,true);
            oTable.fnSetColumnVis(17,true);
            break;
        case 2: 
            oTable.fnSetColumnVis(6,true);
            oTable.fnSetColumnVis(7,true);
            oTable.fnSetColumnVis(8,true);
            oTable.fnSetColumnVis(5,true);
            oTable.fnSetColumnVis(13,true);
            oTable.fnSetColumnVis(14,true);
            oTable.fnSetColumnVis(15,true);
            oTable.fnSetColumnVis(16,true);
            oTable.fnSetColumnVis(17,true);
            oTable.fnSetColumnVis(10,false);
            break;
        }
	
}

function showUnreachedMap(){
//It opens color box popup page.              
            $.colorbox( {   href: '{$_subdomain}/Diary/displayAppointmentMap/',
                            title: "Appointment Map",
                            opacity: 0.75,
                            width:$(window).width(),
                            height:$(window).height(),
                            overlayClose: false,
                            escKey: false
                            
            }); 
}
function showSamsungFilter(){
//It opens color box popup page.              
            $.colorbox( {   html: $('#samsungTableFilter').html(),
                            title: "Samsung Appointment Filtering",
                            opacity: 0.75,
                            
                            overlayClose: true,
                            escKey: true
                            
            }); 
}

</script>
{/block}




{block name=body}
{if !isset($ServiceBase)}
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'} 
    </div>
<!--    <button onclick="posttest()">Posttest</button>-->
</div>
    {/if}
    <div style="position:relative;float:right;border:none;width:500px;text-align: right">
        {if !isset($ServiceBase)}
            <div id="help_menu" style="position:absolute;border:none;top:-80px;float:right;width:500px">
                {else}
                      <div id="help_menu" style="position:absolute;border:none;top:-50px;float:right;width:500px"> 
                          {/if}
              <img title="Click For Help" id onclick="showHelpIcons()" style="position:relative;float:right;border:none;cursor :pointer" src="{$_subdomain}/images/diary/help_user_guide_red.png">
             <!--<img title="Video Tutorial" id="help_video" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/dairy_help_play.png">-->
              <a href="{$_subdomain}/files/Skyline_Online_Diary_(v3).pdf" target="_blank"><img onclick="" title="Diary Manual" id="help_pdf" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/manual-user-guide.png"></a>
              <a href="{$_subdomain}/files/BestPractice.pdf" target="_blank"><img onclick="" title="Best Practice Guide" id="help_guide" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/best-practice-guide-user-guide.png"></a>
              <!--<a href="{$_subdomain}/files/Skyline_Online_Diary_(v3).pdf" target="_blank"><img onclick="" title="Latest Update" id="help_pdf_new" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/update_icon_button3.png"></a>-->
              <a href="{$_subdomain}/Diary/faq" target="_blank"><img onclick="" title="Frequently Asked Questions" id="help_faq" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/FAQ-user-guide.png"></a>
            
        
        </div>
           
    </div>
              
    {if isset($super)}
    <p style="text-align:center;color:red">Diary is in SuperAdmin mode,you curently looking into "{$spinfo['CompanyName']}" diary to select service provider click -><a onclick="window.location='{$_subdomain}/Diary/default/resetSp=1'" style="cursor:pointer">HERE</a><-</p>
    {/if}
    {if isset($PartEngineerName) && $slotsCount|@count==0 && isset($args.chSkillType)} <br>
        <p style="font-size:16px;color:red;text-align: center;margin: auto">
            No Slots Available For: {$PartEngineerCode|default:""} {$PartEngineerName}
        
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="NoPartSlotsHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </p>  
   {/if}
        
   
    <div class="main" id="home">
        <fieldset style="padding:0xp;">
            <legend >Appointment Diary</legend>
            
        <div style="float:left;">
     {include file="calendar.tpl"}                    
         </div>
        <div style="float:left;width:440px;height:381px"> 
            
            
            
            <div style="float:left;margin-left:162px;display:none;z-index: 102" id="subMenu1">
            <div style="position:absolute;z-index: 101;top:380px;">
                <fieldset style="padding:5px;">
                      <img title="Export data" style="cursor:pointer" onclick="dataExport()" title=""  src="{$_subdomain}/css/Base/images/icons2_export_routes.png">
                <img   title=""  src="{$_subdomain}/css/Base/images/icons2_summary.png">
              
                <img title=""  src="{$_subdomain}/css/Base/images/icons2_send_route.png">
                <img title=""  src="{$_subdomain}/css/Base/images/icons2_print.png">
                
                
                </fieldset>
              </div>
                </div>
       <div class="tabbed-area" style="height:30px">

        <ul class="tabs2 group2" style="width:500px;" >
            <li><a  style="cursor:pointer;border-right:none;" id="ttTab1" onclick="setActiveTab('1')" class="active_tab_st blue_tab" >Control Panel</a></li>
<!--            <li id="tab2" style="display:none"><a style="width:140px" style="cursor:pointer;border-right:none;" id="ttTab2"  class="blue_tab" >Service Skills</a></li>-->
            <li id="tab3" style="display:none;border-right:none;"><a style="width:140px;border-right:none;cursor:pointer"  id="ttTab3"  class="blue_tab" >Engineers</a></li>
          
            <li id="tab4" style="display:block;border:none;border-left:none"><a style="width:155px;border-left:none;border-right:none" style="cursor:pointer" id="ttTab3"  class="active_tab_st" ></a></li>
              <li id="tab5" style="display:none;border-right:none;"><a style="width:140px;border-right:none;cursor:pointer"  id="ttTab5"  class="blue_tab" >Remote Engineer</a></li>
              <li id="tab6" style="display:none;border-right:none;"><a style="width:140px;border-right:none;cursor:pointer"  id="ttTab6"  class="blue_tab" >Edit Appt. Duration</a></li>
              <li id="tab7" style="display:none;border-right:none;"><a style="width:152px;border-right:none;cursor:pointer"  id="ttTab7"  class="blue_tab" >Email Reminders</a></li>
        </ul>
	
	<div class="box-wrap" id="box-wrap" style="height:300px">
            <div style="width:50px;position:absolute;float:right;left:350px;height:337px;z-index: 100;padding-top:6px;padding-bottom:14px;top:-30px;right:6px">
                
              <span  style="margin-top:0px"><a   onsubmit="applyFilters()" style="cursor:pointer"  onclick="applyFilters()"><img title="Apply filters" width="47px" height="47px" src="{$_subdomain}/css/Base/images/update_icon_st.png"></a></span>
              <span style="margin-top:0px" > <a onclick="toggleTable()" style="cursor:pointer"><img title="Show/Hide Table" width="47px" height="47px" src="{$_subdomain}/css/Base/images/table_icon.png"></a></span>
              <span style="margin-top:0px" > <a onclick="toggleTable2()" style="cursor:pointer"><img title="Show/Hide Summary Table" width="47px" height="47px" src="{$_subdomain}/css/Base/images/clipboard_icon.png"></a></span>
              <span style="margin-top:0px" ><a onclick="window.location='{$_subdomain}/AppointmentDiary'" style="cursor:pointer"><img  title="Setup diary defaults" width="47px" height="47px" src="{$_subdomain}/css/Base/images/defaultsSetup_icon.png"></a>  </span>
              <span style="margin-top:0px" ><a onclick="dataExport()" style="cursor:pointer"><img  title="Open export menu" width="47px" height="47px" src="{$_subdomain}/css/Base/images/export_icon.png"></a>  </span>
            {if $diaryType=="FullViamente"}
              <span style="margin-top:0px" ><a onclick="window.location='{$_subdomain}/Diary/holidayDiary'" style="cursor:pointer"><img  title="Open Absent Diary" width="47px" height="47px" src="{$_subdomain}/css/Base/images/time-off_icon.png"></a>  </span>
            {/if}
              <span style="top:0px" ><a onclick="window.location='{$_subdomain}/Diary/wallboard'" style="cursor:pointer"><img  title="Display wallboard" width="47px" height="47px" src="{$_subdomain}/css/Base/images/wallboard_icon.png"></a>  </span>
  
            </div>
              
              
              
            <div id="box1" class="commandDiv" style="display:block;height:298px;width:400px;padding-top:10px">
                
                {if in_array($actionType,[5,6])}
               {if $diaryType=="FullViamente"}
                      <img id="vImg"   style="position:absolute;top:-30px;border-top:1px solid #CCC;z-index:99 {if $showFull==1};display:none{/if}" src="{$_subdomain}/css/Base/images/instead_of_remote.jpg">
                     {else}
                      <img id="vImg"   style="position:absolute;top:-30px;border-top:1px solid #CCC;z-index:99 {if $showFull==1};display:none{/if}" src="{$_subdomain}/images/diary/instead_of_remote3.png">
                      {/if}
                 
                  {/if}
                  
                   {if isset($customerInfo)} <span> Customer Name:  {$customerInfo}</span>{/if}
                {if isset($modelInfo)}  <br><span> Product:  {$modelInfo}</span>{/if}
               {if isset($phone)} <br><span> Contact No:  {$phone}</span><br>{/if}
               {if isset($PartEngineerName)} <br><span> Parts Delivered To: </span>{if $PartEngineerName==""}SB Code {/if}{$PartEngineerCode|default:""} {$PartEngineerName}{/if}
               <br>
              <form style="float:left;position: relative" id="cPanleForm">
           
                <fieldset class="st" style=";margin:0px;position: relative;padding:5px;color:#666;background:#F7F7F7;width:290px;float:left;">
              
                  <input id="1person" onclick="skillsetMan(1);locationsAddItem('menReq',1);" type="radio"  name="menReq" value="1" {if !isset($args.menReq)||$args.menReq==1}checked="checked"{/if}> 1 Person &nbsp;&nbsp;&nbsp;
                                               <input id="2person" onclick="skillsetMan(2);locationsAddItem('menReq',2);" type="radio" name="menReq" value="2" {if isset($args.menReq)&&$args.menReq==2}checked="checked"{/if}> 2 Person   
               <select name="chSkillType" id="chSkillType" style="width:285px" onchange="applyFilters(1);">
                            <option value="---">Please select Appointment Type</option>
                        {foreach from=$repskills key=sk item=item} 
                        <option {if isset($args.chSkillType)&&$args.chSkillType==$item}selected=selected{/if}  {if isset($repskill)&&$repskill===$item}selected=selected{/if}   value="{$item}"> {$sk}</option>
                         {/foreach}  
                        
                        </select>
                                  
                                             
                        <select name="chAppType" id="chAppType" style="width:285px" onchange="applyFilters(1);" >
                            <option value="---">Please select Skill Type</option>
                            {foreach from=$appTypes key=sk item=item} 
                        <option {if isset($args.chAppType)&&$args.chAppType==$item}selected=selected{/if} {if isset($apptype)&&$apptype===$item}selected=selected{/if}  value="{$item}"> {$sk}</option>
                         {/foreach}  
                        
                        </select>
                         
                        <div id="sills1" style="display:none" >
                            <select>
                        <option  value="---">Please select Skill Type</option>
                          {foreach from=$repskills key=sk item=item} 
                        <option {if isset($args.chSkillType)&&$args.chSkillType==$item}selected=selected{/if} {if isset($repskill)&&$repskill===$item}selected=selected{/if} value="{$item}"> {$sk}</option>
                         {/foreach} 
                         </select>
                        </div>
                        <div id="sills2" style="display:none">
                     <select>
                       <option value="---">Please select Skill Type</option>
                          {foreach from=$repskills2man key=sk item=item} 
                        <option  {if isset($args.chSkillType)&&$args.chSkillType==$item}selected=selected{/if} {if isset($repskill)&&$repskill===$item}selected=selected{/if} value="{$item}"> {$sk}</option>
                         {/foreach}  
                       </select>
                        </div>
                         <div id="aptypes1" style="display:none">
                          <select>
                        <option  value="---">Please select Appointment Type</option>
                         {foreach from=$appTypes key=sk item=item} 
                        <option {if isset($args.chAppType)&&$args.chAppType==$item}selected=selected{/if} {if isset($apptype)&&$apptype===$item}selected=selected{/if} value="{$item}"> {$sk}</option>
                         {/foreach}    
                       </select>
                        </div>
                        <div id="aptypes2" style="display:none">
                           <select>
                       <option  value="---">Please select Appointment Type</option>
                         {foreach from=$appTypes2man key=sk item=item} 
                        <option  {if isset($args.chAppType)&&$args.chAppType==$item}selected=selected{/if} {if isset($apptype)&&$apptype===$item}selected=selected{/if} value="{$item}"> {$sk}</option>
                         {/foreach}    
                        </select>
                        </div>
                        {if isset($fullPostcode)&&$fullPostcode!=""}
                            {$poste=$fullPostcode}
                            
                        {elseif isset($args.pc)&&!isset($iData.CustPostcode)}    
                        {$poste=$args.pc}
                        
                        {elseif isset($iData.CollPostcode)&&$iData.CollPostcode!=""}
                            {$poste=$iData.CollPostcode}
                           
                            {elseif isset($iData.CustPostcode)}
                                {$poste=$iData.CustPostcode}
                               
                                {else}
                                    {$poste=""}
                        {/if}
                      
                        {$adress=""}
                        
                        
                     {if $allocationType=="GridMapping"}   
                        
                 <input onchange="applyFilters(1)" title="{$poste}" value="{$poste}" onsubmit="applyFilters()" type="text" placeholder="Full address" name="address" id="postCodeFilter"  style="width:220px;text-transform:uppercase;position:relative;float:left;">{if $allocationType=="GridMapping"} 
                        {if isset($adrGeotag)&&$adrGeotag!=""}
                                 <img style="margin-top: 5px;margin-left: 5px;"  onclick="showPointMap()" title="Geotag Found"  src="{$_subdomain}/css/Base/images/green_tick.png">
                                  {else}
                                    
                                     <img style="margin-top: 5px;margin-left: 5px;"  title="Geotag not found" onclick="showPointMap()"  src="{$_subdomain}/css/Base/images/red_cross.png">
                                      {/if}  
                                      {/if}<img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="GeoTagHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                      
                 {else}
                      <input onchange="applyFilters(1)" title="Valid UK postcode first part" value="{$poste}" onsubmit="applyFilters()" type="text" placeholder="Postcode" name="PostCode" id="postCodeFilter"  style="width:220px;text-transform:uppercase;position:relative;float:left;">
                      {if isset($adrGeotag)&&$adrGeotag!=""}
                                  <img style="margin-top: 5px;margin-left: 5px;"  onclick="showPointMap()" title="Geotag Found"  src="{$_subdomain}/css/Base/images/green_tick.png">
                                  {else}
                                    
                                       <img style="margin-top: 5px;margin-left: 5px;"  title="Geotag not found" onclick="showPointMap()"  src="{$_subdomain}/css/Base/images/red_cross.png">
                                      {/if}  
                                     <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="GeoTagHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                     
                 {/if}
                
                 
                 
                 
                 <br>
                 <br> <input  title="Only AM slots will be displayed" {if isset($args.am)}checked=checked{/if} onclick="$('#chPM').prop('checked',false);$('#chAT').prop('checked',false);applyFilters()"type="checkbox" id="chAM" name="chAM"> AM
                        <input title="Only PM slots will be displayed" {if isset($args.pm)}checked=checked{/if} onclick="$('#chAM').prop('checked',false);$('#chAT').prop('checked',false);applyFilters()" type="checkbox" id="chPM" name="chPM"> PM
                        <input   title="Only ANY time slots will be displayed" {if isset($args.any)}checked=checked{/if} type="checkbox" id="chAT" name="chAT" onclick="$('#chPM').prop('checked',false);$('#chAM').prop('checked',false);applyFilters()"> AT
                          
                        <span style="margin-top:0px;float:right;position:relative;" >
                              <a  onclick="showMultiMap();" style="cursor:pointer">
                                  <img  title="Open Multimap Window" width="47px" height="47px" src="{$_subdomain}/css/Base/images/multimaps_icon.png">
                              </a>  
                          </span>
                              
                </fieldset>
              </form>
                        <table>
                            <tr>
                                <td style="text-align:left">
                                
                                </td>
                
                            </tr>
                        </table>
             
                
                </div>
		
                   
            <div id="box2" class="commandDiv" style="display:none;height:298px;width:400px;padding-top:10px">
              <form>
                <fieldset class="st" style="float:left;width:280px;padding:0px; padding-top:10px;padding-left:10px;height:285px;background-color:#F7F7F7 !important;position:relative;top:0px;">
                <legend style="font-size:12px;background:#F7F7F7;margin:0px;padding:0px">Service Skills:</legend>
                <div style="border:none;width:250px;background:none;height:250px;overflow-y:scroll;margin-top:25px;padding-top:0px">
               <input  type="checkbox" id="tagAllSkillsets" onclick=""> Tag/Untag all<br><br>
                    
               {foreach $skillsFull as $sk} 
              <input checked="checked"  {if !isset($args.pc)||isset($args.table)&&$args.table==""}checked="checked"{/if} {if !isset($args.tSkills)&&is_array($args.tSkills)}checked="checked"{/if}    {if isset($args.tSkills)&&is_array($args.tSkills)&&in_array($sk.ServiceProviderSkillsetID,$args.tSkills)}checked=checked{/if}  type="checkbox"  name="st[]" id="st{$sk.ServiceProviderSkillsetID}" value="{$sk.ServiceProviderSkillsetID}"><span style="font-size:10px"> {$sk.SkillsetName}</span><br>
               {/foreach}
            
               </div>
               </fieldset>
              </form>
              
             
             
              
            </div>
       
        
            <div id="box3" class="commandDiv" style="display:none;;height:298px;width:400px;padding-top:10px">
              <form style="width:280px;float:left">
                <fieldset style="width:280px;padding:0px; padding-top:0px;padding-left:10px;height:240px; position:relative; float:left;background:#F7F7F7;position:relative;top:0px" class="st">
                <legend style="font-size:12px;background:#F7F7F7;margin:0px;padding:0px;">Engineers:</legend>
               <div style="border:none;width:250px;background:none;height:190px;overflow-y:scroll;margin-top:25px;padding-top:0px">
              
                   
                   {foreach $engineerList as $ff}
                              <input onclick="checkSpecify();hideConfirmBut();"   type="radio" value="{$ff.ServiceProviderEngineerID}" name="SPID[]">{if isset($ff.EngineerFirstName)}{$ff.EngineerFirstName}{/if}{if isset($ff.EngineerLastName)} {$ff.EngineerLastName}{/if}<br>
               {/foreach}
               <br>
               <br>
               <input onclick="unspecifyBut()"  type="radio" value="0" name="SPID[]">MARK AS UNSPECIFIED <br>
                </div>
               <div style="position:absolute;top:250px;background: none;padding:0;margin:0;border:0;height:20px;width:100px" >
                  <button type="button" onclick="specifyEngineer();" id="specifyEngButtonDiary" disabled="disabled" class="btnStandard" style="color:white;width:150px;background:#6b6b6b;" >Specify Engineer</button>
                 
                  <button type="button" onclick="unspecifyEngineer();" id="unspecifyEngButtonDiary"  class="btnStandard" style="color:white;width:150px;display:none;" >Confirm</button>
               </div>
                </fieldset>
              </form>
              
           
            
            </div>
               
               
               <div id="box5" class="commandDiv" style="display:none;;height:298px;width:400px;padding-top:10px">
              <form style="width:280px;float:left">
                <fieldset style="width:300px;padding:0px; padding-top:10px;padding-left:10px; position:relative; float:left;background:#F7F7F7;position:relative;top:0px;padding-bottom:10px" class="st">
                <legend style="font-size:12px;background:#F7F7F7;margin:0px;padding:0px;">Status:</legend>
               
                <input type="radio" checked="checked" name="mComplete" id="mComplete" value=1> Mark as Closed<br>
                <input type="radio" name="mComplete" id="mInComplete" value=2> Mark as Open
               
              <br>
                  <button type="button" onclick="markAsComplited();" id="markCompleted"  class="btnStandard" style="position:relative;float:left;color:white;width:100px;" >Save</button>
                
             
                </fieldset>
                  
                  <fieldset style="width:300px;padding:0px; padding-top:10px;padding-left:10px; position:relative; float:left;background:#F7F7F7;position:relative;top:0px;padding-bottom:10px" class="st">
                <legend style="font-size:12px;background:#F7F7F7;margin:0px;padding:0px;">Filter table by:</legend>
               
               
                <input  onclick="filterTable('APCOMPL')" type="radio" name="mFilterComplete" id="mComplete"> Closed Appointments<br>
                <input onclick="filterTable('APNCOMPL')" type="radio" name="mFilterComplete" id="mInComplete"> Open Appointments<br>
                <input onclick="filterTable('')" checked="checked" type="radio" name="mFilterComplete" id="mComplete"> All
             
                </fieldset> 
                  
                  
              </form>
              
           
            
            </div>
            
            <div id="box7" class="commandDiv" style="display:none;;height:298px;width:400px;padding-top:10px">
              <form style="width:280px;float:left">
                <fieldset style="width:300px;padding:0px; padding-top:10px;padding-left:10px; position:relative; float:left;background:#F7F7F7;position:relative;top:0px;padding-bottom:10px" class="st">
                <legend style="font-size:12px;background:#F7F7F7;margin:0px;padding:0px;">Email</legend>
                <input type="radio" checked="checked" name="apptRemMail" id="apptRemMail" value="1" onclick="selectAppointments('all');"> Tag all appointments<br>
                <input type="radio" name="apptRemMail" id="mapptRemMail" value="2" onclick="selectAppointments('none');"> Manually Select appointments
                <br>
                <button type="button" onclick="selectAppointments('sel');" id="apptRem"  class="btnStandard" style="position:relative;float:left;color:white;width:100px;" >Send Emails</button>
                <input type="hidden" name="selApptRem" id="selApptRem" value="" />
                </fieldset>
              </form>
            </div>

	</div>

</div>
       </div>
           
               <input type="hidden" id="vtype" value="">        
               <input type="hidden" id="setappwind" value="">        
      {if !isset($slots)}{$slots=[]}{/if}
         {if isset($daySelected)}
             <div style="margin-top:400px;display:none;text-align: center;border:solid 1px #D7D7D7;background-color:#DDFFBB;width:100%;margin-bottom:20px;padding-bottom:20px" id="rebookMsg"><br>
                 <h3 style="color:red">To rebook this appointment select an alternative day</h3>
                 <p id="rebookAppInfo"></p>
              <button  class="btnStandard" style="float:none;color:white;" onclick="cancelRebooking()">Cancel</button>
             </div>
             <div style="margin-top:400px;display:none;text-align: center;border:solid 1px #D7D7D7;background-color:#DDFFBB;width:100%;margin-bottom:20px;padding-bottom:20px" id="rebookConfirm"><br>
                 <h3 style="color:red">Rebook to this date?</h3>
                 <p>{if isset($editRowData)}{$editRowData}{/if}</p>
              <button  class="btnStandard" style="float:none;color:white;" onclick="confirmRebook()">Confirm Rebooking</button>
              <button  class="btnStandard" style="float:none;color:white;" onclick="cancelRebooking()">Cancel</button>
             
             </div>
             <div id="mainTableID" style="display:{if isset($args.table)&&$args.table==1}block{elseif in_array($actionType,[1,2,3,4])}block{else}none{/if}">
            &nbsp;
            <div style="border: 0px solid black;height:120px;/height:120px;width:100%;margin-top:-20px;background-color:white;"> 
                <div  id="dateHolder" style="float:left;margin-top:40px"><h4 style="color:#0189CD;font-weight:bolder"><span style="color:gray;font-weight:normal">Selected Date:</span>  <span id="selectedDateID" style="font-size:18px">{$daySelected|date_format:"%A %e %B %Y"}</span></h4></div>

                 
                <div style="float:right"> 
              <form class="st" style="margin-top:0px;float:left">
              <fieldset class="st" style="margin:0px;padding:0px;color:#666;width:383px;float:left;background:none;">
             <legend style="font-size: 12px;background-color: none;color:#6296CA;margin-left:20px">Table Commands</legend>
              <table style="cellspacing:0px;cellpadding:0px;padding:0px;">
                <tbody>
                <tr>
<!--                <td style="text-align: center;margin-top:5px" aligment="center"><a href="#" id="SendDiaryEmail" ><img  title="Send email to selected customers" width="50" height="50" src="{$_subdomain}/css/Base/images/email_icon_st.png"></a></td>-->
<!--                <td style="text-align: center;margin:0px" align="center"><a onclick="window.location='{$_subdomain}/Diary/testSt'" style="cursor:pointer"><img  title="Send text message to selected customers" width="50" height="50" src="{$_subdomain}/css/Base/images/sms_icon_st.png"></a></td>-->
                 {if $diaryType=="FullViamente"}
                    <td style="text-align: center;margin:0px" align="center"><a onclick="showTimeSliders()" style="cursor:pointer"><img  title="Adjust time sliders" width="50" height="50" src="{$_subdomain}/css/Base/images/timeline_icon.png"></a></td>
                {/if}
                    <td style="text-align: center;margin:0px" align="center"><a onclick="showPrintMenu()" style="cursor:pointer"><img  title="Print details of selected appointments" width="50" height="50" src="{$_subdomain}/css/Base/images/print_icon_st.png"></a></td>
               <td style="text-align: center;margin:0px" align="center"> <a style="cursor:pointer;" target="new" onclick="showGMap()"><img title="Show map with current appointments" width="50" height="50" src="{$_subdomain}/css/Base/images/map_icon_st.png"></a></td>
                 <td style="text-align: center;margin:0px" align="center">  <a onclick="showSamsungFilter();" style="cursor:pointer"><img title="Display only SAMSUNG appointments" width="50" height="50" src="{$_subdomain}/css/Base/images/samsung_icon_st.png"></a></td>
           {if $diaryType=="FullViamente"}
                 <td style="text-align: center;margin:0px" align="center"> <a {if isset($daySelected)}href="#" {if !in_array($daySelected|date_format:"%Y-%m-%d",$finalizedDays)} {if $daySelected|date_format:"%d%m%Y" != $smarty.now|date_format:"%d%m%Y" || $vrunToday=="Yes"}onclick="insertWaitL(1)"{else}onclick="insertWaitL(5)"{/if}{else}onclick="alert('The selected day has been finalised therefore future optimisation is not currently available. You can re-optimise individual engineers using the Time Sliders, alternatively reset Viamente consolidation for this day in general defaults and repeat the finalise day procedure.')"{/if}{else}onclick="alert('To optimise route please select day first')"{/if} ><img title="Optimise route with viamente" width="50" height="50" src="{$_subdomain}/css/Base/images/v_icon_st.png"></a></td>
            {/if}
<!--                <td style="text-align: center;margin:0px" align="center"> <a onclick="alert('INDEVELOPMENT')" target="new"><img title="Send appointments to TomTom" width="50" height="50" src="{$_subdomain}/css/Base/images/icons_tomtom.png"></a></td>-->
                
             
             
             
             
            
                </tr>
                </tbody>
                </table>
                </fieldset>
              </form>
             </div>
<style>
#matches_table_info{
   
    }
</style>
<div style="float:left;margin-top:20px;margin-left:55px">
    <input type="radio" id="tableView1" name="tableMode" {if  in_array($actionType,[1,6])}checked=checked{/if} onclick="settablemode(1);"> View A<br>
    <input type="radio" id="tableView2" name="tableMode" {if  !in_array($actionType,[1,6])}checked=checked{/if} onclick="settablemode(2);"> View B
                    </div>
             </div>
            
              <fieldset style="width:auto;background:#FFF;float:left;">
                    <legend style="font-size:12px;margin-left: 0px">Appointments</legend>

                <button  class="btnStandard" {if  in_array($actionType,[1,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if} id=""  type=button  onclick="insertTimeCheck()"><span style="color:white">Insert</span></button>
                <button id="edit2"  type=button class="btnStandard" {if  in_array($actionType,[2,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Edit</span></button>
                <button id="delete2"  type=button class="btnStandard" {if  in_array($actionType,[3,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Delete</span></button>
                <button id="" onclick="rebookMode('rebookMsg')"  type=button class="btnStandard" {if  in_array($actionType,[4,2,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Rebook</span></button>
               
                </fieldset>
                <fieldset style="width:385px;background:#FFF;float:right;padding-bottom:10px;/padding-bottom:5px">
                    <legend style="font-size:12px;margin-left: 0px;">Table Defaults</legend>
               
                <button style="float:left;margin-right: 4px;width:120px;" class="btnStandard"  onclick="checkCompletedApp();setActiveTab('3')" ><span class="label">Specify Engineers</span></button>
                 <!--<button style="float:left;margin-right: 4px;width:110px;" type="button" onclick="checkCompletedApp();setActiveTab('6');" id="EditApptDurationButtonDiary"  class="btnStandard"  ><span class="label">Edit Appt Duration</span></button>-->
                <!--<button style="float:right" class="btnStandard" onclick="setActiveTab('2')" style="background:#6b6b6b;color:white;width:140px"><span class="label">Service Skills</span></button>-->
                <button style="float:left;margin-right: 4px;width:110px;" class="btnStandard"  onclick="setActiveTab('5')" ><span class="label">Update Status</span></button>
                    <button style="float:right;margin-right: 4px;width:120px;" class="btnStandard" onclick="setActiveTab('7'); selectAppointments('all');" ><span class="label">Email Reminders</span></button>
              </fieldset>
                <div {if $unrdayvar=='no'}style="display:none;"{else}style="float:left;margin-top:5px"{/if}><button class="btnStandard" onclick="showUnreachedMap()">Show Appointment Map</button>
                    <INPUT TYPE="checkbox" id="filterUnreachb" onclick="filterTable('Unreachable');$(this).hide();$('#filterUnreachb2').show();$(this).attr('checked',false)">
                    <INPUT style="display:none" checked=checked TYPE="checkbox" id="filterUnreachb2" onclick="filterTable('');$(this).hide();$('#filterUnreachb').show();$(this).attr('checked','checked')">
                    <span style="color: #FF0000;">Show Unreachable Appointments Only</span>
                </div>
                <div {if $unrdayvar!='no'}style="float:right;width:900px;height:2px;"{else}style="float:right;width:900px;height:20px;"{/if}>&nbsp;</div>
                
             <table   id="matches_table" border="0" cellpadding="0" cellspacing="0" class="browse" >
                 
                 <thead>
                     <tr>
                
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important;height:20px">ID</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Status filter</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">SO</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Time Slot</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">From To</th>
                <th  class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Start<br><br>Travel Time</th>
                <th  class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Arrive Time</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Visit Time</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Depart<br><br>Day Finish</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Post Code</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Town</th>
                <th  class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important;height:20px">Engineer Name</th>
               
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Customer Name</th>
                 <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Manufacturer Client</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Product Type</th>
<!--                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Model</th>-->
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">SB Job<br>SL Job</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Appointment</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Tag</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important">Error filter</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important;display:none">Bookedby</th>
                <th class="diaryDataTable" style="background-position:right bottom;font-size:10px !important;font-weight:normal;line-height: 1!important;display:none">Service Type</th>
                </tr>
                </thead>
                <tbody>
              
                       {$samsung=5}
                       {$engfinish=""}
                       {$engstart=""}
                    {for $er=0 to $slots|@count-1}
                    {$engfinish=""}
                         {$so="p"} 
                    {if isset($slots[$er].deliveryPostcode)&&$slots[$er].deliveryPostcode!=""}{$pcc=$slots[$er].deliveryPostcode}{else}{if isset($slots[$er].PostalCode)}{$pcc=$slots[$er].PostalCode}{else}{$pcc=""}{/if}{/if}
                    {if isset($slots[$er].CompletedBy)}{$APstatus=$slots[$er].CompletedBy}{else}{$APstatus=""}{/if}
                    {if isset($slots[$er].ModelNumber)&&$slots[$er].ModelNumber!=""}{$modelNr=$slots[$er].ModelNumber}{elseif isset($slots[$er].ServiceBaseModel)&&$slots[$er].ServiceBaseModel!=""}{$modelNr=$slots[$er].ServiceBaseModel}{else}{$modelNr="MODEL NO.  NOT AVALIABLE"}{/if}
                    {if isset($slots[$er].UnitTypeName)}{$unitType=$slots[$er].UnitTypeName}{elseif isset($slots[$er].ServiceBaseUnitType)&&(!isset($slots[$er].UnitTypeName)||$slots[$er].UnitTypeName=="")}{$unitType=$slots[$er].ServiceBaseUnitType}{else}{$unitType=""}{/if}
                    {if isset($slots[$er].EngineerAssigned)}
                           {if isset($slots[$er].EngineerAssigned)}
                             {if $diaryType=="FullViamente"}
                               {if isset($sortNo[$slots[$er].EngineerAssigned])}
                    {$sortNo[$slots[$er].EngineerAssigned]=$sortNo[$slots[$er].EngineerAssigned]+1}
                    {$so=$sortNo[$slots[$er].EngineerAssigned]}
                    {$engstart=""}
                     
                    {if !isset($slots[$er+1].EngineerAssigned)||$slots[$er].EngineerAssigned!=$slots[$er+1].EngineerAssigned}
                        {$engfinish=$slots[$er].ViamenteReturnTime|date_format: "%H:%M"}
                        {else}
                            {$engfinish=""}
                        {/if}
                        {else}
                            {$sortNo[$slots[$er].EngineerAssigned]=1}
                             {$so="1"}
                             {$engstart=$slots[$er].EngstartTime|date_format:"%H:%M"}
                         {/if}
                         
                    {else}
                        
                        {$so="-"}
                           {if  isset($slots[$er].SortOrder)&&$slots[$er].SortOrder!=""} {$so=$slots[$er].SortOrder} {/if}
                        {/if}
                        {else}
                             {if  isset($slots[$er].SortOrder)&&$slots[$er].SortOrder!=""} {$so=$slots[$er].SortOrder} {/if}
                        {/if}
                        {else}
                            {$so="-"}
                        {if  isset($slots[$er].SortOrder)&&$slots[$er].SortOrder!=""} {$so=$slots[$er].SortOrder} {/if}
                    {/if}
                    {assign var="appt{$slots[$er].ServiceProviderEngineerID}" value="{$slots[$er].ForceEngineerToViamente}"} 
                    
                    <tr id="{if isset($slots[$er].AppointmentID)}{$slots[$er].AppointmentID}{/if}">
                        
                      <td class="diaryDataTable">{if isset($slots[$er].AppointmentID)}{$slots[$er].AppointmentID}{/if}</td>
                      <td class="diaryDataTable">{if isset($slots[$er].CompletedBy)&&$slots[$er].CompletedBy!=""}APCOMPL{else}APNCOMPL{/if}</td>
                      <td class="diaryDataTable" >{$so}</td>
                      <td class="diaryDataTable" {if isset($slots[$er].Colour)&&$slots[$er].Colour!=""}style="background-color:#{$slots[$er].Colour} !important"{/if}>{if isset($slots[$er].AppointmentTime)}{$slots[$er].AppointmentTime} {else}ERROR{/if}{if $APstatus=="Manual"}<br><img title="Completed by User" style="border:none" src="{$_subdomain}/css/Base/images/orange_tick.png">{/if}{if $APstatus=="Remote Engineer"}<br><img style="border:none" title="Completed by Remote Engineer" src="{$_subdomain}/css/Base/images/green_tick.png">{/if}{if $APstatus=="Out Card Left"}<br><img style="border:none" title="Out Card Left" src="{$_subdomain}/images/diary/blue_tick.png">{/if}</td>
                            <td class="diaryDataTable">
                    {if isset($slots[$er].AppointmentStartTime)}{$slots[$er].AppointmentStartTime|date_format:"%H:%M"}{/if}<br>
                    {if isset($slots[$er].AppointmentEndTime)}{$slots[$er].AppointmentEndTime|date_format:"%H:%M"}{/if}<br>
                    {if isset($slots[$er].AppointmentStartTime2)&&AppointmentStartTime2!="00:00:00"}{$slots[$er].AppointmentStartTime2|date_format:"%H:%M"}<br>{/if}
                    {if isset($slots[$er].AppointmentEndTime2)&&AppointmentEndTime2!="00:00:00"}{$slots[$er].AppointmentEndTime2|date_format:"%H:%M"}{/if} 
                    </td>
                            <td class="diaryDataTable" {if $engstart!=""}style="color:blue !important"{/if}>{$engstart}<br>{if isset($slots[$er].ViamenteTravelTime)}{$slots[$er].ViamenteTravelTime|date_format: "%H:%M"}{/if}</td>
                            
                            <td style=" " class="diaryDataTable">{if isset($slots[$er].ViamenteStartTime)}{$slots[$er].ViamenteStartTime|date_format: "%H:%M"}{/if}</td>
                            <td class="diaryDataTable" {if isset($pcCount[$pcc])}{if $pcCount[$pcc]>1}style="background-color:#ffff99"{/if}{/if} >{if isset($slots[$er].Duration)}{$slots[$er].Duration}{/if}</td>
                            <td class="diaryDataTable" {if $engfinish!=""}style="color:blue !important;"{/if}>{if isset($slots[$er].brStart)&&$slots[$er].brStart!=""}<span style="color:red" title="{$slots[$er].brStart|date_format:"%H:%M"} - {$slots[$er].brEnd|date_format:"%H:%M"}">Break</span><br>{/if}{if isset($slots[$er].ViamenteDepart)}{$slots[$er].ViamenteDepart|date_format: "%H:%M"} {/if}{$engfinish}</td>
                            <td class="diaryDataTable" {if isset($slots[$er].Latitude) && $slots[$er].Latitude==0.0}style="background-color:red;color:white;"{$noGeoTag=true}{else}{$noGeoTag=false}{/if}{if isset($pcCount[$pcc])}{if $pcCount[$pcc]>1}style="background-color:#ffff99"{/if}{/if} title="{if isset($slots[$er].fulladress)}{$slots[$er].fulladress}{/if} {if $noGeoTag}, Location Not Found, Please see FAQ for further details.{/if}">{$pcc}</td>
                            <td class="diaryDataTable" >{if isset($slots[$er].TownCity)}{$slots[$er].TownCity}{/if}</td>
                          {if $diaryType=="FullViamente"}
                            <td class="diaryDataTable" {if $slots[$er].ForceEngineerToViamente==1&&$slots[$er].TempSpecified!="Yes"}title="This engineer is manualy specified"{else}title="This engineer is specified by Skyline"{/if} ><span style="text-decoration:none;border-bottom:solid 2px {if isset($slots[$er].RouteColour)&&$diaryType=="FullViamente"}#{$slots[$er].RouteColour}{else}none{/if};{if $slots[$er].ForceEngineerToViamente==1&&$slots[$er].TempSpecified!="Yes"}font-weight: bold;{/if}">{if isset($slots[$er].EngineerAssigned)}{$slots[$er].EngineerAssigned}{elseif $slots[$er].ViamenteUnreached==0&&($daySelected|date_format:"%Y%m%d")<=(($smarty.now+6*24*60*60)|date_format:"%Y%m%d")*1}<span style="color:red;text-decoration:blink;font-weight:bold" title="It is possible that the appointment has a invalid or new postcode. Please check the location of the appointment, and either manually correct the postcode in Service Base if it is incorrect, or by moving the appointment pin within the Skyline Appointment Card.">AWAITING VIAMENTE</span>{elseif $slots[$er].ViamenteUnreached==0}<span style="color:green;text-decoration:blink;font-weight:bold" title="Viamente optimisation will return engineer names for appointments in the next 7 days only">Pending Viamente</span>{/if}</span> {if $slots[$er].ViamenteUnreached==1}<span style="color:red;text-decoration:blink;font-weight:bold" title="Viamente cannot allocate this appointment to any of assigned engineers, please verify working time is set correct and is sufficiant to fullfill all appointments.">Unreachable</span>{/if}{if isset($slots[$er].hasdHoliday)}<span title="This Engineer is Absent between {$slots[$er].HolidayStart} - {$slots[$er].HolidayEnd}" style="color:red" > (A)</span> {/if}</td>
                           {/if}
                           {if $diaryType=="NoViamente"}
                            <td class="diaryDataTable"  >{if isset($slots[$er].EngineerAssigned)}{$slots[$er].EngineerAssigned}{else}<span style="color:red">Engineer Unassigned</span>{/if}</td>
                          {/if}
                            <td class="diaryDataTable" {if $samsung<2}style="background-color:#0C4DA2;color:#FFF;font-weight:bold"{/if}>{if isset($slots[$er].ContactLastName)}<a href="#" style="{if $samsung<2}color:#FFF;{else}color:#6b6b6b;{/if}text-decoration:underline">{if isset($slots[$er].Title)}{$slots[$er].Title}{/if} {$slots[$er].ContactLastName}</a>{else}{if $samsung<2}SAMSUNG{/if}{/if}</td>
                            <td class="diaryDataTable">{if isset($slots[$er].ManufacturerName)}{$slots[$er].ManufacturerName}{/if}{if isset($slots[$er].ServiceBaseManufacturer)&&(!isset($slots[$er].ManufacturerName)||$slots[$er].ManufacturerName=="")}{$slots[$er].ServiceBaseManufacturer}{/if} <br>{if isset($slots[$er].Client)}{$slots[$er].Client}{/if}</td>
                             <td class="diaryDataTable" title="{$modelNr}">{$unitType}</td>
<!--                             <td class="diaryDataTable">{if isset($slots[$er].ModelNumber)}{$slots[$er].ModelNumber}{/if}{if isset($slots[$er].ServiceBaseModel)&&(!isset($slots[$er].ModelNumber)||$slots[$er].ModelNumber=="")}{$slots[$er].ServiceBaseModel}{/if}</td>-->
                            <td style="font-size:10px;width:51px;text-align:center;"><span title="Service Base Job Number">{if isset($slots[$er].sbJobNo)}{$slots[$er].sbJobNo}{/if}</span><br><b style="color:#158DC6" title="Skyline Job Number">{if isset($slots[$er].JobID)}{$slots[$er].JobID}{/if}</b></td>
                            <td class="diaryDataTable" style="{if isset($slots[$er].Notes)}{if $slots[$er].Notes|strstr:"[2p]"}background-color:#ffff99;{/if}{/if}" title="{if isset($slots[$er].Notes) &&  isset($slots[$er].PreVisitContactRequired) && $slots[$er].PreVisitContactRequired == 'Y'}{$slots[$er].Notes|cat:" "}{else}{$slots[$er].Notes}{/if}{if isset($slots[$er].PreVisitContactRequired) && $slots[$er].PreVisitContactRequired == 'Y'}{$slots[$er].PreVisitContactInfo}{/if}">
                            {if isset($slots[$er].SkillsetName)}{$slots[$er].SkillsetName}{/if}
                            {if isset($slots[$er].LockPartsEngineer)&&$slots[$er].LockPartsEngineer=='Yes'} <span style="color:red">[Parts]</span>{/if}
                            </td>
                            
                            <td class="diaryDataTable"><input type="checkbox" class="apptTagChk" value="{if isset($slots[$er].AppointmentID)}{$slots[$er].AppointmentID}{/if}" onclick="addSelectedApp({if isset($slots[$er].AppointmentID)}{$slots[$er].AppointmentID}{/if})"></td>
                            <td class="diaryDataTable">
                            {if $slots[$er].ViamenteUnreached==1}Unreachable{/if}
                            {if ($daySelected|date_format:"%Y%m%d")<=(($smarty.now+6*24*60*60)|date_format:"%Y%m%d")*1&&$slots[$er].ViamenteStartTime==""}
                            Unreachable
                                {/if}
                            </td>
                            <td class="diaryDataTable" style="display:none">{$slots[$er].BookedBy|default:'&nbsp;'}</td>
                            <td class="diaryDataTable" style="display:none">{if isset($slots[$er].ManufacturerName)}{$slots[$er].ManufacturerName}{/if}{if isset($slots[$er].ServiceBaseManufacturer)&&(!isset($slots[$er].ManufacturerName)||$slots[$er].ManufacturerName=="")}{$slots[$er].ServiceBaseManufacturer}{/if} {$slots[$er].ServiceType|default:'&nbsp;'}</td>
                   </tr>
                   
                  
                            {/for}
                 
                </tbody>
             </table>
                <fieldset style="width:auto;background:#FFF">
                    <legend style="font-size:12px;margin-left: 0px">Appointments</legend>
                    
                <button  class="btnStandard" {if  in_array($actionType,[1,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if} id=""  type=button  onclick="insertTimeCheck()"><span style="color:white">Insert</span></button>
                <button id="edit"  type=button class="btnStandard" {if  in_array($actionType,[2,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Edit</span></button>
                <button id="delete"  type=button class="btnStandard" {if  in_array($actionType,[3,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Delete</span></button>
                <button id="" onclick="rebookMode('rebookMsg')"  type=button class="btnStandard" {if  in_array($actionType,[4,2,5,6])}style="width:100px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:100px"{/if}><span style="color:white">Rebook</span></button>
<!--                <button id=""  type=button class="btnStandard" onclick="showInsertSlot()" {if  in_array($actionType,[5,1,6,2])}style="width:120px;"{else}disabled="disabled" style="background:#6b6b6b;color:white;width:120px"{/if}><span class="label">Add Slot</span></button>-->
                    </fieldset>
             </div>
            {/if}
         </fieldset>
                               
</div>
 <div id="waitDiv" style="display:none">
     <div style="min-height:100px;min-width: 100px;">
        <img src="{$_subdomain}/images/processing.gif">
        <div style="text-align: center">Please wait!</div></div> </div>    
 <div id="ViaDiv1" style="display:none;font-size: 14px;width:650px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:650px;margin-bottom: 0px; text-align: center; ">
         <fieldset>
             <legend>Optimise Route with Viamente</legend>
             <div style="margin-top:10px"></div>
        Would you like to run a "Routine Optimisation" or "Finalise" and consolidate the day?
        <div style="margin-top:20px"></div>
       {if $ServiceProviderID!=64}
          <a > <button id=""  type=button class="btnStandard"   onclick="insertWaitL(3)" style="width:200px;float:left"><span class="label">Routine Optimisation</span></button>   </a>
          <a >  <button id=""  type=button class="btnStandard" onclick="insertWaitL(2)" style="width:200px;float:right"><span class="label">Finalise Day</span></button>   </a>
       {else}
        <a > <button id=""  type=button class="btnStandard"   onclick="insertWaitL(7)" style="width:200px;float:left"><span class="label">Routine Optimisation</span></button>   </a>
        <a >  <button id=""  type=button class="btnStandard" onclick="insertWaitL(8)" style="width:200px;float:right"><span class="label">Finalise Day</span></button>   </a>
        <div style="margin-top:30px"><br>
            
        <input onclick="$('#vtype').val('Brown');$('.errMsgP1').html('')" type="radio"  name="vtype" value="Brown">Brown Goods
        <input onclick="$('#vtype').val('White');$('.errMsgP1').html('')" type="radio" name="vtype" value="White">White Goods
        <input onclick="$('#vtype').val('Both');$('.errMsgP1').html('')" type="radio" name="vtype" value="Both">Both
        
        </div>
        <p class="errMsgP1" ></p> 
       {/if}
       
       {if $spinfo['LockAppWindow']=="Yes"}
           <div style="float:left;"><br>Lock Appointment Time Window: <input type="checkbox" onclick="$('#setappwind').val($(this).attr('checked'))"></div>
       {/if}
         </fieldset>
       </div> </div>  
        
        
        
        <div id="ViaDiv2" style="display:none;font-size: 14px;width:650px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:650px;margin-bottom: 0px;">
         <fieldset>
             <legend>End of Day Process</legend>
             <div style="margin-top:5px"></div>
             By selecting "Proceed" you are confirming that you would like to consolidate the day, by doing so you will prevent additional Samsung One Touch Bookings being made this day.  
             If you wish to add more appointments you can do using the "Force Add" option.
             <div style="margin-top:10px"></div>
           <button id=""  type=button class="btnStandard"  onclick="insertWaitL(4)" style="width:200px;float:left"><span class="label">Proceed</span></button>   
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:200px;float:right"><span class="label">Cancel</span></button><br><br>
           <div style="margin-top:5px"></div>
           Please note:  You can "Reset Viamente Consolidation" in General Defaults 
          </fieldset>
       </div> </div>   
         <div id="ViaDivError" style="display:none;font-size: 14px;width:500px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:500px;margin-bottom: 0px;">
         <fieldset>
             <legend>End of Day Process</legend>
             <span style="color:red">The selected day cannot be finalised at this time due to outstanding errors or unreachable appointments. Please rectify the issues before finalising the day.</span>

       
           <br>
           <br>
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:200px;float:right"><span class="label">Ok</span></button>   
          </fieldset>
       </div> </div>  
        
        
        
        <div id="ViaDiv3" style="display:none;font-size: 14px;width:500px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:500px;margin-bottom: 0px;">
         <fieldset>
             <legend>End of Day Process</legend>
             <span style="color:red">For today Viamente button is disabled</span>

       
         
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:200px;float:right"><span class="label">Close</span></button>   
          </fieldset>
       </div> </div>   
        
        
        
        <div id="insertWarning" style="display:none;font-size: 14px;width:500px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:500px;margin-bottom: 0px;">
         <fieldset>
             <legend>Warning</legend>
             <span style="color:red">We are preparing to run the EOD routine, if you wish to book this appointment you must get approval from your supervisor</span>

       
           <button id=""  type=button class="btnStandard"  onclick="confirmPass()" style="width:200px;float:left"><span class="label">Supervisor Approved</span></button>   
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:200px;float:right"><span class="label">Select another day</span></button>   
           
         </fieldset>
       </div> </div>   
        <div id="confirmPass" style="display:none;font-size: 14px;width:500px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:500px;margin-bottom: 0px;">
         <fieldset>
             <legend>Authorisation</legend>
             <span style="color:red">Please enter supervisor password in box below</span>
             <div style="text-align:center"><form><input onblur="ppp=this.value;" class="supervisorPass" id="supervisorPass" type="password" name="passw"></form></div>
       
           <button id=""  type=button class="btnStandard"  onclick="checkpass(1)" style="width:200px;float:left"><span class="label">Enter</span></button>   
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:200px;float:right"><span class="label">Select another day</span></button>   
           
         
       
        </fieldset>
       </div> </div>  
         <div>
            {include file="diary/diaryAppointmentAnalysis.tpl"}
        </div>
        {if isset($args.timeSliders)}
        <div id="diarytimesliderwarpper">
            &nbsp;
            {include file="diary/diaryTimeSliders.tpl"}
        </div>
        {/if}
        <div id="dummyHolder"></div>
        
        
        <div id="samsungTableFilter" style="display:none">
            <h3>Which Samsung appointments would you like to view?</h3>
            <p><input type="radio" onclick="filterTable('Samsung');$.colorbox.close();">All Samsung</p>
            <p><input type="radio" onclick="filterTable('Samsung Warranty');$.colorbox.close();">All Samsung Warranty</p>
            <p><input type="radio" onclick="filterTable('Samsung One Touch');$.colorbox.close();">Samsung One Touch Only</p>
        </div>
        
{/block}
