{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $County}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />       
        <style type="text/css" >
        .ui-combobox-input {
            width:300px;
        }
        /* added by srinivas for enabling full page */
        .container, #header, .breadcrumb, #container-main .main  {
        width: 98%;
        min-width: 950px;                       
        }
        /* end */
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
        
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
    var $countries = new Array();
       
        {foreach from=$allBrandsCountries item=bCountry key=brandId}
           
           $countries["{$brandId}"] = [        
            
            {foreach from=$bCountry item=ctry} 
                   ["{$ctry.CountryID}", "{$ctry.Name|escape:'html'}"],
            {/foreach}
                
           ]
           
        {/foreach}
                          
    function showTablePreferences(){
        $.colorbox({
        href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=Accessory/table=county",
                title: "Table Display Preferences",
                opacity: 0.75,
                overlayClose: false,
                escKey: false

        });
       }
    
function countTagged(){
      ch=0;
      $('.taggedRec').each(function(){
      if(this.checked) {
      ch+=1
      }
      });
      if(ch>0){
     
      $('#recordsTaggedDiv').show();
      $('#recordsTaggedSpan').html(ch);
      
      }else{
       $('#recordsTaggedDiv').hide();
      }
      }
  
     $('#check_all').click( function() {
            alert('hai');
            $('input', table.fnGetNodes()).attr('checked',this.checked);
            countTagged();
        } );
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
          // if need modification on data 
      
                    }
               
        
        
    function gotoInsertPage($sRow)
    {
       
        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
        
    }

 

    $(document).ready(function() {
          
        $('#check_all').click( function() {            
            $('input', table.fnGetNodes()).attr('checked',this.checked);
            countTagged();
        } );  
    /*$("#BrandID").combobox({
            change: function() {
                $countryDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';        
                var $brandId = $("#BrandID").val();
                if($brandId && $brandId!='')
                {
                    $brandCountries = $countries[$brandId];
                    if($brandCountries)
                    {
                        for(var $i=0;$i<$brandCountries.length;$i++)
                        {
                            $countryDropDownList += '<option value="'+$brandCountries[$i][0]+'" >'+$brandCountries[$i][1]+'</option>';
                        }
                    }
                   $("#CountryID").html($countryDropDownList);     
                }
            }
        });
        $("#CountryID").combobox();*/

                

                  //change handler for brand dropdown box.
                  $(document).on('change', '#BrandID', 
                                function() {
                                    
                                  
                                    $countryDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $brandId = $("#BrandID").val();
                                
                                    if($brandId && $brandId!='')
                                        {
                                            $brandCountries = $countries[$brandId];
                                            if($brandCountries)
                                                {
                                                    for(var $i=0;$i<$brandCountries.length;$i++)
                                                    {
                                                        $countryDropDownList += '<option value="'+$brandCountries[$i][0]+'" >'+$brandCountries[$i][1]+'</option>';
                                                    }
                                                }
                                                
                                           $("#CountryID").html($countryDropDownList);     
                                        }
                                    

                                });  
                    



                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UPA";
                     
                     {else}

                         var displayButtons =  "UP";

                     {/if} 
                     


                    
                    $('#CResults').PCCSDataTable( {                  
                    sDom: 'Rft<"#dataTables_command">rpli', 
                    "bStateSave": true,        
                    "aoColumns": [                 
			{ "mDataProp": "CountyID" , "bVisible":  false },
			{ "mDataProp": "CountyCode", "sWidth" :  "100px" },
			{ "mDataProp": "Name" ,  "sWidth" : "200px"},
                        { "mDataProp": "Cname" },            
			{ "mDataProp": "Status" },
                        { "mDataProp": "BrandName" },            
                        { "mDataProp": "BrandID"  },
                        { "mData" : null, "bSortable": false, "sWidth" : "20px",
                            "mRender": function ( data, type, full ) {
                            return '<input class="taggedRec" type="checkbox" onclick="countTagged()" value="'+full.CountyID+'" name="check'+full.CountyID+'">';
                             }
                          }            
                      
			],               
                                                      
                            
                            
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/LookupTables/county/insert/',
                            createDataUrl:   '{$_subdomain}/LookupTables/ProcessData/County/',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                            
                                                    BrandID:
                                                        {
                                                            required: true
                                                        },
                                                    Name:
                                                        {
                                                            required: true
                                                        },  
                                                    CountryID:
                                                        {
                                                            required: true
                                                        },
                                                    Status:
                                                        {
                                                            required: true
                                                        }     
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    BrandID:
                                                        {
                                                            required: "{$page['Text']['brand_error']|escape:'html'}"
                                                        },
                                                    Name:
                                                        {
                                                            required: "{$page['Text']['name_error']|escape:'html'}"
                                                        },  
                                                    CountryID:
                                                        {
                                                            required: "{$page['Text']['country_code_error']|escape:'html'}"
                                                        },
                                                        
                                                    Status:
                                                        {
                                                            required: "{$page['Text']['status_error']|escape:'html'}"
                                                        }    
                                              },                     
                            
                            popUpFormWidth:  715,
                            popUpFormHeight: 430,
                            
                            pickButtonId:"copy_btn",
                            pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                            pickCallbackMethod: "gotoInsertPage",
                            colorboxForceClose: false,
                            
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/county/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/County/',
                            formUpdateButton:'update_save_btn',
                            
                            colorboxFormId:  "CForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'CResultsPanel',
                            htmlTableId:     'CResults',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/County/fetch/',
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            //fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}
    
  <div style="float:right; padding-right: 25px">
        <a style="cursor:pointer" onclick="showTablePreferences();">Display Preferences</a>
    </div>
  
    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="countyForm" name="countyForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="LTResultsPanel" id="CResultsPanel" >

                    <table id="CResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th> 
                                        <th title="{$page['Text']['county_id_no']|escape:'html'}" >{$page['Text']['county_id_no']|escape:'html'}</th>
                                        <th title="{$page['Text']['county']|escape:'html'}" >{$page['Text']['county']|escape:'html'}</th>
                                        <th title="{$page['Text']['country']|escape:'html'}"  >{$page['Text']['country']|escape:'html'}</th>
                                        <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand_id']|escape:'html'}" >{$page['Text']['brand_id']|escape:'html'}</th>
                                        <th align="center"> &nbsp; <input title="Tag all counties" type='checkbox' value=0 id='check_all' /> </th>    
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



