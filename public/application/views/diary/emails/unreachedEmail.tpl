{if isset($data)}
<h2>{$title}</h2>
            <P>{$errorText}</P>
            {if $type == 3}
                {for $i=0 to $data|@count-1}
                        <table border=1>
                            <tr>
                                <th style="text-align: left;">Skyline Job Number</th>
                                <td>{$data[$i]['sbjobid']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Appointment Date</th>
                                <td>{$data[$i]['AppointmentDate']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Appointment Address</th>
                                <td>{$data[$i]['apptAddr']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Postcode</th>
                                <td>{$data[$i]['pc']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Time Window</th>
                                <td>{$data[$i]['AppointmentStartTime']}-{$data[$i]['AppointmentEndTime']}<br>{$data[$i]['AppointmentStartTime2']}{if $data[$i]['AppointmentStartTime2']!=""}-{/if}{$data[$i]['AppointmentEndTime2']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Type</th>
                                <td>{$data[$i]['AppointmentType']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Skill Set</th>
                                <td>{$data[$i]['SkillsetName']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Product Type</th>
                                <td>{$data[$i]['prType']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Duration</th>
                                <td>{$data[$i]['Duration']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Booked By</th>
                                <td>{$data[$i]['BookedBy']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Booking Date</th>
                                <td>{$data[$i]['CreatedTimeStamp']}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left;">Force Add Reason</th>
                                <td>{$data[$i]['ForceAddReason']}</td>
                            </tr>
                        </table>
                {/for}
            {else}
                <table border=1>
                <th>Job No.</th>
                <th>Appointment Date</th>
                <th>Postcode</th>
                <th>Time Window</th>
                <th>Type</th>
                <th>Duration</th>
                <th>Booked By</th>
                <th>Booking Date</th>
               {for $i=0 to $data|@count-1}
                    <tr>
                    <td>{$data[$i]['sbjobid']}</td>
                    <td>{$data[$i]['AppointmentDate']}</td>
                    <td>{$data[$i]['pc']}</td>
                    <td>{$data[$i]['AppointmentStartTime']}-{$data[$i]['AppointmentEndTime']}<br>{$data[$i]['AppointmentStartTime2']}{if $data[$i]['AppointmentStartTime2']!=""}-{/if}{$data[$i]['AppointmentEndTime2']}</td>
                    <td>{$data[$i]['AppointmentType']}</td>
                    <td>{$data[$i]['Duration']}</td>
                    <td>{$data[$i]['BookedBy']}</td>
                    <td>{$data[$i]['CreatedTimeStamp']}</td>
                    </tr>
                {/for}
                </table>
            {/if}
            
          
            {if $type==1}
                  <P>To resolve the issue action one of the following:</p>
            <ul>
                <li>Check the table for appointments at the same address and reduce the visit times accordingly</li>
                <li>Extend the service times of an engineer with the correct skill set</li>
            </ul>
                  <p>Run Viamente once you have made changes to confirm the issue is resolved</p>
            {/if}
            {if $type==2}
                  <P>To resolve the issue please can you contact the customer and verify the correct address:</p>
            <ul>
                <li>If the postcode has been entered incorectly update the job details in ServiceBase and save changes</li>
                <li>If postcode is not registered with Viamente, edit the appointment map details to save a valid GeoTag point</li>
            </ul>
                  <p>Run Viamente once you have made changes to confirm the issue is resolved</p>
            {/if}
            {if $type==3}
                  <P>To resolve the issue you must now re-optimise driver routes using the time sliders</p>
                  <p><b>Please note:</b> You may also need to extend this drivers working day to service the additional appointment</p>
            {/if}
             {if $type==4}
                  <P>To resolve the issue action one of the following:</p>
            <ul>
                <li>Re-optimize a drivers route to include the new appointment/s</li>
              
            </ul>
                  <p>Please note: you may also need to extend this drivers working day to service the additional appointment/s</p>
            {/if}
            {/if}