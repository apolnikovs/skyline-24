<!DOCTYPE html>

    {*********************************************************************
     *
     * Define Global Varables
     *
     *********************************************************************}
     
        {$wallboardPage = 1}
        
        {$Title = "Wall Board"}
        
        {block name=config}
        {/block}
    
    {**********************************************************************
     * SMARTY Functions:
     *
     *
     **********************************************************************}
     
    
    {***********************************************************************}
    
<head>

    <meta charset="utf-8" />
      
    {********************************************************************
     *
     * Add your meta tags here  
     *
     *******************************************************************}
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="language" content="en" />
    <meta name="author" content="" />

    <title>{$Title|escape:"html"}</title>

    <link rel="shortcut icon" href="{$_subdomain}/favicon.ico" type="image/x-icon" />
        
    {* Included CSS Files *}
    <link rel="stylesheet" href="{$_subdomain}/css/Base/wallboard_style.css">

        
    {* Included JS Files *}
    <script src="{$_subdomain}/js/jquery-1.8.2.min.js"></script>
    <script src="{$_subdomain}/js/jquery.fittext.js"></script>   
                   
    {block name=scripts}
    {/block}

</head>
<body>
    
{block name=body}
{/block}

</body>
</html>