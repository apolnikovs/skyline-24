<select name="County" class="countySelect text" >
    <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
    {foreach $counties as $county}
    <option value="{$county.CountyID}" >{$county.Name|escape:'html'}</option>
{/foreach}
</select>