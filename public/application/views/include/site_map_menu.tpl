
{if !isset($def)}
    {$def=1}
    {/if}
<div id="menu" >
    
  
  <!--
  <button id="skillsPostcodeBtn" type="button" onclick="document.location.href='{$_subdomain}/AppointmentDiary/skillsAreaMap';" >
        <span class="label">Skills & Postcode Allocation</span>
  </button> -->
  
  <button type="button" class="first" onclick="document.location.href='{$_subdomain}/AppointmentDiary/';" >
        <span class="label" >
            {if $allocOnly eq 'AllocationOnly'}
                 Allocation Only Defaults
            {else}
                 Engineer & Postcode Allocation
            {/if}    
        </span>
  </button>
  
  
        
        
        {if $def!=0}
   <button type="button" class="first" onclick="document.location.href='{$_subdomain}/AppointmentDiary/skillsSetSP';" >
        <span class="label">Skill Set Admin</span>
  </button>         
            
            
  <button type="button" class="first" onclick="document.location.href='{$_subdomain}/AppointmentDiary/diaryDefaults'" >
        <span class="label">General Defaults</span>
  </button>      
        
  <button type="button" class="first" onclick="document.location.href='{$_subdomain}/Diary/default'" >
        <span class="label">Diary Interface</span>
  </button>
        {else if $def eq 0}
            <button type="button" class="first" onclick="document.location.href='{$_subdomain}/AppointmentDiary/diaryDefaults/spID=1/sPage=10'" >
                <span class="label">General Defaults</span>
            </button>
        {/if}
        {if $def==0||$SuperAdmin}
            <button type="button" class="first" onclick="document.location.href='{$_subdomain}/Diary/allocationOnlyTable'" >
        <span class="label">Appointments</span>
  </button>      
        
  <button type="button" class="first" onclick="dataExport();" >
        <span class="label">Exports</span>
  </button>
            {/if}
           
        
</div>
            <script>
                function dataExport(){

$.post("{$_subdomain}/Diary/showDataExport",{ 'a':1 },
function(data) {

$.colorbox({ html:data, title:"Data Export",escKey: false,
    overlayClose: false,onComplete:function(data){

                  $("#dateFrom" ).datepicker(
              {
              dateFormat: "dd/mm/yy",
                       showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        

          }
          }
          );
            
            
             $('#dateTo').datepicker(  {
              dateFormat: "dd/mm/yy",
                        showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        

          }
          });
     
	
        
        var  oTable3 = $('#export_table').dataTable( {
"sDom": '<"top"><"bottom"><"clear">',
"bPaginate": false,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    false },
                                  false
			 
                           
                            
                            
                            
		] 
});//end datatable

/* Add a click handler to the rows - this could be used as a callback */
	$("#export_table tbody").click(function(event) {
		$(oTable3.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable3 );
                var aData = oTable3.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
        
        
        
        
          }
      });



})


}

  /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}

                </script>