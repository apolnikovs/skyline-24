<script type="text/javascript" >
  
$(document).ready(function() {

     //alert('doc ready...');

     $('#wildCard').click(function(event) {
     
        if(!$('#jobSearch').val())
        {
            $('#jobSearch').focus();
            $("[for='jobSearch']").css("display", "none");
        }
        
        insertAtCursor(document.jobSearchForm.jobSearch, "*");
        
        
     });
     
     $("#wildCardForm").hide();
     
     $("#wildCardHelp").colorbox(

                    { 
                            inline:true, 
                            href:"#wildCardForm", 
                            title: 'Wild Card Search',
                            opacity: 0.75,
                            height:290,
                            width:700,
                            overlayClose: false,
                            escKey: false,
                            onOpen:function(){
                                $("#wildCardForm").show("fast");
                            },                            
                            onCleanup:function(){
                                $("#wildCardForm").hide("fast");
                            }

                    }
                );

});
    
</script>    

  <form id="jobSearchForm" name="jobSearchForm" method="get" action="{$_subdomain}/index/jobSearch" >
        
        <fieldset style="padding-right: 40px;padding-top: 18px;">
            
            <legend title="AP7102">Job Search</legend>
        
            <p>
                <input type="text" name="jobSearch" id="jobSearch" 
		    value="{$Job->search_value}" class="text bigTextBox" tabIndex="11" title="Please enter the {$page['Labels']['referral_no']|escape:'html'|trim}, {$page['Labels']['job_no']|escape:'html'|trim}, IMEI No, {$page['Labels']['postcode']|escape:'html'|trim}, {$page['Labels']['surname']|escape:'html'|trim}, {$page['Labels']['policy_no']|escape:'html'|trim} or {$page['Labels']['authorisation_no']|escape:'html'|trim} " />
                <span style="float:right;">
                    <a tabIndex="12" href="javascript:if (validateJobForm()) document.jobSearchForm.submit();" >Search</a>
                </span>
            </p>
        

	    {* Wild Card and Advanced search is hidden from everybody except super admin (sa) *}

	    {if $session_user_id=="sa"}
	    
		<p class="clearfix" style="margin-bottom: 8px;" >
		    {*
		    {html_radios name='jobGroup' options=$Job->search_fields selected=$Job->selected_job_group separator=''}
		    *}

		    <a style="float:left;" tabIndex="16" href="#" id="wildCard"   >Wild Card Search</a>
		    <img src="{$_subdomain}/css/Skins/{$_theme}/images/help.png" alt="Click here to see the help." title="Click here to see the help." width="20" height="20" id="wildCardHelp" style="float:left;padding-left:5px;padding-top:4px;" >

		    <a style="float:right;" tabIndex="16" href="{$_subdomain}/index/jobAdvancedSearch">Advanced Job Search</a>
		</p>
	    
	    {/if}
	    
        
            <p id="jobError" class="formError" style="display: none;" />
        
        </fieldset>
        
    </form>     

            
<form id="wildCardForm" name="wildCardForm">

    <fieldset>
         <legend >Wild Card Search</legend>
         
         <p>
             <br>You can create a wild card search by inserting * before or after the string you are searching for.
         </p>
         <p>
             Example1: Berry* would deliver all occurrences of Berry, Berryman, etc.
         </p>
          <p>
              Example2: *erry* would deliver all occurrences of Berry, Berryman, Merry, Merryman etc.
         </p>
         
    </fieldset>
    
</form>      