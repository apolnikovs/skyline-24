<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Central Service Allocations Page in Job Allocation section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class CentralServiceAllocations extends CustomModel {
    
    private $conn;
    
    private $table                      = "central_service_allocation";
    private $table_network              = "network";
    
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->table 
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID       = isset($args['firstArg'])?$args['firstArg']:'';
        $ManufacturerID  = isset($args['secondArg'])?$args['secondArg']:'';
        
        
        $dbTablesColumns  = array('T1.CentralServiceAllocationID', 'T2.CompanyName', 'T3.Name', 'T4.ClientName', 'T5.ManufacturerName', 'T6.RepairSkillName', 
            'T7.Type', 'T8.ServiceTypeName', 'T9.UnitTypeName', 'T10.Name', 'T1.Status');
        $dbTables         = $this->table." AS T1 
            
                            LEFT JOIN service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID
                            LEFT JOIN country  AS T3 ON T1.CountryID=T3.CountryID
                            LEFT JOIN client  AS T4 ON T1.ClientID =T4.ClientID 
                            LEFT JOIN manufacturer AS T5 ON T1.ManufacturerID=T5.ManufacturerID
                            LEFT JOIN repair_skill AS T6 ON T1.RepairSkillID=T6.RepairSkillID
                            LEFT JOIN job_type  AS T7 ON T1.JobTypeID=T7.JobTypeID
                            LEFT JOIN service_type AS T8 ON T1.ServiceTypeID=T8.ServiceTypeID
                            LEFT JOIN unit_type AS T9 ON T1.UnitTypeID=T9.UnitTypeID
                            LEFT JOIN county AS T10 ON T1.CountyID=T10.CountyID
                            
                            
                            ";
                
        
        if($NetworkID!='')
        {
            if($ManufacturerID)
            {
                $args['where']    = "T1.NetworkID='".$NetworkID."' AND T1.ManufacturerID='".$ManufacturerID."'";
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }   
            else
            {    
                $args['where']    = "T1.NetworkID='".$NetworkID."'";
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }
        }
        else
        {
            $args['where']    = "T1.CentralServiceAllocationID='0'";
            
             $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
        }
       
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['CentralServiceAllocationID']) || !$args['CentralServiceAllocationID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to check for duplicate row.
     *
     * @param array $args It contains list of fields
     
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($CentralServiceAllocationID, $args) {
        
        $prepArgs = array(
            
           ':NetworkID' => $args['NetworkID'], 
           ':CountryID' => $args['CountryID'], 
           ':ManufacturerID' => $args['ManufacturerID'], 
           ':RepairSkillID' => $args['RepairSkillID'], 
           ':JobTypeID' => $args['JobTypeID'], 
           ':ServiceTypeID' => $args['ServiceTypeID'], 
           ':UnitTypeID' => $args['UnitTypeID'], 
           ':CountyID' => $args['CountyID'], 
           ':ClientID' => $args['ClientID'], 
           ':ServiceProviderID' => $args['ServiceProviderID'], 
           ':CentralServiceAllocationID' => $CentralServiceAllocationID
            
        ); 
         
         
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CentralServiceAllocationID FROM '.$this->table.' WHERE NetworkID=:NetworkID AND 
        
        CountryID=:CountryID AND
        ManufacturerID=:ManufacturerID AND
        RepairSkillID=:RepairSkillID AND
        JobTypeID=:JobTypeID AND
        ServiceTypeID=:ServiceTypeID AND
        UnitTypeID=:UnitTypeID AND
        CountyID=:CountyID AND
        ClientID=:ClientID AND
        ServiceProviderID=:ServiceProviderID AND
        CentralServiceAllocationID!=:CentralServiceAllocationID';
       
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute($prepArgs);
        $result = $fetchQuery->fetch();
                
        
        if(is_array($result) && $result['CentralServiceAllocationID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
   
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        if($this->isValid(0, $args))
        {  
                $result = false;
                    
                /* Execute a prepared statement by passing an array of values */
                $sql ='	INSERT INTO '.$this->table.' (	NetworkID, 
							CountryID, 
							ManufacturerID, 
							RepairSkillID, 
							JobTypeID, 
							ServiceTypeID, 
							UnitTypeID, 
							CountyID, 
							ClientID, 
							ServiceProviderID, 
							Status, 
							CreatedDate, 
							ModifiedUserID, 
							ModifiedDate)
			VALUES (			:NetworkID, 
							:CountryID, 
							:ManufacturerID, 
							:RepairSkillID, 
							:JobTypeID, 
							:ServiceTypeID, 
							:UnitTypeID, 
							:CountyID, 
							:ClientID, 
							:ServiceProviderID, 
							:Status, 
							:CreatedDate, 
							:ModifiedUserID, 
							:ModifiedDate)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

/*		$this->controller->log(var_export(array(

                    ':NetworkID' => (isset($args['NetworkID'])) ? $args['NetworkID'] : null,
                    ':CountryID' => (isset($args['CountryID'])) ? $args['CountryID'] : null,
                    ':ManufacturerID' => (isset($args['ManufacturerID'])) ? $args['ManufacturerID'] : null,
                    ':RepairSkillID' => (isset($args['RepairSkillID'])) ? $args['RepairSkillID'] : null,
                    ':JobTypeID' => (isset($args['JobTypeID'])) ? $args['JobTypeID'] : null,
                    ':ServiceTypeID' => (isset($args['ServiceTypeID']) && $args['ServiceTypeID']!="") ? $args['ServiceTypeID'] : null,
                    ':UnitTypeID' => (isset($args['UnitTypeID'])) ? $args['UnitTypeID'] : null,
                    ':CountyID' => (isset($args['CountyID'])) ? $args['CountyID'] : null,
                    ':ClientID' => (isset($args['ClientID'])) ? $args['ClientID'] : null,
                    ':ServiceProviderID' => (isset($args['ServiceProviderID'])) ? $args['ServiceProviderID'] : null,
                    ':Status' => (isset($args['Status'])) ? $args['Status'] : null,
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => (isset($this->controller->user->UserID)) ? $this->controller->user->UserID : null,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

                    ),true));*/
		
                $result =  $insertQuery->execute(array(

                    ':NetworkID' => empty($args['NetworkID']) ? null : $args['NetworkID'],
                    ':CountryID' => empty($args['CountryID']) ? null : $args['CountryID'],
                    ':ManufacturerID' => empty($args['ManufacturerID']) ? null : $args['ManufacturerID'],
                    ':RepairSkillID' => empty($args['RepairSkillID']) ? null : $args['RepairSkillID'],
                    ':JobTypeID' => empty($args['JobTypeID']) ? null : $args['JobTypeID'],
                    ':ServiceTypeID' => empty($args['ServiceTypeID']) ? null : $args['ServiceTypeID'],
                    ':UnitTypeID' => empty($args['UnitTypeID']) ? null : $args['UnitTypeID'],
                    ':CountyID' => empty($args['CountyID']) ? null : $args['CountyID'],
                    ':ClientID' => empty($args['ClientID']) ? null : $args['ClientID'],
                    ':ServiceProviderID' => empty($args['ServiceProviderID']) ? null : $args['ServiceProviderID'],
                    ':Status' => empty($args['Status']) ? null : $args['Status'],
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => (isset($this->controller->user->UserID)) ? $this->controller->user->UserID : null,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

		));
                

                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_inserted_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @global $this->table_network
     *  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CentralServiceAllocationID, NetworkID, CountryID, ManufacturerID, RepairSkillID, JobTypeID, ServiceTypeID, UnitTypeID, CountyID, ClientID, ServiceProviderID, Status FROM '.$this->table.' WHERE CentralServiceAllocationID=:CentralServiceAllocationID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':CentralServiceAllocationID' => $args['CentralServiceAllocationID']));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['NetworkID'])
        {
            //Getting network name.
            $sql3        = 'SELECT CompanyName FROM '.$this->table_network.' WHERE NetworkID=:NetworkID';
            $fetchQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery3->execute(array(':NetworkID' => $result['NetworkID']));
            $result3     = $fetchQuery3->fetch();
            $result['NetworkName']  = isset($result3['CompanyName'])?$result3['CompanyName']:'';
            
        }
        else
        {
            $result['NetworkName'] = '';
        }
     
        
        return $result;
     }
    
     
     
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        
        if($this->isValid($args['CentralServiceAllocationID'], $args))
        {        
                $EndDate = "0000-00-00 00:00:00";
                $row_data = $this->fetchRow($args);
                if($this->controller->statuses[1]['Code']==$args['Status'])
                {
                    if($row_data['Status']!=$args['Status'])
                    {
                            $EndDate = date("Y-m-d H:i:s");
                    }
                }
            
            
             

                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE '.$this->table.' SET NetworkID=:NetworkID, CountryID=:CountryID, ManufacturerID=:ManufacturerID, RepairSkillID=:RepairSkillID, JobTypeID=:JobTypeID, ServiceTypeID=:ServiceTypeID, UnitTypeID=:UnitTypeID, CountyID=:CountyID, ClientID=:ClientID, ServiceProviderID=:ServiceProviderID, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE CentralServiceAllocationID=:CentralServiceAllocationID';




                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(


                        array(

                                ':NetworkID' => empty($args['NetworkID']) ? null : $args['NetworkID'],
                                ':CountryID' => empty($args['CountryID']) ? null : $args['CountryID'],
                                ':ManufacturerID' => empty($args['ManufacturerID']) ? null : $args['ManufacturerID'],
                                ':RepairSkillID' => empty($args['RepairSkillID']) ? null : $args['RepairSkillID'],
                                ':JobTypeID' => empty($args['JobTypeID']) ? null : $args['JobTypeID'],
                                ':ServiceTypeID' => empty($args['ServiceTypeID']) ? null : $args['ServiceTypeID'],
                                ':UnitTypeID' => empty($args['UnitTypeID']) ? null : $args['UnitTypeID'],
				':CountyID' => empty($args['CountyID']) ? null : $args['CountyID'],
                                ':ClientID' => empty($args['ClientID']) ? null : $args['ClientID'],
                                ':ServiceProviderID' => empty($args['ServiceProviderID']) ? null : $args['ServiceProviderID'],
                                ':Status' => empty($args['Status']) ? null : $args['Status'],
                                ':EndDate' => $EndDate,
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s"),
                                ':CentralServiceAllocationID' => $args['CentralServiceAllocationID']  

                            )

                        );
        
               
               
                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_updated_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
              
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    
}
?>