<?php

/**
 * APIAppointments.class.php
 * 
 * Database access routines for the Appointments API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link 
 * @version    1.08
 * 
 * Changes
 * Date        Version Author                Reason
 * 19/09/2012  1.00    Andrew J. Williams    Initial Version
 * 02/11/2012  1.01    Andrew J. Williams    Issue 115 - RoutedAppointments should also return blank appointment slots
 * 07/11/2012  1.02    Andrew J. Williams    Issue 122 - Servicebase ignore unassigned engineers when passed as NULLs from Skyline
 * 19/11/2012  1.03    Andrew J. Williams    BRS 114 - AM PM Times / AutoChangeTimeSlotLabel
 * 16/01/2013  1.04    Andrew J. Williams    Diary Wiazrd API
 * 01/02/2013  1.05    Andrew J. Williams    Trackerbase VMS Log 157 - Send Network RefNo to API
 * 12/02/2013  1.06    Andrew J. Williams    Issue 213 - Appointments API sending all appointment types to ServiceBase
 * 07/05/2013  1.07    Andrew J. Williams    Trackerbase VMS Log 258 - Appointment Time Window passed via Routed Appointments
 * 28/05/2013  1.08    Brian Etherington     Trackerbase VMS Log 272 - bug 2nd appt time window same as first. fixed.
 * 28/05/2013  1.09    Brian Etherington     Trackerbase VMS Log 271 - remove seconds from apt time window
 ****************************************************************************/

require_once('CustomModel.class.php');
include_once ('SkylineRESTClient.class.php');

class APIAppointments extends CustomModel {
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        $this->debug = false;
        
    }
    
    
    /**
     * Skyline API  ServiceBase.PutAppointmentDetails
     *  
     * When a new appointment is booked using the diary system if the user is
     * a ServiceBase user then the diary appointment routine should call this API
     * function to service base. The API will then update teh appountments table
     * with the SeviceBaseID
     * 
     * @param $aId  - Skyline Appointment ID
     * 
     * @return putNewJobs   Array containing output of the API call
     *                          SBAppointmentID => Servicebase appointment ID or 0 if failed
     *                          SBJobNo => The service base job number
     *                          SLAppointmentID => The skyline appointmet id
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function PutAppointmentDetails($aId) {
        $sql = "
                SELECT 
			IF (ISNULL(j.`ServiceCentreJobNo`),
				nsj.`ServiceProviderJobNo`
			,
				j.`ServiceCentreJobNo`
			) AS `SBJobNo`,
                        a.`AppointmentID` AS SLAppointmentID,
			IF (ISNULL(j.`NetworkRefNo`),
				nsj.`NetworkRefNo`
			,
				j.`NetworkRefNo`
			) AS `NetworkRefNo`,
                        a.`SBAppointID` AS `SBAppointmentID`,
                        a.Notes,
			DATE_FORMAT(a.`AppointmentDate`,'%d/%m/%Y') AS `AppointmentDate`,
                        a.`AppointmentType` AS `AppointmentType`,
                        a.`AppointmentTime` AS `TimeSlot`,
                        IF (ISNULL(a.`AppointmentStartTime2`),
				CONCAT(DATE_FORMAT(`AppointmentStartTime`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime`,'%H:%i'))
			,
				CONCAT(DATE_FORMAT(`AppointmentStartTime`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime`,'%H:%i'),', ',DATE_FORMAT(`AppointmentStartTime2`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime2`,'%H:%i'))
			) AS `appttimewindow`,
                        CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
                        u.`Username` AS `username`,
			u.`Password` AS `password`,
                        a.CustContactTime,
                        a.CustContactType,
                        CASE
                            WHEN c.ContactEmail != '' THEN c.ContactEmail
                            WHEN nsj.CustomerEmail != '' THEN c.ContactEmail ELSE '' 
                        END as CustEmail,
                        if(nsj.NetworkRefNo is null, j.NetworkRefNo,nsj.NetworkRefNo) as NetworkRefNo
                FROM
                        `appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
                                        LEFT JOIN `customer` c ON j.`CustomerID` = c.`CustomerID`
					LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                        LEFT JOIN `diary_allocation` da ON a.`DiaryAllocationID` = da.`DiaryAllocationID`
                                        LEFT JOIN `appointment_allocation_slot` aas ON da.`AppointmentAllocationSlotID` = aas.`AppointmentAllocationSlotID`,
                        `service_provider` sp,
                        `user` u
                WHERE
			a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = u.`ServiceProviderID`
                        AND a.`AppointmentID` = $aId
                ";
        
        $result = $this->Query($this->conn, $sql);
        if(isset($result[0]['CustEmail']) && empty($result[0]['CustEmail']))
            unset($result[0]['CustEmail']);
        if ($this->debug) $this->controller->log("APIAppointments::PutAppointmentDetails - Query Result \n".var_export($result,true),'appointments_api_');
        
        /*
         * Create and Call REST Client
         */
        
        $restClient = new SkylineRESTClient($this->controller);
        $output = $restClient->sbDiaryPutAppointmentDetails($result[0]);
        
        if ($this->debug) $this->controller->log('APIAppointments::PutAppointmentDetails - REST Call response \n'.var_export($output,true),'appointments_api_');
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response'] === false ) {
            // Server failure
            $this->controller->log('REST Server Error: '.var_export($output,true),'appointments_api_');
            
            return(
                   array(
                         'SBAppointmentID' => 0,
                         'Message' => 'REST Server Error'
                        )
                  );
            
        } elseif (isset($output['response']['SBAppointmentID'])) {              /* Successful response */
            $sqlUpdate = "
                          UPDATE 
				`appointment`
			  SET 
				`SBAppointID` = :SBAppointID
			  WHERE
				`AppointmentID` = :AppointmentID
                         ";
            $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $update_params = array( ':AppointmentID' => $aId );
            $update_params[':SBAppointID'] = $output['response']['SBAppointmentID'];
            
            $update->execute( $update_params );
        } else {                                                                /* Failure response */
            $this->controller->log('APIAppointments::PutAppointmentDetails - Failure','appointments_api_');
        }
        
        return($output['response']);
    }
    
    /**
     * Skyline API  ServiceBase.DeleteAppointment
     *  
     * Delete an appointment in service base
     * 
     * @param $aId  - The Skyline ID of the appointment deleted 
     * 
     * @return Asscoiate array with response
     *                      ResponseCode => SC0001 if successfull
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function DeleteAppointment($aId) {
        $sql = "
                SELECT 
			a.`AppointmentID` AS `SLAppointmentID`,
			a.`SBAppointID` AS `SBAppointmentID`,
			IF (ISNULL(j.`ServiceCentreJobNo`),
				nsj.`ServiceProviderJobNo`
			,
				j.`ServiceCentreJobNo`
			) AS `SBJobNo`,
                        CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
                        u.`Username` AS `username`,
			u.`Password` AS `password`						
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
					LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`,
                        `service_provider` sp,
                        `user` u
		WHERE
			a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = u.`ServiceProviderID`		
			AND a.`AppointmentID` = $aId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log("APIAppointments::DeleteAppointment - Query Result \n".var_export($result,true),'appointments_api_');
        
        /*
         * Create and Call REST Client
         */
        
        $restClient = new SkylineRESTClient($this->controller);
        if (isset($result[0])) {                                                /* Ceheck we have a result */
            $output = $restClient->sbDiaryDeleteAppointment($result[0]);

            if ($this->debug) $this->controller->log('APIAppointments::DeleteAppointment - REST Call response \n'.var_export($output,true),'appointments_api_');

            /*
            * Check and process REST output
            */

            if ( $output['response'] === false ) {
                // Server failure
                $this->controller->log('REST Server Error: '.var_export($output,true),'appointments_api_');

                return(
                    array(
                            'SBAppointmentID' => 0,
                            'Message' => 'REST Server Error'
                            )
                    );
            } elseif ( isset($output['response']['ResponseCode'])
                       && ($output['response']['ResponseCode'] == 'SC0001') ) {   /* Successful response */
                if ($this->debug) $this->controller->log('APIAppointments::DeleteAppointment - Success!!','appointments_api_');
            } else {                                                                /* Failure response */
                $this->controller->log('APIAppointments::DeleteAppointment - Failure');
            }
        } else {
            $output['response']['ResponseCode'] = 'SC0003';
            $this->controller->log("APIAppointments::DeleteAppointment - Appointment ID $aId not found",'appointments_api_');
        }
        return($output['response']);
    }
    
    
    /**
     * Skyline API  ServiceBase.RoutedAppointments
     *  
     * When a new appointment is booked using the diary system if the user is
     * a ServiceBase user then the diary appointment routine should call this API
     * function to
     * 
     * @param $d    Date to produce routed list for (in mysql yyyy-mm-dd format)
     *        $scId Service Centre ID
     * 
     * @return      Array containing response from service base 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function RoutedAppointments($d, $scId) {
        if((isset($this->controller->session->SPInfo['DiaryType'])&&$this->controller->session->SPInfo['DiaryType']!="FullViamente")||$this->controller->session->GlobalViamenteStatus){
            $orderby="spe.`ServiceProviderEngineerID`,
                        a.SortOrder";
        }else{
            $orderby="spe.`ServiceProviderEngineerID`,
                        a.`ViamenteStartTime`";
        }
        if ($this->debug) $this->controller->log("APIAppointments::RoutedAppointments - START --------------------",'appointments_api_');
        $sql = "
		SELECT
			a.`SBAppointID` AS `SBAppointmentID`,
			a.`AppointmentID` AS `SLAppointmentID`,
                        a.SortOrder,
			IF (ISNULL(j.`ServiceCentreJobNo`),
				nsj.`ServiceProviderJobNo`
			,
				j.`ServiceCentreJobNo`
			) AS `SBJobNo`, 
			DATE_FORMAT(a.`AppointmentDate`, '%d/%m/%Y') AS `AppointmentDate`,
			a.`AppointmentTime` AS `TimeSlot`,
                        IF (ISNULL(a.`AppointmentStartTime2`),
				CONCAT('(', DATE_FORMAT(`AppointmentStartTime`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime`,'%H:%i'), ')')
			,
				CONCAT('(', DATE_FORMAT(`AppointmentStartTime`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime`,'%H:%i'),') and (',DATE_FORMAT(`AppointmentStartTime2`,'%H:%i'),' - ',DATE_FORMAT(`AppointmentEndTime2`,'%H:%i'), ')')
			) AS `appttimewindow`,
                        TIME_FORMAT(SEC_TO_TIME(a.ViamenteTravelTime),'%H:%i') as TravelTime,
                        TIME_FORMAT(SEC_TO_TIME(a.Duration*60),'%H:%i') as Duration,
                        CASE
                            WHEN a.PreVisitContactRequired != 'N'
                            THEN a.PreVisitContactInfo
                            ELSE 'N/A'
                        END as PreVisit,
			IF (ISNULL(spe.`ServiceBaseUserCode`),
                                ''
                        ,
                                spe.`ServiceBaseUserCode`
                        ) AS `EngineerCode`,
			IF (ISNULL(spe.`EngineerLastName`),
                                ''
                        ,
                                spe.`EngineerLastName`
                        ) AS `EngineerName`,
			DATE_FORMAT(a.`ViamenteStartTime`, '%H:%i') AS `AppointmentTime`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
                        u.`Username` AS `username`,
			u.`Password` AS `password`,
			sp.`AutoChangeTimeSlotLabel`,
                        a.CustContactTime,
                        a.CustContactType,
                        a.Notes
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
			                LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
        			        LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID` = spe.`ServiceProviderEngineerID`,
			`service_provider` sp,
                        `user` u
		WHERE
			a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = u.`ServiceProviderID`
			AND a.`ServiceProviderID` = $scId
                        AND a.`AppointmentDate` = '$d'
                ORDER BY 
                        $orderby
               ";
               /* !!! IMPORTANT !!! - It is critical that the ORDER BY clause is not changed. See explanation below */
        
        $result = $this->Query($this->conn, $sql);
        
        if (count($result) == 0) {                                              /* No matches for service provider and date */
            $this->controller->log("APIAppointments::RoutedAppointments - No Appointment for that service provider ($scId) and appointment ($d)",'appointments_api_');
            
            return(
                   array(
                         'ResponseCode' => '300',
                         'Message' => 'No Appointment for that service provider and appointment'
                        )
                  );
        }
        
        $login = array(                                                         /* All login deatils for service centre should be the same so store them */
                       'Site' => $result[0]['Site'],                            /* based on the first result */
                       'username' => $result[0]['username'],
                       'password' => $result[0]['password']
                      );
        
        $AutoChangeTimeSlotLabel = $result[0]['AutoChangeTimeSlotLabel'];       /* Is the service provider set up for auto chaneg time slot option */
        
        /*
         * We need to include a sort order in the output to ServiceBase
         * 
         * To do this we add a column incrementing for each record resetting to
         * 0 every time we move on to a new engineer.
         * 
         * This is possibloe because the query ran above is ordered by engineer
         * and then time. It is criticical this order is not changed.
         */

        $visit = 0;
        $spe_code_old = "";                                                     /* First item so no old */
        for ($n = 0; $n < count($result); $n++ ) {
            $spe_code = $result[$n]['EngineerCode'];                           /* Current engineer code */
            if ($spe_code != $spe_code_old) $visit = 0;                         /* If last engineer viewed is not same as current */
            $visit ++;
           if((isset($this->controller->session->SPInfo['DiaryType'])&&$this->controller->session->SPInfo['DiaryType']!="FullViamente")||$this->controller->session->GlobalViamenteStatus){
             $result[$n]['SortOrder'] = $result[$n]['SortOrder'];
         }else{
            $result[$n]['SortOrder'] = $visit;  
         }/* Sort Order is record number as record sorted */
            $spe_code_old = $spe_code;
            if ($AutoChangeTimeSlotLabel == 'Yes') {                            /* Check if we are changinging the slot time labe (ie from any to AP or PM as appropriate */
                $result[$n]['TimeSlot'] = date('A', strtotime($result[$n]['AppointmentTime']));     /* We are so output AM or PM as appropriate  */
            }
            unset($result[$n]['Site']);                                         /* Don't want to pass site in parameters (we have stored previously */
            unset($result[$n]['username']);                                     /* Don't want to pass username in parameters (we have stored previously */
            unset($result[$n]['password']);                                     /* Don't want to pass password in parameters (we have stored previously */
            unset($result[$n]['AutoChangeTimeSlotLabel']);                      /* Don't want to pass AutoChangeTimeSlotLabel option in parameters (we have stored previously) */
        }
        
        if ($this->debug) $this->controller->log("APIAppointments::RoutedAppointments - Query Result /n".var_export($result,true),'appointments_api_');
        
        /*
         * Create and Call REST Client
         */
        
        $result['Site'] = $login['Site'];                                       /* Pass log in details to REST parameters */
        $result['username'] = $login['username'];
        $result['password'] = $login['password'];
        
        $restClient = new SkylineRESTClient($this->controller);
        $output = $restClient->sbRoutedAppointments($result);
        
        if ($this->debug) $this->controller->log('APIAppointments::RoutedAppointments - REST Call response /n'.var_export($output,true),'appointments_api_');
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response'] === false ) {
            // Server failure
            $this->controller->log('REST Server Error: '.var_export($output,true),'appointments_api_');
            
            return(
                   array(
                         'ResponseCode' => '201',
                         'Message' => 'Cannot establish a secure connection to the Service Base Server. Please make sure your Remote Engineer is turned on and reachable from internet.'
                        )
                  );
            
        } elseif (isset($output['response']['ResponseCode'])&&$output['response']['ResponseCode'] == 'SC0001') {            /* Successful response */
            if ($this->debug) $this->controller->log('APIAppointments::RoutedAppointments - Success!!','appointments_api_');
        } else {
            $this->controller->log('APIAppointments::RoutedAppointments - Failure','appointments_api_');
            $this->controller->log('APIAppointments::RoutedAppointments - Response /n'.var_export($output,true),'appointments_api_');
        }
        
        if ($this->debug) $this->controller->log("APIAppointments::RoutedAppointments - END --------------------",'appointments_api_');
        
        return($output['response']);
    }
    
    
    /*** Text fix to update for one engineer onl;y */
       public function TestRoutedAppointments($date, $eid, $spid) {

        if ($this->debug) $this->controller->log("APIAppointments::TestRoutedAppointments - START --------------------",'appointments_api_');
        $sql = "
		SELECT
			a.`SBAppointID` AS `SBAppointmentID`,
			a.`AppointmentID` AS `SLAppointmentID`,
			IF (ISNULL(j.`ServiceCentreJobNo`),
				nsj.`ServiceProviderJobNo`
			,
				j.`ServiceCentreJobNo`
			) AS `SBJobNo`, 
			DATE_FORMAT(a.`AppointmentDate`, '%d/%m/%Y') AS `AppointmentDate`,
			a.`AppointmentTime` AS `TimeSlot`,
			spe.`ServiceBaseUserCode` AS `EngineerCode`,
			spe.`EngineerLastName` AS `EngineerName`,
			DATE_FORMAT(a.`ViamenteStartTime`, '%H:%i') AS `AppointmentTime`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
                        u.`Username` AS `username`,
			u.`Password` AS `password`
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
			                LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
        			        LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID` = spe.`ServiceProviderEngineerID`,
			`service_provider` sp,
                        `user` u
		WHERE
			a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = u.`ServiceProviderID`
			AND a.`ServiceProviderID` = $spid
                        AND a.`AppointmentDate` = '$date'
                        AND spe.`ServiceProviderEngineerID` = $eid
                ORDER BY 
                        spe.`ServiceProviderEngineerID`,
                        a.`ViamenteStartTime`
               ";
               /* !!! IMPORTANT !!! - It is critical that the ORDER BY clause is not changed. See explanation below */
        
        $this->controller->log("sql= $sql",'appointments_api_');
        
        $result = $this->Query($this->conn, $sql);
        
        //$this->controller->log("sql result = ".var_export($result,true),'appointments_api_');
        
        if (count($result) == 0) {                                              /* No matches for service provider and date */
            $this->controller->log("APIAppointments::RoutedAppointments - No Appointment for that service provider (8) and appointment ($date)",'appointments_api_');
            
            return(
                   array(
                         'ResponseCode' => '301',
                         'Message' => 'No Appointment for that service provider and appointment'
                        )
                  );
        }
        
        $login = array(                                                         /* All login deatils for service centre should be the same so store them */
                       'Site' => $result[0]['Site'],                            /* based on the first result */
                       'username' => $result[0]['username'],
                       'password' => $result[0]['password']
                      );
        
        /*
         * We need to include a sort order in the output to ServiceBase
         * 
         * To do this we add a column incrementing for each record resetting to
         * 0 every time we move on to a new engineer.
         * 
         * This is possibloe because the query ran above is ordered by engineer
         * and then time. It is criticical this order is not changed.
         */

        $visit = 0;
        $spe_code_old = "";                                                     /* First item so no old */
        for ($n = 0; $n < count($result); $n++ ) {
            $spe_code = $result[$n]['EngineerCode'];                            /* Current engineer code */
            if ($spe_code != $spe_code_old) $visit = 0;                         /* If last engineer viewed is not same as current */
            $visit ++;
            $result[$n]['SortOrder'] = $visit;                                  /* Sort Order is record number as record sorted */
            $spe_code_old = $spe_code;
            unset($result[$n]['Site']);                                         /* Don't want to pass site in parameters (we have stored previously */
            unset($result[$n]['username']);                                     /* Don't want to pass username in parameters (we have stored previously */
            unset($result[$n]['password']);                                     /* Don't want to pass password in parameters (we have stored previously */
        }
        
        if ($this->debug) $this->controller->log("APIAppointments::RoutedAppointments - Query Result /n".var_export($result,true),'appointments_api_');
        
        /*
         * Create and Call REST Client
         */
        
        $result['Site'] = $login['Site'];                                       /* Pass log in details to REST parameters */
        $result['username'] = $login['username'];
        $result['password'] = $login['password'];
        
        $restClient = new SkylineRESTClient($this->controller);
        $output = $restClient->sbRoutedAppointments($result);
        
        if ($this->debug) $this->controller->log('APIAppointments::PutAppointmentDetails - REST Call response /n'.var_export($output,true),'appointments_api_');
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response'] === false ) {
            // Server failure
            $this->controller->log('REST Server Error: '.var_export($output,true),'appointments_api_');
            
            return(
                   array(
                         'ResponseCode' => '200',
                         'Message' => 'Cannot establish a secure connection to the Service Base Server. Please make sure your Remote Engineer is turned on and reachable from internet.'
                        )
                  );
            
        } elseif ($output['response']['ResponseCode'] == 'SC0001') {            /* Successful response */
            if ($this->debug) $this->controller->log('APIAppointments::RoutedAppointments - Success!!','appointments_api_');
        } else {
            $this->controller->log('APIAppointments::PutAppointmentDetails - Failure','appointments_api_');
            $this->controller->log('APIAppointments::PutAppointmentDetails - Response /n'.var_export($output['Response'],true),'appointments_api_');
        }
        
        if ($this->debug) $this->controller->log("APIAppointments::RoutedAppointments - END --------------------",'appointments_api_');
        
        return($output['response']);
    }
    
    /**
     * Skyline API  ServiceBase.PutDiaryWizardSelection
     *  
     * Return the selection from the diary Wizard to ServiceBase
     * 
     * @param $scId Service Centre ID
     * 
     * @return      Array containing response from service base 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function PutDiaryWizardSelection($scId) {
        $sqlLogin = "
                SELECT 
                        CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
                        u.`Username` AS `username`,
			u.`Password` AS `password`						
		FROM
			`service_provider` sp,
                        `user` u
		WHERE
			sp.`ServiceProviderID` = u.`ServiceProviderID`
			AND sp.`ServiceProviderID` = $scId
               ";
        
        $login = $this->Query($this->conn, $sqlLogin);                          /* Get login details */
        
        $result = array();                                                      /* Array to build up results in */
        
        /*
         * Skill sets
         */
        
        $sqlSkillsets = "
                SELECT DISTINCT
                        rs.`RepairSkillID` AS `id`,
                        rs.`RepairSkillName` AS `SkillTypeName`					
		FROM
			`service_provider_skillset` spss,
                        `skillset` ss,
                        `repair_skill` rs
		WHERE
			ss.`SkillsetID` = spss.`SkillsetID`
			AND ss.`RepairSkillID` = rs.`RepairSkillID`
			AND spss.`ServiceProviderID` = $scId
                        ";
        
        $resultSkillsets = $this->Query($this->conn, $sqlSkillsets);            /* Get the skill sets */
        
        
        $result['SkillTypes'] = json_encode($resultSkillsets);

        /*
         * Appointment Types
         */
        
        $sqlAppTypes = "
                SELECT DISTINCT
                        `at`.`Type` AS `AppointmentType`				
		FROM
			`appointment_type` `at`,
			`service_provider_skillset` spss,
                        `skillset` ss
		WHERE
			ss.`SkillsetID` = spss.`SkillsetID`
			AND ss.`AppointmentTypeID` = `at`.`AppointmentTypeID`
                        AND spss.`Status` = 'Active'
			AND spss.`ServiceProviderID` = $scId
                        ";
        
        $resultAppTypes = $this->Query($this->conn, $sqlAppTypes);              /* Get the appointment types */
        
        
        $result['AppointmentTypes'] = json_encode($resultAppTypes);
        
        /*
         * REST call
         */
        
        $result['Site'] = $login[0]['Site'];                                    /* Pass log in details to REST parameters */
        $result['username'] = $login[0]['username'];
        $result['password'] = $login[0]['password'];
        
        $restClient = new SkylineRESTClient($this->controller);
        $output = $restClient->sbPutDiaryWizardSelection($result);
        
        if ($this->debug) $this->controller->log('APIAppointments::PutDiaryWizardSelection - REST Call response /n'.var_export($output,true),'appointments_api_');
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response'] === false ) {
            // Server failure
            $this->controller->log('REST Server Error: '.var_export($output,true),'appointments_api_');
            
            return(
                   array(
                         'ResponseCode' => '200',
                         'Message' => 'Cannot establish a secure connection to the Service Base Server. Please make sure your Remote Engineer is turned on and reachable from internet.'
                        )
                  );
            
        } elseif ($output['response']['ResponseCode'] == 'SC0001') {            /* Successful response */
            if ($this->debug) $this->controller->log('APIAppointments::RoutedAppointments - Success!!','appointments_api_');
        } else {
            $this->controller->log('APIAppointments::PutDiaryWizardSelection - Failure','appointments_api_');
            $this->controller->log('APIAppointments::PutDiaryWizardSelection - Response /n'.var_export($output['Response'],true),'appointments_api_');
        }
        
        if ($this->debug) $this->controller->log("APIAppointments::PutDiaryWizardSelection - END --------------------",'appointments_api_');
        
        return($output['response']);
    }
}

?>