<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Service Providers Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.25
 * 
 *
 * Date        Version Author                Reason
 * 16/05/2012  1.00    Nageswara Rao Kanteti Initial Version
 * 25/06/2012  1.01    Andrew J. Williams    Added updateByID
 * 27/06/2012  1.02    Nageswara Rao Kanteti New design changes have been done
 * 06/07/2012  1.03    Nageswara Rao Kanteti Job allocation pages
 * 10/07/2012  1.04    Andrew J. Williams    Skyline API Bug Fixes
 * 11/07/2012  1.05    Andrew J. Williams    Skyline API Alpha Release 1
 * 03/08/2012  1.06    Andrew J. Williams    Skyline Samsung API
 * 13/08/2012  1.07    Nageswara Rao Kanteti Service provider selection on job booking process
 * 17/09/2012  1.08    Andrew J. Williams    Samsung API Calling Issues
 * 02/10/2012  1.09    Nageswara Rao Kanteti Job bookings changes, bug fixes and unallocation jobs admin screen done
 * 03/10/2012  1.10    Nageswara Rao Kanteti Fixed service centre when postcode doesn't contain space, cvommented crm email sending
 * 11/10/2012  1.11    Nageswara Rao Kanteti Sending job to RMA after booking is done
 * 12/10/2012  1.12    Vykintas Rutkunas     Job CSV import changes
 * 30/10/2012  1.13    Nageswara Rao Kanteti Diary default changes have been done
 * 31/10/2012  1.14    Andrew J. Williams    PutJobDetails should not import appointments when Service Centre uses online diary
 * 06/11/2012  1.15    Nageswara Rao Kanteti Service Centre's client contacts have been done except on job update page
 * 13/11/2012  1.16    Andrew J. Williams    Added getPlatform
 * 23/11/2012  1.17    Andrew J. Williams    Added getServiceProviderCapacity
 * 27/11/2012  1.18    Andrew J. Williams    Added getServiceCentreCompanyName
 * 21/01/2013  1.19    Brian Etherington     Added primary key as 'id' field in data structure returned by create and update methods
 * 23/01/2013  1.20    Brian Etherington     Added GetServiceProviderManufacturers and UpdateServiceProviderManufacturers methods
 * 28/02/2013  1.21    Andris Polnikovs      Added DiaryType and Grid system defaults in service provider form
 * 19/03/2013  1.22    Andrew J. Williams    Added getServiceCentreDiaryType
 * 13/05/2013  1.23    Andris Polnikovs      Added AutoReorderStock default
 * 19/06/2013  1.24    Andris Polnikovs      Added documentColor default
 * 24/06/2013  1.25    Andris Polnikovs      Added ChangeSupplieronJobPartOrder default
 ******************************************************************************/

class ServiceProviders extends CustomModel {
    
    private $conn;
    private $dbColumns =    ['ServiceProviderID', 'CompanyName', "Acronym", "SendASCReport", "OnlineDiary", "EmailServiceInstruction", 'Platform', 'ServiceBaseVersion', 'IPAddress', 'Status'];
    private $dbAllColumns = ['ServiceProviderID', 'CompanyName', "Acronym", "SendASCReport", "OnlineDiary", "EmailServiceInstruction", 'Platform', 'ServiceBaseVersion', 'IPAddress', 'Status', 'ServiceProviderID AS Assigned'];
    
    private $table = "service_provider";
    private $table_network = "network";
    private $table_network_service_provider = "network_service_provider";
    private $table_network_client = "network_client";
    private $table_client_branch = "client_branch";
    
    private $tbl;                                                               /* Used by TableFactory */
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       
        
        $this->tbl = TableFactory::ServiceProvider();
    }
    
   
    
    /**
    * Description
    * 
    * This method is for fetching data from database
    * 
    * @param array $args Its an associative array contains where clause, limit and order etc.
    * @global $this->conn
    * @global $this->table
    * @global $this->table_network_service_provider
    * @global $this->dbColumns
    * @global $this->dbAllColumns
    * 
    * @return array 
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */  
    
    public function fetch($args) {
        
        $NetworkID = isset($args["firstArg"]) ? $args["firstArg"] : false;
        $showAssigned = isset($args["secondArg"]) ? $args["secondArg"] : false;
        $limitedFieldsFlag = isset($args["thirdArg"]) ? $args["thirdArg"] : false;
        $postcode = isset($args["fourthArg"]) ? $args["fourthArg"] : false;
        
        if ($NetworkID) {
	    
            if ($showAssigned) {
                
                $dbTables = $this->table . " AS T1 LEFT JOIN " . $this->table_network_service_provider . " AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID";
                
		//For unallocated jobs page.
                if ($limitedFieldsFlag == "2") {
                    
                    if ($postcode) {
                        $dbTables = $this->table . " AS T1 
						    LEFT JOIN " . $this->table_network_service_provider . " AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
						    LEFT JOIN postcode_allocation AS T3 ON T1.ServiceProviderID=T3.ServiceProviderID 
						    LEFT JOIN postcode_allocation_data AS T4 ON T3.PostcodeAllocationID=T4.PostcodeAllocationID AND T4.PostcodeArea='" . $postcode . "'";
                        
                        $dbTablesColumns2 = [
			    "T1.ServiceProviderID", 
			    "T1.CompanyName", 
			    "T1.TownCity", 
			    "T1.ContactPhone", 
			    "T4.PostcodeArea"
			];
			
                        $args["where"] = "T2.NetworkID = '" . $NetworkID . "' AND T2.Status='Active' AND T1.Status='Active' GROUP BY T1.ServiceProviderID";
                        
                    } else {    
			
                        $dbTablesColumns2 = [
			    "T1.ServiceProviderID", 
			    "T1.CompanyName", 
			    "T1.TownCity", 
			    "T1.ContactPhone", 
			    '""'
			];
			
                        $args["where"] = "T2.NetworkID='" . $NetworkID . "' AND T2.Status='Active' AND T1.Status='Active'";
			
                    }
                    
                } else if ($limitedFieldsFlag) {
		    
                    $dbTablesColumns2 = [
			"T1.ServiceProviderID", 
			"T1.CompanyName", 
			"T1.TownCity"
		    ];
                    $args["where"] = "T2.NetworkID='" . $NetworkID . "' AND T2.Status='Active' AND T1.Status='Active'";
		    
                } else {    
		    
                    $dbTablesColumns2 = [
			"T1.ServiceProviderID", 
			"T1.CompanyName", 
			"T1.Acronym", 
			"T1.SendASCReport", 
			"T1.OnlineDiary", 
			"T1.EmailServiceInstruction", 
			"T1.Platform", 
			"T1.ServiceBaseVersion", 
			"T1.IPAddress", 
			"T1.Status", "T1.ServiceProviderID AS Assigned"
		    ];
                    $args["where"] = "T2.NetworkID='" . $NetworkID . "' AND T2.Status='" . $this->controller->statuses[0]['Code'] . "'";
		    
                }
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns2, $args);
		
            } else {   
		
                $output = $this->ServeDataTables($this->conn, $this->table, $this->dbAllColumns, $args);
		
            }
	    
        } else {
            
	    $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
	     
        }
        
        return  $output;
        
    }
    
    
    
    /**
    * Description
    * 
    * This method calls update method if the $args contains primary key.
    * 
    * @param array $args Its an associative array contains all elements of submitted form.

    * @return array It contains status and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */   
    
    public function processData($args) {
         
	if((!isset($args["ServiceProviderID"]) || $args["ServiceProviderID"] == "") && $args['ServiceProviderID'] !== 0 && $args['ServiceProviderID'] !== "0") {
	    return $this->create($args);
	} else {
	    return $this->update($args);
	}
	
    }
    
    
      /**
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param interger $CompanyName  
     * @param interger $ServiceProviderID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($CompanyName, $ServiceProviderID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderID FROM '.$this->table.' WHERE CompanyName=:CompanyName AND ServiceProviderID!=:ServiceProviderID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':CompanyName' => $CompanyName, ':ServiceProviderID' => $ServiceProviderID));
        $result = $fetchQuery->fetch();
        
       // $this->controller->log(var_export($result, true));
        
        if(is_array($result) && $result['ServiceProviderID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to assign network to selected service providers.
     *
     * @param int    $NetworkID
     * @param array  $assignedSPs
     * @param string $sltSPs    
     
     * @global $this->table_network_service_provider
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateNetworkServiceProviders($NetworkID, $assignedSPs, $sltSPs){
        
             $result = false;
        
            
            //Updating details into $this->table_network_service_provider
            $sql2 = 'UPDATE '.$this->table_network_service_provider.' SET EndDate=:EndDate, Status=:Status, ServiceProviderID=:ServiceProviderID, ModifiedDate=:ModifiedDate WHERE NetworkID=:NetworkID AND ServiceProviderID=:ServiceProviderID';

            /* Execute a prepared statement by passing an array of values */    
            $updateQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            //Inserting details into $this->table_network_service_provider
            /* Execute a prepared statement by passing an array of values */
            $sql3 = 'INSERT INTO '.$this->table_network_service_provider.' (NetworkID, ServiceProviderID, EndDate, CreatedDate, Status, ModifiedUserID, ModifiedDate)
            VALUES(:NetworkID, :ServiceProviderID, :EndDate, :CreatedDate, :Status, :ModifiedUserID, :ModifiedDate)';

            $insertQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


           
            $sltSPsList = explode(",", $sltSPs);
            
           // $this->controller->log(var_export($assignedSPs, true));
           // $this->controller->log(var_export($sltSPsList, true));
            
            if(is_array($sltSPsList))
            {
                    if(!is_array($assignedSPs))
                    {   
                        $assignedSPs = array();
                    }
                
                    foreach ( $sltSPsList as $amKey=>$amValue)
                    {

                        if($amValue)
                        {    
                                $existsResult = $this->getNetworkServiceProvider($amValue, $NetworkID);



                                $EndDate = date("Y-m-d H:i:s");
                                $args['Status'] = $this->controller->statuses[1]['Code'];

                                //If the assigned check box is checked then we are assigning active as status and end date to null.
                                 
                                if(in_array($amValue, $assignedSPs))
                                {
                                    $EndDate = "0000-00-00 00:00:00";
                                    $args['Status'] = $this->controller->statuses[0]['Code'];
                                }
                                        
                                $result = true;
                                if($existsResult)
                                {
                                    $result = $updateQuery2->execute(array(

                                    ':NetworkID' => $NetworkID, 
                                    ':ServiceProviderID' => $amValue,    
                                    ':EndDate' => $EndDate,
                                    ':Status' => $args['Status'],
                                    ':ModifiedUserID' => $this->controller->user->UserID,
                                    ':ModifiedDate' => date("Y-m-d H:i:s")

                                    ));
                                }
                                else if($args['Status']!=$this->controller->statuses[1]['Code'])//If the relation is not exists in database and assigned checkbox is ticked then we are inserting relation into database.
                                {

                                        $result = $insertQuery3->execute(array(

                                        ':NetworkID' => $NetworkID, 
                                        ':ServiceProviderID' => $amValue,    
                                        ':EndDate' => $EndDate,    
                                        ':CreatedDate' => date("Y-m-d H:i:s"),
                                        ':Status' => $args['Status'],
                                        ':ModifiedUserID' => $this->controller->user->UserID,
                                        ':ModifiedDate' => date("Y-m-d H:i:s")

                                        ));

                                }
                            
                        }   

                    }
            
            }
        
        return $result;
    
    }
    
    
    
    /*
    * Description
    * 
    * This method is used for to insert/update row into network_service_provider table.
    *
    * @param interger $ServiceProviderID  
    * @param array $args.
    * @global $this->table_network_service_provider
    * 
    * @return boolean.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function processNetworkServiceProvider($ServiceProviderID, $args) {
        
         
         $isExists = $this->getNetworkServiceProvider($ServiceProviderID, $args['NetworkID']);
         
         
         if(isset($args['WalkinJobsUpload']) && $args['WalkinJobsUpload'])
         {
                $args['WalkinJobsUpload'] = 1;
         }
         else
         {
                $args['WalkinJobsUpload'] = 0;
         }
         
         if(isset($isExists['NetworkServiceProviderID']) && $isExists['NetworkServiceProviderID'])//Updating details...
         {
             
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table_network_service_provider.' SET ServiceProviderID=:ServiceProviderID, NetworkID=:NetworkID, AccountNo=:AccountNo, SageSalesAccountNo=:SageSalesAccountNo, SagePurchaseAccountNo=:SagePurchaseAccountNo, TrackingUsername=:TrackingUsername, TrackingPassword=:TrackingPassword, WarrantyUsername=:WarrantyUsername,
            WarrantyPassword=:WarrantyPassword, WarrantyAccountNumber=:WarrantyAccountNumber, Area=:Area, WalkinJobsUpload=:WalkinJobsUpload, CompanyCode=:CompanyCode, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE ServiceProviderID=:ServiceProviderID AND NetworkID=:NetworkID';


            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $updateQuery->execute(array(

                ':ServiceProviderID' => $ServiceProviderID, 
                ':NetworkID' => $args['NetworkID'],
                ':AccountNo' => $args['AccountNo'],
                ':SageSalesAccountNo' => $args['SageSalesAccountNo'],
                ':SagePurchaseAccountNo' => $args['SagePurchaseAccountNo'],
                ':TrackingUsername' => $args['TrackingUsername'],
                ':TrackingPassword' => $args['TrackingPassword'],
                ':WarrantyUsername' => $args['WarrantyUsername'],
                ':WarrantyPassword' => $args['WarrantyPassword'],
                ':WarrantyAccountNumber' => $args['WarrantyAccountNumber'],
                ':Area' => $args['Area'],
                ':WalkinJobsUpload' => $args['WalkinJobsUpload'],
                ':CompanyCode' => $args['CompanyCode'],
                ':Status' => $this->controller->statuses[0]['Code'],
                ':EndDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")

                ));
             
             
             
         }
         else//Inserting details....
         {    
         
            /* Execute a prepared statement by passing an array of values */
            $sql = 'INSERT INTO '.$this->table_network_service_provider.' (ServiceProviderID, NetworkID, AccountNo, SageSalesAccountNo, SagePurchaseAccountNo, TrackingUsername, TrackingPassword, WarrantyUsername,
            WarrantyPassword, WarrantyAccountNumber, Area, WalkinJobsUpload, CompanyCode, Status, CreatedDate, ModifiedUserID, ModifiedDate) VALUES (:ServiceProviderID, :NetworkID, :AccountNo, :SageSalesAccountNo, :SagePurchaseAccountNo, :TrackingUsername, :TrackingPassword, :WarrantyUsername,
            :WarrantyPassword, :WarrantyAccountNumber, :Area, :WalkinJobsUpload, :CompanyCode, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';


           

            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $insertQuery->execute(array(

                ':ServiceProviderID' => $ServiceProviderID, 
                ':NetworkID' => $args['NetworkID'],
                ':AccountNo' => $args['AccountNo'],
                ':SageSalesAccountNo' => $args['SageSalesAccountNo'],
                ':SagePurchaseAccountNo' => $args['SagePurchaseAccountNo'],
                ':TrackingUsername' => $args['TrackingUsername'],
                ':TrackingPassword' => $args['TrackingPassword'],
                ':WarrantyUsername' => $args['WarrantyUsername'],
                ':WarrantyPassword' => $args['WarrantyPassword'],
                ':WarrantyAccountNumber' => $args['WarrantyAccountNumber'],
                ':Area' => $args['Area'],
                ':WalkinJobsUpload' => $args['WalkinJobsUpload'],
                ':CompanyCode' => $args['CompanyCode'],
                ':Status' => $this->controller->statuses[0]['Code'],
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")

                ));
         }
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to In-activate row of Network and ServiceProvider in table_network_service_provider table.
     *
     * @param interger $ServiceProviderID
     * @param interger $NetworkID  
     * 
     * @global $this->table_network_service_provider
     * 
     * @return array.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function deleteNetworkServiceProvider($ServiceProviderID, $NetworkID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'UPDATE '.$this->table_network_service_provider.' SET Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE NetworkID=:NetworkID AND ServiceProviderID=:ServiceProviderID';
        $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result = $deleteQuery->execute(
                array(
                    ':NetworkID' => $NetworkID, 
                    ':ServiceProviderID' => $ServiceProviderID,
                    ':Status' => $this->controller->statuses[1]['Code'],
                    ':EndDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")
                ));
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to get service provider's network details from table_network_service_provider table.
     *
     * @param interger $ServiceProviderID  
     * @param interger $NetworkID  
     * 
     * @global $this->table_network_service_provider
     * 
     * @return array.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getNetworkServiceProvider($ServiceProviderID, $NetworkID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkServiceProviderID, NetworkID, ServiceProviderID, AccountNo, SageSalesAccountNo, SagePurchaseAccountNo, TrackingUsername, TrackingPassword, WarrantyUsername, WarrantyPassword, WarrantyAccountNumber, Area, WalkinJobsUpload, CompanyCode, Status  FROM '.$this->table_network_service_provider.' WHERE ServiceProviderID=:ServiceProviderID AND NetworkID=:NetworkID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ServiceProviderID' => $ServiceProviderID));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get service providers which have jobs.
     *
     * @param interger $NetworkID  default NULL
     * 
     * @global $this->table_network_service_provider
     * 
     * @return array.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getServiceProvidersHaveJobs($User) {
        
         /* Execute a prepared statement by passing an array of values */
         /* Insert conditions for getting ServiceProvider List according to User type
          * if ClientID, fetch all Service Provider for particular ClientID and NetworkID
          * if ServiceProvider then only particular ServiceProviders details
          */
        if($User->BranchID){ 
            
            $sql = 'SELECT 
                    T2.ServiceProviderID, 
                    T2.CompanyName, 
                    T2.Acronym
                FROM 
                    '.$this->table_network_service_provider.' AS T1 
                LEFT JOIN 
                    '.$this->table_network_client.' AS T4 ON T1.NetworkID=T4.NetworkID 
                LEFT JOIN 
                    '.$this->table_client_branch.' AS CB ON (T4.ClientID=CB.ClientID AND T4.NetworkID=CB.NetworkID)
                LEFT JOIN 
                    '.$this->table.' AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
                LEFT JOIN 
                    job AS T3 ON T1.NetworkID=T3.NetworkID AND T1.ServiceProviderID=T3.ServiceProviderID
                WHERE 
                    T1.NetworkID=:NetworkID 
                AND 
                    T1.Status=:Status 
                AND 
                    T2.Status=:Status 
                AND 
                    T4.ClientID=:ClientID 
                AND 
                    T4.Status=:Status 
                AND 
                    CB.BranchID=:BranchID 
                AND 
                    CB.Status=:Status 
                AND 
                    T3.JobID IS NOT NULL 
                AND 
                    T3.ClosedDate IS NULL 
                GROUP BY 
                    T2.ServiceProviderID 
                ORDER BY 
                    T2.CompanyName';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $User->NetworkID, ':Status' => 'Active',':ClientID'=>$User->ClientID,':BranchID'=>$User->BranchID));
        } 
        else if($User->ClientID)
         {
            $sql = 'SELECT T2.ServiceProviderID, T2.CompanyName, T2.Acronym  
                FROM '.$this->table_network_service_provider.' AS T1 
                    
                    LEFT JOIN '.$this->table_network_client.' AS T4 ON T1.NetworkID=T4.NetworkID 
                    LEFT JOIN service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
                    LEFT JOIN job AS T3 ON T1.NetworkID=T3.NetworkID AND T1.ServiceProviderID=T3.ServiceProviderID
                    WHERE T1.NetworkID=:NetworkID AND T1.Status=:Status AND T2.Status=:Status AND T4.ClientID=:ClientID AND T4.Status=:Status AND T3.JobID IS NOT NULL AND T3.ClosedDate IS NULL GROUP BY T2.ServiceProviderID ORDER BY T2.CompanyName';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $User->NetworkID, ':Status' => 'Active',':ClientID'=>$User->ClientID));
         }
         else  if($User->NetworkID)
         {
            $sql = 'SELECT T2.ServiceProviderID, T2.CompanyName, T2.Acronym  
                FROM '.$this->table_network_service_provider.' AS T1 
                    
                    LEFT JOIN service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
                    LEFT JOIN job AS T3 ON T1.NetworkID=T3.NetworkID AND T1.ServiceProviderID=T3.ServiceProviderID
                    WHERE T1.NetworkID=:NetworkID AND T1.Status=:Status AND T2.Status=:Status AND T3.JobID IS NOT NULL AND T3.ClosedDate IS NULL GROUP BY T2.ServiceProviderID ORDER BY T2.CompanyName';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $User->NetworkID, ':Status' => 'Active'));
         }
         else if($User->ServiceProviderID)
         {
            $sql = 'SELECT T2.ServiceProviderID, T2.CompanyName, T2.Acronym  
                
                    FROM service_provider AS T2                     
                    LEFT JOIN job AS T3 ON T2.ServiceProviderID=T3.ServiceProviderID
                    WHERE T2.Status=:Status AND T2.ServiceProviderID=:ServiceProviderID AND T3.JobID IS NOT NULL AND T3.ClosedDate IS NULL GROUP BY T2.ServiceProviderID ORDER BY T2.CompanyName';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active',':ServiceProviderID'=>$User->ServiceProviderID));
         }
         else
         {
             $sql = 'SELECT T2.ServiceProviderID, T2.CompanyName, T2.Acronym  
                
                    FROM service_provider AS T2                     
                    LEFT JOIN job AS T3 ON T2.ServiceProviderID=T3.ServiceProviderID
                    WHERE T2.Status=:Status AND T3.JobID IS NOT NULL AND T3.ClosedDate IS NULL GROUP BY T2.ServiceProviderID ORDER BY T2.CompanyName';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active'));
         }
        
         $result = $fetchQuery->fetchAll();
         
         return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to get clients related to service provider.
     *
     * @param interger $ServiceProviderID  
     * 
     * @global $this->table_network_service_provider
     * 
     * @return array.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getServiceProviderClients($ServiceProviderID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT  T3.ClientID, T3.ClientName  FROM '.$this->table_network_service_provider.' AS T1 
                                                    LEFT JOIN network_client AS T2 ON  T1.NetworkID=T2.NetworkID AND T1.ServiceProviderID=:ServiceProviderID 
                                                    LEFT JOIN client AS T3 ON  T2.ClientID=T3.ClientID
                                                    
                                                    WHERE T1.Status=:Status AND T2.Status=:Status AND T3.Status=:Status ORDER BY T3.ClientName';
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':Status' => 'Active'));
        $result = $fetchQuery->fetchAll();
        
        return $result;
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to get service provider's client contacts.
     *
     * @param interger $ServiceProviderID  
     * 
     
     * 
     * @return array.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getServiceProviderClientContacts($ServiceProviderID, $ClientID=null) {
        
         
         if($ClientID)
         {    
            /* Execute a prepared statement by passing an array of values */
           $sql = 'SELECT  T2.ClientID, T2.ClientName, T1.ContactPhone, T1.ContactEmail  FROM service_provider_contacts AS T1 
                                                       LEFT JOIN client AS T2 ON T1.ClientID=T2.ClientID 

                                                       WHERE T1.ServiceProviderID=:ServiceProviderID AND T1.ClientID=:ClientID AND  T2.Status=:Status ORDER BY T2.ClientName';

           $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
           $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':ClientID' => $ClientID, ':Status' => 'Active'));
           $result = $fetchQuery->fetchAll();
           
         }
         else
         {
                /* Execute a prepared statement by passing an array of values */
                $sql = 'SELECT  T2.ClientID, T2.ClientName, T1.ContactPhone, T1.ContactEmail  FROM service_provider_contacts AS T1 
                                                            LEFT JOIN client AS T2 ON T1.ClientID=T2.ClientID 

                                                            WHERE T1.ServiceProviderID=:ServiceProviderID AND  T2.Status=:Status ORDER BY T2.ClientName';

                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':Status' => 'Active'));
                $result = $fetchQuery->fetchAll();
         }
        
//        $this->controller->log("SSSSSS");
//        $this->controller->log(var_export(array(':ServiceProviderID' => $ServiceProviderID, ':Status' => 'Active'), true));
//        $this->controller->log(var_export($result, true));
//        
        return $result;
    }
    
    
    
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args  
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {

	$sql = 'INSERT INTO ' . $this->table . ' 
			    (CompanyName, 
			    BuildingNameNumber, 
			    PostalCode, 
			    Street, 
			    LocalArea, 
			    TownCity, 
			    CountyID, 
			    CountryID, 
			    ContactPhone, 
			    ContactPhoneExt, 
			    ContactEmail, 
			    ReplyEmail,
			    Platform, 
			    IPAddress, 
			    Port, 
			    InvoiceToCompanyName, 
			    VATRateID, 
			    VATNumber, 
			    WeekdaysOpenTime, 
			    WeekdaysCloseTime, 
			    SaturdayOpenTime, 
			    SaturdayCloseTime, 
                            SundayOpenTime,
                            SundayCloseTime,
			    ServiceProvided, 
			    SynopsisOfBusiness, 
			    Status, 
			    CreatedDate, 
			    ModifiedUserID, 
			    ModifiedDate, 
			    DefaultTravelTime, 
			    DefaultTravelSpeed, 
			    CourierStatus,
                            OnlineDiary,
			    ServiceManagerForename,
			    ServiceManagerSurname,
			    AdminSupervisorForename,
			    AdminSupervisorSurname,
			    ServiceManagerEmail,
			    AdminSupervisorEmail,
			    SendASCReport,
                            EmailServiceInstruction,
                            DiaryType,
                            Acronym,
                            DiaryAllocationType,
                            SetupUniqueTimeSlotPostcodes,
                            PublicWebsiteAddress,
                            GeneralManagerForename,
                            GeneralManagerSurname,
                            GeneralManagerEmail,
                            ServiceProviderType,
                            AutoReorderStock,
			    SBRASync,
                            CurrencyID,
                            DocumentCustomColorScheme,
                            ChangeSupplieronJobPartOrder
			    
			    ) VALUES (
			    
			    :CompanyName, 
			    :BuildingNameNumber, 
			    :PostalCode, 
			    :Street, 
			    :LocalArea, 
			    :TownCity, 
			    :CountyID, 
			    :CountryID, 
			    :ContactPhone, 
			    :ContactPhoneExt, 
			    :ContactEmail, 
			    :ReplyEmail, 
			    :Platform, 
			    :IPAddress, 
			    :Port, 
			    :InvoiceToCompanyName, 
			    :VATRateID, 
			    :VATNumber,
			    :WeekdaysOpenTime, 
			    :WeekdaysCloseTime, 
			    :SaturdayOpenTime, 
			    :SaturdayCloseTime, 
                            :SundayOpenTime,
                            :SundayCloseTime,
			    :ServiceProvided, 
			    :SynopsisOfBusiness, 
			    :Status, 
			    :CreatedDate, 
			    :ModifiedUserID, 
			    :ModifiedDate, 
			    :DefaultTravelTime, 
			    :DefaultTravelSpeed, 
			    :CourierStatus,
                            :OnlineDiary,
			    :ServiceManagerForename,
			    :ServiceManagerSurname,
			    :AdminSupervisorForename,
			    :AdminSupervisorSurname,
			    :ServiceManagerEmail,
			    :AdminSupervisorEmail,
			    :SendASCReport,
                            :EmailServiceInstruction,
                            :DiaryType,
                            :Acronym,
                            :DiaryAllocationType,
                            :SetupUniqueTimeSlotPostcodes,
                            :PublicWebsiteAddress,
                            :GeneralManagerForename,
                            :GeneralManagerSurname,
                            :GeneralManagerEmail,
                            :ServiceProviderType,
                            :AutoReorderStock,
			    :SBRASync,
                            :CurrencyID,
                            :DocumentCustomColorScheme,
                            :ChangeSupplieronJobPartOrder
                            
			    )';
        
        if($this->isValid($args['CompanyName'], 0)) {
           
            $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            if(!isset($args["DiaryType"]))
            {    
                $args["DiaryType"] = 'FullViamente';
            }
            
            $result = $insertQuery->execute([
                ':CompanyName' => $args['CompanyName'], 
                ':BuildingNameNumber' => $args['BuildingNameNumber'], 
                ':PostalCode' => ($args['PostalCode']=='')? NULL : $args['PostalCode'],
                ':Street' => $args['Street'],
                ':LocalArea' => $args['LocalArea'],
                ':TownCity' => $args['TownCity'],
                ':CountyID' => ($args['CountyID'] == '') ? NULL : $args['CountyID'],
                ':CountryID' => ($args['CountryID'] == '') ? NULL : $args['CountryID'],
                ':ContactPhone' => $args['ContactPhone'],
                ':ContactPhoneExt' => $args['ContactPhoneExt'],
                ':ContactEmail' => $args['ContactEmail'],
                ':ReplyEmail' => $args['ReplyEmail'],
                ':Platform' => $args['Platform'],
                ':IPAddress' => $args['IPAddress'],
                ':Port' => ($args['Port'] == '') ? null: $args['Port'],
                ':InvoiceToCompanyName' => $args['InvoiceToCompanyName'],
                ':VATRateID' => ($args['VATRateID'] == '') ? NULL : $args['VATRateID'],
                ':VATNumber' => $args['VATNumber'],
                ':WeekdaysOpenTime' => ($args['WeekdaysOpenTime'] == '') ? NULL : $args['WeekdaysOpenTime'],
                ':WeekdaysCloseTime' => ($args['WeekdaysCloseTime'] == '') ? NULL : $args['WeekdaysCloseTime'],
                
                ':SaturdayOpenTime' => ($args['SaturdayOpenTime'] == '') ? NULL : $args['SaturdayOpenTime'],
                ':SaturdayCloseTime' => ($args['SaturdayCloseTime']=='') ? NULL : $args['SaturdayCloseTime'],
                
                ':SundayOpenTime' => ($args['SundayOpenTime'] == '') ? NULL : $args['SundayOpenTime'],
                ':SundayCloseTime' => ($args['SundayCloseTime']=='') ? NULL : $args['SundayCloseTime'],
                
                
                ':ServiceProvided' => $args['ServiceProvided'],
                ':SynopsisOfBusiness' => $args['SynopsisOfBusiness'],
                ':Status' => isset($args['Status']) ? $args['Status']: "Active",
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s"),
                ':DefaultTravelTime' => $args['DefaultTravelTime'],
                ':DefaultTravelSpeed' => $args['DefaultTravelSpeed'],
                ':CourierStatus' => $args['CourierStatus'],
                ':OnlineDiary' => $args['OnlineDiary'],
                ":ServiceManagerForename" => $args["ServiceManagerForename"],
		":ServiceManagerSurname" => $args["ServiceManagerSurname"],
		":AdminSupervisorForename" => $args["AdminSupervisorForename"],
		":AdminSupervisorSurname" => $args["AdminSupervisorSurname"],
		":ServiceManagerEmail" => $args["ServiceManagerEmail"],
		":AdminSupervisorEmail" => $args["AdminSupervisorEmail"],
                ":SendASCReport" => isset($args["SendASCReport"]) ? $args["SendASCReport"] : NULL,
                ":EmailServiceInstruction" => isset($args["EmailServiceInstruction"]) ? $args["EmailServiceInstruction"] : NULL,
                ":DiaryType" => $args["DiaryType"],
                ":Acronym" => isset($args["Acronym"]) ? $args["Acronym"] : null,
                ":DiaryAllocationType" => isset($args["DiaryAllocationType"]) ? "GridMapping" : "Postcode",
                ":SetupUniqueTimeSlotPostcodes" => $args['DiaryType'] == "FullViamente" ? "No" : "Yes",
                ":PublicWebsiteAddress" => $args["PublicWebsiteAddress"],
                ":GeneralManagerForename" => $args["GeneralManagerForename"],
                ":GeneralManagerSurname" => $args["GeneralManagerSurname"],
                ":GeneralManagerEmail" => $args["GeneralManagerEmail"],
                ":ServiceProviderType" => $args["ServiceProviderType"],
                ":AutoReorderStock" => isset($args["AutoReorderStock"]) ? "Yes" : "No",
		"SBRASync" => isset($args["SBRASync"]) ? $args["SBRASync"] : null,
		"CurrencyID" => $args["CurrencyID"],
		"DocumentCustomColorScheme" => $args["DocumentCustomColorScheme"],
		"ChangeSupplieronJobPartOrder" => $args["ChangeSupplieronJobPartOrder"],

	    ]);
            
	    $ServiceProviderID = $this->conn->lastInsertId();
            
	    $result2 = false; 
	    
	    if($result && isset($args['NetworkID']) && $args['NetworkID']!='') {    //if the service provider details are inserted 
										    //into database then only we are storing service 
										    //provider and network id in table 
										    //network_service_provider
		$result2 = $this->processNetworkServiceProvider($ServiceProviderID, $args);
	    }
               
	    if($result) {
		return ['status' => 'OK',
			'message' => $this->controller->page['Text']['data_inserted_msg'],
                        'id' => $ServiceProviderID];
	    } else {
		return ['status' => 'ERROR',
			'message' => $this->controller->page['Errors']['data_not_processed']];
	    }
	    
        } else {
            
            return ['status' => 'ERROR',
                    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)];
	    
        }
	
    }
    
    
    
    /*
    * Description
    * 
    * This method is used for to fetch a row from database.
    *
    * @param array $args
    * @global $this->table  
    * @return array It contains row of the given primary key.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchRow($args) {
        
	$result = [];
        
        $sql = "SELECT	ServiceProviderID, 
			CompanyName, 
			Acronym,
			BuildingNameNumber, 
			PostalCode, 
			Street, 
			LocalArea, 
			TownCity, 
			CountyID, 
			CountryID, 
			ContactPhone, 
			ContactPhoneExt, 
                        ContactEmail, 
			ReplyEmail, 
			Platform, 
			IPAddress, 
			Port, 
			InvoiceToCompanyName, 
			VATRateID, 
			VATNumber, 
			TIME_FORMAT(WeekdaysOpenTime, '%H:%i') AS WeekdaysOpenTime, 
			TIME_FORMAT(WeekdaysCloseTime, '%H:%i') AS WeekdaysCloseTime, 
			TIME_FORMAT(SaturdayOpenTime, '%H:%i') AS SaturdayOpenTime, 
                        TIME_FORMAT(SaturdayCloseTime, '%H:%i') AS SaturdayCloseTime, 
                        TIME_FORMAT(SundayOpenTime, '%H:%i') AS SundayOpenTime, 
                        TIME_FORMAT(SundayCloseTime, '%H:%i') AS SundayCloseTime, 
			ServiceProvided, 
			SynopsisOfBusiness, 
			Status, 
			DefaultTravelTime, 
			DefaultTravelSpeed,
                        BookingCapacityLimit,
                        KeepEngInPCArea,
			AutoChangeTimeSlotLabel, 
			CourierStatus, 
			RunViamenteToday, 
			SetupUniqueTimeSlotPostcodes,
                        UnlockingPassword, 
			PasswordProtectNextDayBookings, 
			PasswordProtectNextDayBookingsTime, 
			DiaryAdministratorEmail, 
			ViamenteKey, 
			ViamenteKey2, 
			ViamenteKey2MaxEng, 
			ViamenteKey2MaxWayPoints, 
			AutoSelectDay, 
			AutoDisplayTable, 
			AutoSpecifyEngineerByPostcode, 
                        ViamenteRunType,
                        EmailAppBooking,
                        DiaryShowSlotNumbers,
			ServiceManagerForename,
			ServiceManagerSurname,
			AdminSupervisorForename,
			AdminSupervisorSurname,
			ServiceManagerEmail,
			AdminSupervisorEmail,
			SendASCReport,
                        OnlineDiary,
                        EngineerDefaultStartTime,
                        EngineerDefaultEndTime,
                        NumberOfVehicles,
                        NumberOfWaypoints,
                        SendRouteMapsEmail,
                        Multimaps,
                        EmailServiceInstruction,
                        DiaryType,
                        DiaryAllocationType,
                        PublicWebsiteAddress,
                        GeneralManagerForename,
                        GeneralManagerSurname,
                        GeneralManagerEmail,
                        LockAppWindow,
                        LockAppWindowTime,
                        ServiceProviderType,
                        AutoReorderStock,
			SBRASync,
                        CurrencyID,
                        DocColour1,
                        DocColour2,
                        DocColour3,
                        DocumentCustomColorScheme,
                        ChangeSupplieronJobPartOrder
			
		FROM	" . $this->table . "
		    
		WHERE	ServiceProviderID = :ServiceProviderID
	    ";
	
        $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        
        $fetchQuery->execute([':ServiceProviderID' => $args['ServiceProviderID']]);
        $result = $fetchQuery->fetch();
        
        if(isset($args['NetworkID'])) {
            $networksArray = $this->getNetworkServiceProvider($args['ServiceProviderID'], $args['NetworkID']);
            if(is_array($result) && is_array($networksArray)) {
                unset($networksArray['Status']);
                $result = array_merge($result, $networksArray);
            }  
        }
        
        $result['NetworkID'] = isset($result['NetworkID']) ? $result['NetworkID'] : '';
        $result['NetworkName'] = '';
        
        if($result['NetworkID'] != '') {
            
	    //Getting network name.
            $sql3 = "	SELECT	CompanyName 
			FROM	" . $this->table_network . " 
			WHERE	NetworkID = :NetworkID AND 
				Status = '" . $this->controller->statuses[0]['Code'] . "'";
	    
            $fetchQuery3 = $this->conn->prepare($sql3, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $fetchQuery3->execute([':NetworkID' => $result['NetworkID']]);
            $result3 = $fetchQuery3->fetch();
            $result['NetworkName'] = isset($result3['CompanyName']) ? $result3['CompanyName'] : '';
            
        }
        
	//$this->controller->log("RESULT: " . var_export($result, true));
	
        return $result;
	
    }
    
    
    
     /*
    * Description
    * 
    * This method is used for to check ViamenteKey Exists or Not
    *
    * @param string $ViamenteKey
    * @param int $ExceptServiceProviderID 
    
    * @global $this->table  
    * 
    * @return boolean
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function isViamenteKeyExists($ViamenteKey, $ExceptServiceProviderID) {
        
        
        
          $sql = 'SELECT  ServiceProviderID FROM  ' . $this->table . ' 
		    
		WHERE	ServiceProviderID != :ServiceProviderID AND (ViamenteKey = :ViamenteKey OR ViamenteKey2 = :ViamenteKey2)
	    ';
	
        $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        
        $fetchQuery->execute([':ServiceProviderID' => $ExceptServiceProviderID, ':ViamenteKey' => $ViamenteKey, ':ViamenteKey2' => $ViamenteKey]);
        $result = $fetchQuery->fetch();
        
        if(isset($result['ServiceProviderID']) && $result['ServiceProviderID'])
        {
            return true;
        } 
        else
        {
             return false;
        }
        
    }
    
    
    
    
    
    
    /**
     * getServiceCentre
     * 
     * Returns the details of a service provider from its ID
     * 
     * @param $spId  The ID of the Service Provider
     * 
     * @return array  The record beloning top the service centre.
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function fetchRowById($spId) {
        $sql = "
                SELECT
			*
		FROM
			`service_provider`
		WHERE
			`ServiceProviderID` = $spId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* ServiceProviderID exists so return Service Provider Details */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to udpate a row into database.
    *
    * @param array $args
    * @global $this->table   
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */
    
    public function update($args) {
        
        
	if($this->isValid($args['CompanyName'], $args['ServiceProviderID'])) {     
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status']) {
                if($row_data['Status']!=$args['Status']) {
		    $EndDate = date("Y-m-d H:i:s");
                }
            }
            
            if(!isset($args["DiaryType"]))
            {    
                $args["DiaryType"] = 'FullViamente';
            }
            
	    $sql = 'UPDATE  ' . $this->table . ' 
		
		    SET	    CompanyName = :CompanyName, 
			    Acronym = :Acronym,
			    BuildingNameNumber = :BuildingNameNumber, 
			    PostalCode = :PostalCode, 
			    Street = :Street, 
			    LocalArea = :LocalArea, 
			    TownCity = :TownCity, 
			    CountyID = :CountyID, 
			    CountryID = :CountryID, 
			    ContactPhone = :ContactPhone, 
			    ContactPhoneExt = :ContactPhoneExt, 
			    ContactEmail = :ContactEmail, 
			    ReplyEmail = :ReplyEmail, 
			    Platform = :Platform, 
			    IPAddress = :IPAddress, 
			    Port = :Port, 
			    InvoiceToCompanyName = :InvoiceToCompanyName, 
			    VATRateID = :VATRateID, 
			    VATNumber = :VATNumber, 
			    WeekdaysOpenTime = :WeekdaysOpenTime, 
			    WeekdaysCloseTime = :WeekdaysCloseTime, 
			    SaturdayOpenTime = :SaturdayOpenTime, 
			    SaturdayCloseTime = :SaturdayCloseTime, 
                            SundayOpenTime = :SundayOpenTime, 
			    SundayCloseTime = :SundayCloseTime, 
			    ServiceProvided = :ServiceProvided, 
			    SynopsisOfBusiness = :SynopsisOfBusiness, 
			    Status = :Status, 
			    EndDate = :EndDate, 
			    ModifiedUserID = :ModifiedUserID, 
			    ModifiedDate = :ModifiedDate, 
			    DefaultTravelTime = :DefaultTravelTime, 
			    DefaultTravelSpeed = :DefaultTravelSpeed, 
			   
			   
			    CourierStatus = :CourierStatus, 
                            OnlineDiary = :OnlineDiary, 
                            EmailServiceInstruction = :EmailServiceInstruction,
			    ServiceManagerForename = :ServiceManagerForename,
			    ServiceManagerSurname = :ServiceManagerSurname,
			    AdminSupervisorForename = :AdminSupervisorForename,
			    AdminSupervisorSurname = :AdminSupervisorSurname,
			    ServiceManagerEmail = :ServiceManagerEmail,
			    AdminSupervisorEmail = :AdminSupervisorEmail,
			    SendASCReport = :SendASCReport,
                            DiaryType=:DiaryType,
                            DiaryAllocationType=:DiaryAllocationType,
			    SetupUniqueTimeSlotPostcodes=:SetupUniqueTimeSlotPostcodes,
                            PublicWebsiteAddress=:PublicWebsiteAddress,
                            GeneralManagerForename=:GeneralManagerForename,
                            GeneralManagerSurname=:GeneralManagerSurname,
                            GeneralManagerEmail=:GeneralManagerEmail,
                            ServiceProviderType=:ServiceProviderType,
                            AutoReorderStock=:AutoReorderStock,
			    SBRASync = :SBRASync,
			    CurrencyID = :CurrencyID,
                            DocColour1=:DocColour1,
                            DocColour2=:DocColour2,
                            DocColour3=:DocColour3,
                            DocumentCustomColorScheme=:DocumentCustomColorScheme,
                            ChangeSupplieronJobPartOrder=:ChangeSupplieronJobPartOrder
                                
		    WHERE   ServiceProviderID = :ServiceProviderID';
            
	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    
            $result = $updateQuery->execute([
		':CompanyName' => $args['CompanyName'], 
		":Acronym" => isset($args["Acronym"]) ? $args["Acronym"] : null,
		':BuildingNameNumber' => $args['BuildingNameNumber'], 
		':PostalCode' => ($args['PostalCode']=='')? NULL : $args['PostalCode'],
		':Street' => $args['Street'],
		':LocalArea' => $args['LocalArea'],
		':TownCity' => $args['TownCity'],
		':CountyID' => ($args['CountyID']=='') ? NULL : $args['CountyID'],
		':CountryID' => ($args['CountryID']=='') ? NULL : $args['CountryID'],
		':ContactPhone' => $args['ContactPhone'],
		':ContactPhoneExt' => $args['ContactPhoneExt'],
		':ContactEmail' => $args['ContactEmail'],
		':ReplyEmail' => $args['ReplyEmail'],                          
		':Platform' => $args['Platform'],
		':IPAddress' => $args['IPAddress'],
		':Port' => $args['Port'],
		':InvoiceToCompanyName' => $args['InvoiceToCompanyName'],
		':VATRateID' => ($args['VATRateID']=='') ? NULL : $args['VATRateID'],
		':VATNumber' => $args['VATNumber'],
		':WeekdaysOpenTime' => ($args['WeekdaysOpenTime']=='') ? NULL : $args['WeekdaysOpenTime'],
		':WeekdaysCloseTime' => ($args['WeekdaysCloseTime']=='') ? NULL : $args['WeekdaysCloseTime'],
		
                ':SaturdayOpenTime' => ($args['SaturdayOpenTime']=='') ? NULL : $args['SaturdayOpenTime'],
		':SaturdayCloseTime' => ($args['SaturdayCloseTime']=='') ? NULL : $args['SaturdayCloseTime'],
                
                ':SundayOpenTime' => ($args['SundayOpenTime']=='') ? NULL : $args['SundayOpenTime'],
		':SundayCloseTime' => ($args['SundayCloseTime']=='') ? NULL : $args['SundayCloseTime'],
                
                
                
		':ServiceProvided' => $args['ServiceProvided'],
		':SynopsisOfBusiness' => $args['SynopsisOfBusiness'],
		':Status' => $args['Status'],
		':EndDate' => $EndDate,
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s"),
		':ServiceProviderID' => $args['ServiceProviderID'],
		':DefaultTravelTime' => $args['DefaultTravelTime'],
		':DefaultTravelSpeed' => $args['DefaultTravelSpeed'],
		
		':CourierStatus' => $args['CourierStatus'],
                ':OnlineDiary' => $args['OnlineDiary'],
                ":EmailServiceInstruction" => isset($args["EmailServiceInstruction"])?$args["EmailServiceInstruction"]:NULL,
                ":ServiceManagerForename" => $args["ServiceManagerForename"],
		":ServiceManagerSurname" => $args["ServiceManagerSurname"],
		":AdminSupervisorForename" => $args["AdminSupervisorForename"],
		":AdminSupervisorSurname" => $args["AdminSupervisorSurname"],
		":ServiceManagerEmail" => $args["ServiceManagerEmail"],
		":AdminSupervisorEmail" => $args["AdminSupervisorEmail"],
                ":SendASCReport" => isset($args["SendASCReport"])?$args["SendASCReport"]:NULL,
                ":DiaryType"=>$args["DiaryType"],
                ":DiaryAllocationType" => isset($args["DiaryAllocationType"]) ? "GridMapping" : "Postcode",
                ":SetupUniqueTimeSlotPostcodes"=>$args['DiaryType']=="FullViamente"?"No":"Yes",
                ':PublicWebsiteAddress' => $args['PublicWebsiteAddress'],
                ':GeneralManagerForename' => $args['GeneralManagerForename'],
                ':GeneralManagerSurname' => $args['GeneralManagerSurname'],
                ':GeneralManagerEmail' => $args['GeneralManagerEmail'],
                ':ServiceProviderType' => $args['ServiceProviderType'],
                ':DocColour1' => $args['DocColour1'],
                ':DocColour2' => $args['DocColour2'],
                ':DocColour3' => $args['DocColour3'],
                ':DocumentCustomColorScheme' => $args['DocumentCustomColorScheme'],
                ':AutoReorderStock' => isset($args["AutoReorderStock"])?'Yes':'No',
		":SBRASync" => isset($args["SBRASync"]) ? $args["SBRASync"] : null,
		":CurrencyID" => isset($args["CurrencyID"]) ? $args["CurrencyID"] : null,
		":ChangeSupplieronJobPartOrder" => isset($args["ChangeSupplieronJobPartOrder"]) ? $args["ChangeSupplieronJobPartOrder"] : null
		
	    ]);

	    $sql3 = 'DELETE FROM service_provider_contacts WHERE ServiceProviderID = :ServiceProviderID';
        
	    $deleteQuery = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	    $deleteQuery->execute([':ServiceProviderID' => $args['ServiceProviderID']]);
                    
	    if(isset($args['ClientID']) && $args['ClientID'] != '' && ((isset($args['ClientContactPhone']) && $args['ClientContactPhone']) || (isset($args['ClientContactEmail']) && $args['ClientContactEmail']))) {
		$args['ClientIDs'][] = $args['ClientID'];
		$args['ClientContactPhones'][] = $args['ClientContactPhone'];
		$args['ClientContactEmails'][] = $args['ClientContactEmail'];
	    }    
                    
	    //Inserting client contacts.....
	    if(isset($args['ClientIDs']) && is_array($args['ClientIDs'])) {
		
		$sql4 = '   INSERT INTO service_provider_contacts 
					(ServiceProviderID, 
					ClientID, 
					ContactPhone, 
					ContactEmail) 
					VALUES 
					(:ServiceProviderID, 
					:ClientID, 
					:ContactPhone, 
					:ContactEmail)';
           
		$insertQuery = $this->conn->prepare($sql4, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                        
		for($k=0;$k<count($args['ClientIDs']);$k++) {
		    $insertQuery->execute([
			':ServiceProviderID' => $args['ServiceProviderID'],
			':ClientID' => $args['ClientIDs'][$k],
			':ContactPhone' => $args['ClientContactPhones'][$k],
			':ContactEmail' => $args['ClientContactEmails'][$k]
		    ]);
		     
		}

	    }    
                    
                  
	    $result2 = false; 
	    if($result && isset($args['NetworkID']) && $args['NetworkID']!='') {    //if the service provider details are updated into 
										    //database then only we are updateing/inserting 
										    //service provider and network id in table 
										    //network_service_provider
		$result2 = $this->processNetworkServiceProvider($args['ServiceProviderID'], $args);

	    }

	    if($result) {
		return ['status' => 'OK',
			'message' => $this->controller->page['Text']['data_updated_msg'],
                        'id' => $args['ServiceProviderID']];
	    } else {
		return ['status' => 'ERROR',
			'message' => $this->controller->page['Errors']['data_not_processed']];
	    }
              
        } else {
	    
	    return ['status' => 'ERROR',
                    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)];
        }
	
    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }

    /**
     * updateByID
     *  
     * Update fields passed in argument based upon the SeriveProviderID provided
     * 
     * @param array $args   Associative array of field values for the update.
     *                      The fields of the array must match the fields in the
     *                      database. Additionally the primary key ServiceProviderID
     *                      must also be provided.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected - the number of roes affected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function updateByID($args) {
        $cmd = $this->tbl->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                            /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * getServiceCentreContactNameRole
     *  
     * Get contact name and roles for all the users of a service centre
     * 
     * @param array $spId    Service Provider ID
     * 
     * @return array    (Title => Job Title / Position , Name => Contact Name)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getServiceCentreContactNameRole($sId) {
        $sql = "
                SELECT
			u.`Position` AS `Title`,
			CONCAT_WS(' ',ct.`Title`, u.`ContactFirstName`,u.`ContactLastName`) AS `Name`,
                        CONCAT_WS(' ',u.`ContactWorkPhone`,u.`ContactWorkPhoneExt`) AS WorkPhone,
			u.`ContactMobile`
                FROM
			`user` u LEFT JOIN `customer_title` ct ON u.CustomerTitleID = ct.CustomerTitleID
                WHERE
			u.ServiceProviderID = $sId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* ServiceProviderID exists so return Service Provider Details */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
        /**
     * getServiceCentreSamsungUpload
     *  
     * Return whether the Service Centre can upload jobs to Samsung. 
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return boolean  Can the service center upload the 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getServiceCentreSamsungUpload($sId) {
        $sql = "
                SELECT
			`SamsungDownloadEnabled`
                FROM
			`network_service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['SamsungDownloadEnabled']);                       /* ServiceProviderID exists so return Samsung Upload Permission */
        } else {
            return(0);                                                          /* Not found, so no permission so return null */
        }
    }
    
    /**
     * getServiceCentreSamsungUpload
     *  
     * Return whether the Service Centre's company Name. 
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return string   Company Name of Service Centre 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getServiceCentreCompanyName($sId) {
        $sql = "
                SELECT
			`CompanyName`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['CompanyName']);                                  /* ServiceProviderID exists so return Samsung Upload Permission */
        } else {
            return('');                                                         /* Not found, so no permission so return empty string */
        }
    }
    /**
     * getServiceCentreSamsungUpload
     *  
     * Return whether the Service Centre's company Name. 
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return string   Company Name of Service Centre 
     * 
     * @author Andris Polnikovs
     **************************************************************************/

    public function getServiceCentreAncronym($sId) {
        $sql = "
                SELECT
			`Acronym`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['Acronym']);                                  /* ServiceProviderID exists so return Samsung Upload Permission */
        } else {
            return('');                                                         /* Not found, so no permission so return empty string */
        }
    }
    
    
    
    /**
    @action gets service provider data of the given ID
    @input service provider ID
    @return service provider details
    @output void
    2012-10-11 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getServiceProvider($serviceProviderID) {
	
	$q = "SELECT * FROM service_provider WHERE ServiceProviderID = :serviceProviderID";
	$args = ["serviceProviderID" => $serviceProviderID];
        $result = $this->Query($this->conn, $q, $args);
        if(count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
	
    }
    
    
    
    /**
    @action gets all service providers data
    @input  void
    @return service providers details
    @output void
    2013-02-11 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getAllServiceProviders() {
	
	$q = "SELECT * FROM service_provider order by CompanyName";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
      /**
    @action gets all service providers data
    @input  void
    @return service providers details
    @output void
    2013-02-11 © Andris
    */
    
    public function getAllActiveServiceProviders() {
	
	$q = "SELECT ServiceProviderID,Acronym FROM service_provider where Status='Active'";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    

    
    /**
     * getServiceCentreOnlineDiary
     *  
     * Get whether the current service center uses the online diary
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return boolean  Does the service centre use the online diary
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getServiceCentreOnlineDiary($sId) {
        $sql = "
                SELECT
			`OnlineDiary`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['OnlineDiary']);                                  /* ServiceProviderID exists so return Online diary */
        } else {
            return(null);                                                       /* Not found, so no permission so return null */
        }
    }
    
    
    /**
     * getServiceCentreDiaryType
     *  
     * Get the type of diary for a service center
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return string  The diary type
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getServiceCentreDiaryType($sId) {
        $sql = "
                SELECT
			`DiaryType`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['DiaryType']);                                    /* ServiceProviderID exists so return diary type */
        } else {
            return('');                                                    /* Not found, so return blank */
        }
    }
    
    
    /**
     * getServiceCentrePlatform
     *  
     * Return the platform the service provider uses
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return string   Platform used
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getPlatform($sId) {
        $sql = "
                SELECT
			`Platform`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['OnlineDiary']);                                  /* ServiceProviderID exists so return Online diary */
        } else {
            return(null);                                                       /* Not found, so no permission so return null */
        }
    }

   /**
    * getServiceProviderCapacity
    * 
    * Returns the vaerage number of appointments undertaken by each service centre
    * in the last week.
    * 
    * @param 
    * 
    * @return asscoiative array 
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/  
    public function getServiceProviderCapacity() {
        $now = date('Y-m-d');
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filter query accourding to user type - only want to report on own data */
            $type = " AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->user->BranchID)) {
            $type = " AND `BranchID` = {$this->user->BranchID} ";    
        } elseif (isset($this->user->ClientID)) {
            $type = " AND `ClientID` = {$this->user->ClientID} ";
        } elseif (isset($this->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                    sp.`CompanyName`,
                    sp.`ServiceProviderID`,
                    (SELECT 
                            FLOOR(COUNT(a.`AppointmentID`) / 7) 
                    FROM 
                            `appointment` a 
                    WHERE
                            a.`AppointmentDate` >= DATE_SUB('$now', INTERVAL 7 DAY)
                            AND a.`AppointmentDate` <= '$now'
                            AND a.`ServiceProviderID` = sp.`ServiceProviderID`
                    ) AS `Capacity`
                FROM
                    `service_provider` sp
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return records */
        } else {
            return(null);                                                       /* Not found return empty array */
        }

    }
    
   /**
    * getTotalServiceProviderCapacity
    * 
    * Returns the total average number of appointments undertaken by each service
    * centre in the last week for all service providers liked to teh current
    * user.
    * 
    * @param $add_where     Optional additional where clauses
    * 
    * @return asscoiative array 
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/  
    public function getTotalServiceProviderCapacity($add_where = "") {
        $now = date('Y-m-d');
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filter query accourding to user type - only want to report on own data */
            $type = " AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->user->BranchID)) {
            $type = " AND `BranchID` = {$this->user->BranchID} ";    
        } elseif (isset($this->user->ClientID)) {
            $type = " AND `ClientID` = {$this->user->ClientID} ";
        } elseif (isset($this->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			FLOOR(COUNT(a.`AppointmentID`) / 7) AS `TotalCapacity`
                FROM
			`appointment` a ,
			`service_provider` sp
                WHERE
			a.`AppointmentDate` >= DATE_SUB('$now', INTERVAL 7 DAY)
			AND a.`AppointmentDate` <= '$now'
			AND a.`ServiceProviderID` = sp.`ServiceProviderID`
                        $add_where
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['TotalCapacity']);                                   /* Job exists so return records */
        } else {
            return(0);                                                          /* Not found return empty array */
        }

    }
    
    
    
    public function authServiceProvidersList($ManufacturerID = null) {
	
	$q = "SELECT CompanyName as ServiceProviderName, 0 AS Auth, 0 as DataChecked, ServiceProviderID FROM service_provider WHERE Status='Active' order by CompanyName";
	$result = $this->query($this->conn, $q);
        
        if ($ManufacturerID === null) return $result;

        $model = $this->loadModel('ManufacturerServiceProvider');       
        $auths = $model->getServiceProvidersByManufacturer( $ManufacturerID );
        
        foreach($auths as $auth) {
            foreach($result as &$row) {
                if ($row['ServiceProviderID'] == $auth['ServiceProviderID']) {
                    if ($auth['Authorised'] == 'Yes') $row['Auth'] = 1;
                    if ($auth['DataChecked'] == 'Yes') $row['DataChecked'] = 1;
                    break;
                }
            }
        }
        
        return $result;
    } 
    
/**
     * getServiceCentreSamsungUpload
     *  
     * Return AutoReorderStock bollean or if sp not found returns false. 
     * 
     * @param integer $sId    Service Provider ID
     * 
     * @return string   Company Name of Service Centre 
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com>  
     **************************************************************************/

    public function getServiceProviderAutoReorderStatus($sId) {
        $sql = "
                SELECT
			`AutoReorderStock`
                FROM
			`service_provider`
                WHERE
			ServiceProviderID = $sId
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['AutoReorderStock']);                                  /* ServiceProviderID exists so return Samsung Upload Permission */
        } else {
            return false;                                                         /* Not found, so no permission so return empty string */
        }
    }
    
    public function updateSPDocLogo($spid,$img){
        $sql="update service_provider set DocLogo='$img' where ServiceProviderID=$spid";
        $this->Execute($this->conn, $sql);
    }
    
    public function getDocColours($spid){
        $sql="select DocColour1,DocColour2,DocColour3,DocLogo,DocumentCustomColorScheme from service_provider where ServiceProviderID=$spid";
        
      $res=$this->Query($this->conn, $sql);
        if($res[0]['DocumentCustomColorScheme']=="Skyline"){
            $res[0]['DocColour1']="25AAE1";
            $res[0]['DocColour2']="0F75BC";
            $res[0]['DocColour3']="0F75BC";
        }
        return $res;
    }
    
}
?>
