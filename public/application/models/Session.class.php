<?php
require_once('CustomModel.class.php');

class Session extends CustomModel { 
    
    private static $SessionActive = false;
    
    public function  __construct($Controller) {
    
        parent::__construct($Controller); 
        if (!self::$SessionActive) {
            self::$SessionActive = session_start();
        }
	
    }
    
    public function __get($property) {
        
        if (isset($_SESSION[$property])) {
            return $_SESSION[$property];
        } 
        
        return '';
    }
    
    public function __set($property, $value) {
        $_SESSION[$property] = $value;
    }
    
    public function __isset($property) {
        return isset($_SESSION[$property]);
    }
    
    public function __unset($property) {
        unset($_SESSION[$property]);
    }
    
    // Clear all session data...
    public function clear() {
        $_SESSION = array();
    }
}
?>
