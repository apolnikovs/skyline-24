<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of System Statuses Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class SMS extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.SMSID', 't1.SMSName', 't2.BrandName', 't1.BrandID', "DATE_FORMAT(t1.ModifiedDate, '%d/%m/%Y %h:%i %p')", "CONCAT(t3.ContactFirstName, ' ', t3.ContactLastName) AS UserFullName", 't1.Status');
    private $table     = "sms";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
           $tables = $this->table.' AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID LEFT JOIN user AS t3 ON t1.ModifiedUserID=t3.UserID';
           $output = $this->ServeDataTables($this->conn, $tables, $this->dbColumns, $args);
        
        
           return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['SMSID']) || !$args['SMSID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate status name.
     *
     * @param string $SMSName 
     * @param string $BrandID 
     * @param interger $SMSID
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($SMSName, $BrandID, $SMSID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT SMSID FROM '.$this->table.' WHERE SMSName=:SMSName AND BrandID=:BrandID AND SMSID!=:SMSID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':SMSName' => $SMSName, ':BrandID' => $BrandID, ':SMSID' => $SMSID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['SMSID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (BrandID, SMSName, SMSBodyText, Status, CreatedDate, ModifiedUserID, ModifiedDate)
            VALUES(:BrandID, :SMSName, :SMSBodyText, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';
        
        
        if($this->isValidAction($args['SMSName'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(
                
                ':BrandID' => $args['BrandID'], 
                ':SMSName' => $args['SMSName'], 
                ':SMSBodyText' => $args['SMSBodyText'], 
                ':Status' => $args['Status'], 
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => 'Your data has been inserted successfully.');
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT SMSID, SMSName, SMSBodyText, BrandID, Status, EndDate, ModifiedUserID, ModifiedDate FROM '.$this->table.' WHERE SMSID=:SMSID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':SMSID' => $args['SMSID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch all rows from database.
     *
     * @param int $BrandID  default false;
     * 
     * @global $this->table  
     * @return array It contains rows.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchAll($BrandID=false) {
       
        if($BrandID)
        {
            $sql = 'SELECT SMSID, SMSName, SMSBodyText, BrandID FROM '.$this->table.' WHERE Status=:Status AND BrandID=:BrandID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active', ':BrandID' => $BrandID));
        }   
        else
        {
            $sql = 'SELECT SMSID, SMSName, SMSBodyText, BrandID FROM '.$this->table.' WHERE Status=:Status';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active'));
        }
        
        
        $result = $fetchQuery->fetchAll();
        
        return $result;
    }
    
    
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['SMSName'], $args['BrandID'], $args['SMSID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($args['Status']=="In-active")
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                    $EndDate = $row_data['EndDate'];
                }    
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              BrandID=:BrandID, SMSName=:SMSName, SMSBodyText=:SMSBodyText, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate
              
              WHERE SMSID=:SMSID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        ':BrandID' => $args['BrandID'],
                        ':SMSName' => $args['SMSName'], 
                        ':SMSBodyText' => $args['SMSBodyText'], 
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':SMSID' => $args['SMSID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to send SMS 
     * 
     * @param int $SMSID 
     * @param int $MobileNumber 
     * 
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
   /* public function fetch($SMSID, $MobileNumber) {
        
           $tables = $this->table.' AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID LEFT JOIN user AS t3 ON t1.ModifiedUserID=t3.UserID';
           $output = $this->ServeDataTables($this->conn, $tables, $this->dbColumns, $args);
        
        
           return  $output;
        
     }*/
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
    
}
?>