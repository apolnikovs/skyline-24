<?php

require_once('CustomBusinessModel.class.php');

/**
 * Short Description of Performance Business Model.
 * 
 * Long description of Performance Business Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 11/12/2012  1.0     Brian Etherington     Initial Version
 ******************************************************************************/

class PerformanceBusinessModel extends CustomBusinessModel {
    
    private $QueryModel;
    
    public function __construct( $controller ) {    
        parent::__construct($controller); 
        $this->QueryModel = $this->loadModel('PerformanceQueries');
    }
    
    public function getSegmentationSummary($EndDate=null, $StartDate=null) {
        
        if ($EndDate === null) $EndDate = date('Y-m-d');
        if ($StartDate === null) $StartDate = date('Y-m-d', strtotime('30 days ago',strtotime($EndDate)));
               
        $result = $this->QueryModel->listServiceCentres();       
        $data = $this->QueryModel->getSegSummary($EndDate, $StartDate);
        
        foreach($result as &$row) {
            
            $found = false;
            foreach($data as $datarow) {
            
                if ($row['ServiceCentre'] == $datarow['ServiceCentre']) {
                    $found = true;
                    $row['ResponseTime']     = $datarow['ResponseTime'];
                    $row['CallOutTime']      = $datarow['CallOutTime'];
                    $row['InspectionTime']   = $datarow['InspectionTime'];
                    $row['WaitingForRepair'] = $datarow['WaitingForRepair'];
                    $row['CustomerFeedback'] = $datarow['CustomerFeedback'];
                    $row['CompletionTime']   = $datarow['CompletionTime'];
                    $row['FinalDelivery']    = $datarow['FinalDelivery'];
                    $row['TimeToClaim']      = $datarow['TimeToClaim'];
                    continue;
                }
            
            }
            
            if (!$found) {
                
                $row['ResponseTime']     = 0.00;
                $row['CallOutTime']      = 0.00;
                $row['InspectionTime']   = 0.00;
                $row['WaitingForRepair'] = 0.00;
                $row['CustomerFeedback'] = 0.00;
                $row['CompletionTime']   = 0.00;
                $row['FinalDelivery']    = 0.00;
                $row['TimeToClaim']      = 0.00;
            }
            
        }
        
        return $result;
        
    }
    
    public function getSegmentationDetail( $user, $service_centre ) {
        
    }

}

?>
