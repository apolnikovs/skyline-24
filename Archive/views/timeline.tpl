<div class="node past first">
            <p class="stage">Booked <br /> [branch]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/past.png" />*}
            <p class="date">{$BookedDate}</p>     
        </div>
        <div class="node past">
            <p class="stage">Dispatched <br /> [courier]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/past.png" />*}
            <p class="date">{$DispatchedFromDate}</p>     
        </div>
        <div class="node past">
            <p class="stage">Received <br /> [service center]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/past.png" />*}
            <p class="date">dd/mm/yyyy</p>     
        </div>
        <div class="node past">
            <p class="stage">[job type] <br /> Repair Authorised</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/past.png" />*}
            <p class="date">dd/mm/yyyy</p>     
        </div>            

        <div class="node present">
            <p class="stage">Parts Ordered <br /> [service center]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/present.png" />*}
            <p class="date">dd/mm/yyyy</p>
        </div>
        <div class="node future">
            <p class="stage">Parts Arrived <br /> [service center]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/future.png" />*}
            <p class="date">dd/mm/yyyy</p>
        </div>
        <div class="node future">
            <p class="stage">Despatched <br /> [courier]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/future.png" />*}
            <p class="date">dd/mm/yyyy</p>
        </div>
        <div class="node future last">
            <p class="stage">Returned to <br /> [title surname]</p>
            {*<img class="hide" src="{$_subdomain}/css/images/{$_theme}/future.png" />*}
            <p class="date">dd/mm/yyyy</p>
        </div>