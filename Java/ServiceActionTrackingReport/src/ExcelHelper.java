import java.util.ArrayList;


public class ExcelHelper {

	
	//This function calculates max width for each column
	public static float[] calculateColumnWidths(ArrayList<String[]> data) {
		
		float[] result = new float[data.get(0).length];
		
		for(int i = 0; i < data.get(0).length; i++) {
			
			int strLength = 0;
			
			for(int j = 0; j < data.size(); j++) {
				if(data.get(j)[i] != null && data.get(j)[i].length() > strLength) {
					strLength = data.get(j)[i].length();
				}
			}
			
			float maxDigitWidth = 5.5f;
			int padding = 20;
			
			result[i] = ((strLength * maxDigitWidth + padding) / maxDigitWidth * 256) / 256;
			
			if(result[i] < 5) {
				result[i] = 5;
			}
			
		}
		
		//width = Truncate([{Number of Characters} * {Maximum Digit Width} + {5 pixel padding}] / {Maximum Digit Width} * 256) / 256
		
		return result;
		
	}
	
	
}
